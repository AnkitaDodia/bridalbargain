package com.aasthasolutions.bridalbargain.fcm;

/**
 * Created by Adite-Ankita on 23-Aug-16.
 */

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.activities.MyPlansActivity;
import com.aasthasolutions.bridalbargain.activities.NotificationViewActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class CustomFirebaseMessagingService extends FirebaseMessagingService
{
    private static final String TAG = "ANKITA";

    private static NotificationManager notifManager;

    int mode;

    @Override
    public void onCreate()
    {
        // TODO Auto-generated method stub
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        Map<String, String> data = remoteMessage.getData();
        try {
            JSONObject dataJSON = new JSONObject(data);
            Log.e("onMessageReceived-->","" + data);
            processNotification(dataJSON);
        } catch (Exception e) {
            Log.e("onMessageReceived-->","" + e.getMessage());
        }
    }

    private void processNotification(JSONObject data)
    {
        final String msg = data.optString("body");
        final String title = data.optString("title");
        final String click_action = data.optString("click_action");

        handleDataMessage(title,msg,click_action);
    }

    private void handleDataMessage(String title, String msg, String clickEvent) {

        try {
            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + msg);
            Log.e(TAG, "click_action: " + clickEvent);

            if(clickEvent.equalsIgnoreCase("BRIDLE_EXPIRED")){
                Intent completedIntent = new Intent();
                completedIntent.setAction(clickEvent);

                if(!isAppIsInBackground(getApplicationContext())){
                    createNotification(title,getApplicationContext(),msg,completedIntent);
                }else {
                    completedIntent.setClass(this, MyPlansActivity.class);
                    createNotification(title,getApplicationContext(),msg,completedIntent);
                }
            }if(clickEvent.equalsIgnoreCase("BRIDLE_NOTICE")){
                Intent completedIntent = new Intent();
                completedIntent.setAction(clickEvent);
                completedIntent.putExtra("body", msg);
                completedIntent.putExtra("title", title);

                if(!isAppIsInBackground(getApplicationContext())){
                    createNotification(title,getApplicationContext(),msg,completedIntent);
                }else {
                    completedIntent.setClass(this, NotificationViewActivity.class);
                    createNotification(title,getApplicationContext(),msg,completedIntent);
                }
            } else{

                mode = 10;

                Intent completedIntent = new Intent();
                completedIntent.setAction(clickEvent);
                completedIntent.putExtra("mode", mode);

                BaseActivity.saveMode(mode,getApplicationContext());

                if(!isAppIsInBackground(getApplicationContext())){
                    createNotification(title,getApplicationContext(),msg,completedIntent);
                }else {
                    completedIntent.setClass(this, HomeActivity.class);
                    createNotification(title,getApplicationContext(),msg,completedIntent);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    public void createNotification(String title, Context context,String msg,Intent intent) {
        final int NOTIFY_ID = 0; // ID of notification
        String id = "my_channel_03"; // default_channel_id
        CharSequence name = getApplicationContext().getString(R.string.app_name);
        String description = getApplicationContext().getString(R.string.app_name);

        PendingIntent pendingIntent;
        NotificationCompat.Builder builder;
        if (notifManager == null) {
            notifManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notifManager.getNotificationChannel(id);
            if (mChannel == null) {
                mChannel = new NotificationChannel(id, name, importance);
                mChannel.setDescription(description);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notifManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(context, id);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentTitle(title)                            // required
                    .setSmallIcon(R.mipmap.ic_white_notification)   // required
                    .setContentText(msg) // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                    .setContentIntent(pendingIntent)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        }
        else {
            builder = new NotificationCompat.Builder(context, id);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//            pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            builder.setContentTitle(title)                            // required
                    .setSmallIcon(R.mipmap.ic_white_notification)   // required
                    .setContentText(msg) // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                    .setContentIntent(pendingIntent)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH);

//            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
//            bigTextStyle.setBigContentTitle("Title");
//            bigTextStyle.bigText();
        }
        Notification notification = builder.build();
        notifManager.notify(NOTIFY_ID, notification);
    }

    public void createActionNotification(String title, Context context,String msg,Intent intent) {
        final int NOTIFY_ID = 0; // ID of notification
        String id = "my_channel_02"; // default_channel_id
        int notification_id = 1;
        CharSequence name = getApplicationContext().getString(R.string.app_name);
        String description = getApplicationContext().getString(R.string.app_name);

        PendingIntent pendingIntent;
        NotificationCompat.Builder builder;
        if (notifManager == null) {
            notifManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notifManager.getNotificationChannel(id);
            if (mChannel == null) {
                mChannel = new NotificationChannel(id, name, importance);
                mChannel.setDescription(description);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notifManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(context, id);

            //Pending intent for mute action
            Intent muteIntent = new Intent(context, MyPlansActivity.class);
            muteIntent.setAction("MY_PLAN");
            PendingIntent pendingIntentMute = PendingIntent.getBroadcast(context, notification_id, muteIntent, PendingIntent.FLAG_UPDATE_CURRENT);


            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentTitle(msg)                            // required
                    .setSmallIcon(R.mipmap.ic_launcher)   // required
                    .setContentText(context.getString(R.string.app_name)) // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(title)
                    .addAction(R.drawable.ic_payment,"My Plan",pendingIntentMute)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        }
        else {
            builder = new NotificationCompat.Builder(context, id);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//            pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            Intent muteIntent = new Intent(context, MyPlansActivity.class);
            muteIntent.setAction("MY_PLAN");
            PendingIntent pendingIntentMute = PendingIntent.getBroadcast(context, notification_id, muteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentTitle(title)                            // required
                    .setSmallIcon(R.mipmap.ic_white_notification)   // required
                    .setContentText(msg) // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(msg)
                    .addAction(R.drawable.ic_payment,"My Plan",pendingIntentMute)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH);
        }
        Notification notification = builder.build();
        notifManager.notify(NOTIFY_ID, notification);
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }
}