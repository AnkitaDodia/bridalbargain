package com.aasthasolutions.bridalbargain.customviews.callback;

public interface Callback {
  void onError(Throwable e);
}
