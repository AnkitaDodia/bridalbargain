package com.aasthasolutions.bridalbargain.customviews.callback;

import android.net.Uri;

public interface SaveCallback extends Callback {
  void onSuccess(Uri uri);
}
