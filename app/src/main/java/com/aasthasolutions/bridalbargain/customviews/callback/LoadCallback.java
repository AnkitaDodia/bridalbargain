package com.aasthasolutions.bridalbargain.customviews.callback;

public interface LoadCallback extends Callback {
  void onSuccess();
}
