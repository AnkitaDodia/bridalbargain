package com.aasthasolutions.bridalbargain.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.model.BannerTitle;
import com.aasthasolutions.bridalbargain.model.SellerProductData;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.model.VendorServicesData;
import com.aasthasolutions.bridalbargain.utility.InternetStatus;
import com.aasthasolutions.bridalbargain.utility.TypeFaces;

import java.text.ParseException;
import java.util.ArrayList;

public class
BaseActivity extends AppCompatActivity
{
    //For Seller 0, vendor 1, customer 2
    public static int USER_MODE;

    public static String SERVICE_CATEGORY_ID ;
    public static String SERVICE_ID ;

    public static String SELLER_CATEGORY_ID ;
    public static String SELLER_PRODUCT_ID ;
    public static int SELLER_PRODUCT_POSITION ;
    public static int VENDOR_SERVICE_POSITION ;

    public static String CHAT_USER_ID, CHAT_ROLE , FROM_ROLE, TO_ROLE,  CHAT_NAME, CHAT_IMAGE, USER_IMAGE;

    public static ArrayList<SellerProductData> mGetSellerProductDataList = new ArrayList<>();
    public static ArrayList<VendorServicesData> mVendorServiceDataList = new ArrayList<>();

    public static boolean IS_ADD_PRODUCT = true;
    public static boolean IS_ADD_SERVICE = true;
    public static boolean isFromVendor = false;

    private Typeface font;
    ProgressDialog mProgressDialog;

    private static final String PREFS_NAME = "LOCAL_DB";
    private static final String DEFAULT_VAL = null;


    public static String FCMTOKEN = null;

    public static ArrayList<BannerTitle> mInro_Images_List = new ArrayList<>();


    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final int WEEK_MILLIS = 7 * DAY_MILLIS;

    public static boolean isShowBackIcon = false;
    public static Bitmap BmpCropped;

    public static final String GALLERY_DIRECTORY_NAME = "Camera";
    // Image and Video file extensions
    public static final String IMAGE_EXTENSION = "jpg";
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static String imageStoragePath;

    public static boolean CheckInternet(Context mContext){

        return new InternetStatus().isInternetOn(mContext);
    }


    // Code for get and set login
    public void setUserMode(int i)
    {
        SharedPreferences sp = getSharedPreferences("UserMode",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("UserMode",i);
        spe.apply();
    }

    public int getUserMode()
    {
        SharedPreferences sp = getSharedPreferences("UserMode",MODE_PRIVATE);
        int i = sp.getInt("UserMode",0);
        return i;
    }
    // Code for get and set login


    // Code for get and set login
    public void setLogin(int i)
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.clear();
        spe.putInt("Login",i);
        spe.commit();
        spe.apply();
    }

    public int getLogin()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        int i = sp.getInt("Login",0);
        return i;
    }
    // Code for get and set login

    // Code for get and set login
    public void setIntroScreenText(String mtext)
    {
        SharedPreferences sp = getSharedPreferences("INTROTEXT", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.clear();
        spe.putString("IntroText",mtext);
        spe.commit();
        spe.apply();
    }

    public String getIntroScreenText()
    {
        SharedPreferences sp = getSharedPreferences("INTROTEXT", MODE_PRIVATE);
        String mtext = sp.getString("IntroText", "");
        return mtext;
    }
    // Code for get and set login

    public Typeface getRegularFonts(Context mContext)
    {
        font = TypeFaces.getTypeFace(mContext, "TimesNewRomans.ttf");
//        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Regular.ttf");
        return font;
    }

    public Typeface getRegularBoldFonts(Context mContext)
    {
        font = TypeFaces.getTypeFace(mContext, "TimesNewRomansbold.ttf");
//        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Regular.ttf");
        return font;
    }

    public Typeface getThemeFonts(Context mContext)
    {
        font = TypeFaces.getTypeFace(mContext, "Vibes.ttf");
//        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Regular.ttf");
        return font;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public String getFCMTOKEN()
    {
        SharedPreferences sp = getSharedPreferences("FCM",MODE_PRIVATE);
        String fcm_token = sp.getString("FCM_ID",null);
        return fcm_token;
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public void showWaitIndicator(boolean state) {
//        showWaitIndicator(state, "");

        if (state) {

            showProgressDialog();

        } else {

            dismissProgressDialog();
        }
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            initProgressDialog();
        }
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
//            mProgressDialog.setMessage("LOADING...");
            mProgressDialog.show();
            ProgressBar progressbar=(ProgressBar)mProgressDialog.findViewById(android.R.id.progress);
            progressbar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FFDF284D"), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    private void initProgressDialog() {
        Log.d("ProgressBar", "initProgressDialog()");
        mProgressDialog = new ProgressDialog(this,  R.style.TransparentProgressDialog);
//        mProgressDialog.setMessage(LOADING);
        mProgressDialog.setCancelable(false);
//        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    public void dismissProgressDialog() {
        Log.d("ProgressBar", "dismissProgressDialog()");
        try {
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public void showWaitIndicator(boolean state, String message) {
        try {
            try {

                if (state) {

                    mProgressDialog = new ProgressDialog(this, R.style.TransparentProgressDialog);
                    mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
//                    mProgressDialog.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progress_bar));
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();

                } else {
                    mProgressDialog.dismiss();
                }

            } catch (Exception e) {


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public static void setUserDetails(Context context, UserDetails user) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor dbEditor = prefs.edit();

        dbEditor.putString("firstname",user.getFirstName());
        dbEditor.putString("lastname",user.getLastName());
        dbEditor.putString("email",user.getEmail());
        dbEditor.putString("address",user.getAddress());
        dbEditor.putString("city",user.getCity());
        dbEditor.putString("userid",user.getIdUser());
        dbEditor.putString("profile_pic",user.getProfileImage());
        dbEditor.putString("phone",user.getPhonenumber());

//		dbEditor.apply();

        dbEditor.commit();
    }

    public static UserDetails getUserDetails(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        UserDetails user = new UserDetails();

        user.setFirstName(prefs.getString("firstname",DEFAULT_VAL));
        user.setLastName(prefs.getString("lastname",DEFAULT_VAL));
        user.setEmail(prefs.getString("email",DEFAULT_VAL));
        user.setAddress(prefs.getString("address",DEFAULT_VAL));
        user.setCity(prefs.getString("city",DEFAULT_VAL));
        user.setIdUser(prefs.getString("userid",DEFAULT_VAL));
        user.setProfileImage(prefs.getString("profile_pic",DEFAULT_VAL));
        user.setPhonenumber(prefs.getString("phone",DEFAULT_VAL));

        return user;
    }

    public void overrideFonts(final View v, Context ctx) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(child, ctx);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(getRegularFonts(ctx));
            }else if (v instanceof Button) {
                ((Button) v).setTypeface(getRegularFonts(ctx));
            }else if (v instanceof EditText) {
                ((EditText) v).setTypeface(getRegularFonts(ctx));
            }


        } catch (Exception e) {
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void saveMode(int mode,Context ctx){
        SharedPreferences sp = ctx.getSharedPreferences("MODE", Context.MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("mode",mode);
        spe.apply();
    }

    public int getMode(){
        SharedPreferences sp = getSharedPreferences("MODE", Context.MODE_PRIVATE);
        return sp.getInt("mode",0);
    }

    public void clearModePref(){
        SharedPreferences sp = getSharedPreferences("MODE", Context.MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.clear();
        spe.apply();
    }
}
