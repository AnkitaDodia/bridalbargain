package com.aasthasolutions.bridalbargain.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.ForgotPassword;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgotPasswordActivity extends BaseActivity {

    Context mContext;

    final String TAG = "FORGOT";
    private Button btn_forgot_submit;
    private LinearLayout ll_parent_forgot_password, ll_back_forgot;
    private TextInputEditText tedt_forgot_email;
    private TextView txt_forgot_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mContext = this;

        initviews();
        setListners();
    }

    private void initviews() {

        tedt_forgot_email = findViewById(R.id.tedt_forgot_email);

        btn_forgot_submit = findViewById(R.id.btn_forgot_submit);


        ll_parent_forgot_password = findViewById(R.id.ll_parent_forgot_password);
        ll_back_forgot = findViewById(R.id.ll_back_forgot);

        txt_forgot_title = findViewById(R.id.txt_forgot_title);

        overrideFonts(ll_parent_forgot_password, mContext);

        txt_forgot_title.setTypeface(getThemeFonts(mContext));
    }

    private void setListners() {

        btn_forgot_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(tedt_forgot_email.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "Please enter email address", Toast.LENGTH_LONG).show();
                } else if (!BaseActivity.isValidEmail(tedt_forgot_email.getText().toString())) {
//                    edt_login_email.setError("Please enter valid email");
                    Toast.makeText(mContext, "Please enter a valid email address", Toast.LENGTH_LONG).show();
                } else {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendForgotPasswordRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        ll_back_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }

    private void sendForgotPasswordRequest() {

        showWaitIndicator(true);

        String email = tedt_forgot_email.getText().toString();

        Log.e(TAG, "Mode : " + BaseActivity.USER_MODE);
        Log.e(TAG, "Email : " + tedt_forgot_email.getText().toString());

//        Gson gson = new GsonBuilder()
//                .setLenient()
//                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverterFactory.create()
                .build();

//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(RestInterface.API_BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<ForgotPassword> call = service.sendForgotPasswordRequest(email, BaseActivity.USER_MODE);

        call.enqueue(new Callback<ForgotPassword>() {
            @Override
            public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e(TAG, "RESPONSE : " + response.body().getMessage());
                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if (response.body().getStatus() == 1) {

                            finish();

                        } else {

                        }


                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ForgotPassword> call, Throwable t) {
                showWaitIndicator(false);
                Log.d(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onBackPressed() {

        finish();

    }

}
