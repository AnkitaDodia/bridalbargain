package com.aasthasolutions.bridalbargain.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.PayMent;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sqip.Callback;
import sqip.CardDetails;
import sqip.CardEntry;
import sqip.CardEntryActivityResult;

public class PaymentActivity extends BaseActivity {

    final String TAG = "PAYMENT";
    PaymentActivity mContext;
    Button btn_pay;
    private String mNonce = null;
    private String mUserId = null;
    private String mFrom_Where = null;
    LinearLayout ll_back_payment;

//    sandbox-sq0idp-hZoRKdG4MaX4-iwLiNlLcw
//    sq0idp-hZoRKdG4MaX4-iwLiNlLcw

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        mContext = this;

        Intent mIntent = getIntent();
        mUserId = mIntent.getStringExtra("USER_ID");
        Log.e("USER_ID","USER_ID : "+mUserId);
        mFrom_Where = getIntent().getStringExtra("FROM_WHERE");
        Log.e("FROM_WHERE","mFrom_Where : "+mFrom_Where);


        isShowBackIcon = false;

        btn_pay = findViewById(R.id.btn_pay);
        ll_back_payment = findViewById(R.id.ll_back_payment);

        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CardEntry.startCardEntryActivity(mContext);
            }
        });

        ll_back_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               finish();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        CardEntry.handleActivityResult(data, new Callback<CardEntryActivityResult>() {
            @Override
            public void onResult(CardEntryActivityResult cardEntryActivityResult) {

                if (cardEntryActivityResult.isSuccess()) {

                    CardDetails mCard = cardEntryActivityResult.getSuccessValue();
                    String successMessage = "Card to be Charged : "+ mCard.toString();

                    Log.e("RESPONSE", "Card Details  : "+successMessage);

                    successMessage = successMessage.substring(successMessage.indexOf("=") + 1);
                    successMessage = successMessage.substring(0, successMessage.indexOf(","));

                    mNonce = successMessage;

                    Log.e("RESPONSE", "Card Nonce  : "+successMessage);

                    sendCardNonceRequest();

                }

            }
        });

    }


    private void sendCardNonceRequest() {

        mContext.showWaitIndicator(true);

//        Log.e(TAG,"Mode : "+ BaseActivity.USER_MODE);
        Log.e(TAG,"mIdUser : "+ mUserId);
        Log.e(TAG,"Nonce : "+mNonce);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<PayMent> call = service.sendCardNonceRequest(mUserId, mNonce);

        call.enqueue(new retrofit2.Callback<PayMent>() {
            @Override
            public void onResponse(Call<PayMent> call, Response<PayMent> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

//                        if(response.body().getStatus() == 1){

                            Log.e(TAG,"RESPONSE : "+response.body().getMessage());
                            Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            if(mFrom_Where.equalsIgnoreCase("MyPlans")){

                                Intent i = new Intent(mContext, HomeActivity.class);
                                startActivity(i);
                            }else {

                                Intent i = new Intent(mContext, AuthorizationTabActivity.class);
                                startActivity(i);
                            }

//                        }else {
//
//                        }


                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<PayMent> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG,"onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }


}
