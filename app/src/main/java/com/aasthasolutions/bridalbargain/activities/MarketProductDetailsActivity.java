package com.aasthasolutions.bridalbargain.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.customview.SquareImageView;
import com.aasthasolutions.bridalbargain.fragment.MessagesFragment;
import com.aasthasolutions.bridalbargain.model.SellerSingleProduct;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MarketProductDetailsActivity extends BaseActivity {

    Context mContext;
    LinearLayout layout_detail_main, ll_market_details_message;
    SquareImageView img_detail;
    TextView txt_product_name_detail, txt_product_price_detail, txt_lable_product_discription, txt_product_discription, txt_soldby_msg, txt_product_provider, txt_lable_manufacture_details,
            txt_lable_product_email_detail, txt_product_email_detail, txt_lable_product_address_detail, txt_product_address_detail, txt_lable_product_contact, txt_product_contact, txt_product_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_product_details);

        mContext = this;

        InitViews();

        if (BaseActivity.CheckInternet(mContext)) {

            sendSellerProductRequest();

        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews()
    {
        layout_detail_main = findViewById(R.id.layout_detail_main);
        ll_market_details_message = findViewById(R.id.ll_market_details_message);

        img_detail = findViewById(R.id.img_detail);

        txt_product_name_detail = findViewById(R.id.txt_product_name_detail);
        txt_product_price_detail = findViewById(R.id.txt_product_price_detail);
        txt_lable_product_discription = findViewById(R.id.txt_lable_product_discription);
        txt_product_discription = findViewById(R.id.txt_product_discription);

        txt_soldby_msg = findViewById(R.id.txt_soldby_msg);
        txt_product_provider = findViewById(R.id.txt_product_provider);
        txt_lable_manufacture_details = findViewById(R.id.txt_lable_manufacture_details);

        txt_lable_product_email_detail  = findViewById(R.id.txt_lable_product_email_detail);
        txt_product_email_detail = findViewById(R.id.txt_product_email_detail);

        txt_lable_product_address_detail = findViewById(R.id.txt_lable_product_address_detail);
        txt_product_address_detail = findViewById(R.id.txt_product_address_detail);

        txt_lable_product_contact = findViewById(R.id.txt_lable_product_contact);
        txt_product_contact = findViewById(R.id.txt_product_contact);


        txt_product_message = findViewById(R.id.txt_product_message);
        txt_product_message.setText("Contact to Seller");

        txt_soldby_msg.setText("SOLD  BY");

        overrideFonts(layout_detail_main, mContext);


        ll_market_details_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getLogin() == 0) {

                    Intent i = new Intent(mContext, AuthorizationTabActivity.class);
                    startActivity(i);

                } else {

//                    Intent i = new Intent(mContext, MessageActivity.class);
//                    startActivity(i);

                    Toast.makeText(mContext, "Working on it", Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    private void sendSellerProductRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Log.e("SELLER_PRODUCT_DETAILS","RESPONSE : "+BaseActivity.SELLER_PRODUCT_ID);

        Call<SellerSingleProduct> call = service.sendSellerSingleProductRequest(BaseActivity.SELLER_PRODUCT_ID);

        call.enqueue(new Callback<SellerSingleProduct>() {
            @Override
            public void onResponse(Call<SellerSingleProduct> call, Response<SellerSingleProduct> response) {
                showWaitIndicator(false);

                Log.e("SELLER_PRODUCT_DETAILS","RESPONSE : "+response.code());

                try {
                    if (response.code() == 200) {

                        Log.e("SELLER_PRODUCT_DETAILS","RESPONSE : "+response.body().getStatus());

                        if(response.body().getStatus() == 1){

                            Log.e("SELLER_PRODUCT_DETAILS","RESPONSE : "+response.body().getData().getProductName());

                           /* Glide.with(mContext)
                                    .load(response.body().getData().getProductImg())
                                    .into(img_detail);

                            txt_product_name_detail.setText(response.body().getData().getProductName());
                            txt_product_price_detail.setText("$"+response.body().getData().getProductPrice());
                            txt_product_owner_name_detail.setText(response.body().getData().getFirstName());
                            text_product_email_detail.setText(response.body().getData().getEmail());
                            text_product_address_detail.setText(response.body().getData().getAddress());

                            Glide.with(mContext)
                                    .load(response.body().getData().getProfileImg())
                                    .into(img_profile_pic);

//                            text_product_provider.setText(response.body().getData().getFirstName()+" "+response.body().getData().getLastName());

                            text_product_discription.setVisibility(View.VISIBLE);
                            text_product_discription.setText(response.body().getData().getDescription());
                            text_product_contact.setText(response.body().getData().getPhone());

                            String upperString = response.body().getData().getFirstName().substring(0,1).toUpperCase() + response.body().getData().getFirstName().substring(1);
                            text_product_provider.setText(upperString+" "+response.body().getData().getLastName());*/


                            Glide.with(mContext)
                                    .load(response.body().getData().getProfileImg())
                                    .into(img_detail);

                            txt_product_name_detail.setText(response.body().getData().getProductName());
                            txt_product_price_detail.setText("$"+response.body().getData().getProductPrice());
                            txt_product_discription.setText(response.body().getData().getDescription());

                            String upperString = response.body().getData().getFirstName().substring(0,1).toUpperCase() + response.body().getData().getFirstName().substring(1);
                            txt_product_provider.setText(upperString+" "+response.body().getData().getLastName());


                            txt_product_email_detail.setText(response.body().getData().getEmail());
                            txt_product_address_detail.setText(response.body().getData().getAddress());
                            txt_product_contact.setText(response.body().getData().getPhone());


                        }else {


                        }

                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<SellerSingleProduct> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());

            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
