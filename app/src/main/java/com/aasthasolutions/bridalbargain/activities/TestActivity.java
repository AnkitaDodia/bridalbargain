//package com.aasthasolutions.bridalbargain.activities;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.View;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.aasthasolutions.bridalbargain.R;
//import com.aasthasolutions.bridalbargain.adapter.GetSellersAdapter;
//import com.aasthasolutions.bridalbargain.adapter.GetVendorsAdapter;
//import com.aasthasolutions.bridalbargain.common.BaseActivity;
//import com.aasthasolutions.bridalbargain.model.GetSellers;
//import com.aasthasolutions.bridalbargain.model.GetSellersData;
//import com.aasthasolutions.bridalbargain.model.GetVendors;
//import com.aasthasolutions.bridalbargain.model.GetVendorsData;
//import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
//
//import java.util.ArrayList;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import retrofit2.Retrofit;
//import retrofit2.converter.gson.GsonConverterFactory;
//
//public class TestActivity extends BaseActivity {
//
//    Context mContext;
////    HomeActivity mContext;
//    RecyclerView rv_marketplace;
//    TextView txt_empty_marketplace;
//    GetSellersAdapter mGetSellersAdapter;
//
//    GetVendorsAdapter mGetVendorsAdapter;
//
//    ArrayList<GetVendorsData> mGetVendorsDataList = new ArrayList<>();
//
//    ArrayList<GetSellersData> mGetSellersDataList = new ArrayList<>();
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.fragment_chatting);
//
//        mContext = this;
///*
//        txt_empty_marketplace = findViewById(R.id.txt_empty_marketplace);
//        rv_marketplace = findViewById(R.id.rv_marketplace);
//        rv_marketplace.setHasFixedSize(true);
//        rv_marketplace.setLayoutManager(new GridLayoutManager(mContext, 2));
//
//        rv_marketplace.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_marketplace, new BaseActivity.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//
//                BaseActivity.SELLER_CATEGORY_ID = mGetSellersDataList.get(position).getIdCategory();
//
////                changeFragment(new MarketProductFragment());
//
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));
//
//        if (BaseActivity.CheckInternet(mContext)) {
////            sendGetSellersRequest();
////            sendGetVendorRequest();
//        } else {
//            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
//        }*/
//    }
//
//   /* private void sendGetSellersRequest() {
//
//        showWaitIndicator(true);
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(RestInterface.API_BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        RestInterface service = retrofit.create(RestInterface.class);
//
//        Call<GetSellers> call = service.sendGetSellersRequest();
//
//        call.enqueue(new Callback<GetSellers>() {
//            @Override
//            public void onResponse(Call<GetSellers> call, Response<GetSellers> response) {
//
//                showWaitIndicator(false);
//
//                try {
//                    if (response.code() == 200) {
//
//                        if(response.body().getStatus() == 1){
//
//                            rv_marketplace.setVisibility(View.VISIBLE);
//                            txt_empty_marketplace.setVisibility(View.GONE);
//
//                            for(int i =0; i < response.body().getData().size(); i++){
//
//                                mGetSellersDataList.add(response.body().getData().get(i));
//                                Log.e("GET_VENDOR","RESPONSE : "+response.body().getData().get(i).getCategoryName());
//                            }
//
//                            mGetSellersAdapter = new  GetSellersAdapter(mContext, mGetSellersDataList);
//                            rv_marketplace.setAdapter(mGetSellersAdapter);
//
//                        }else {
//
//                            rv_marketplace.setVisibility(View.GONE);
//                            txt_empty_marketplace.setVisibility(View.VISIBLE);
//                            txt_empty_marketplace.setText(response.body().getMessage());
//                        }
//                    }
//
//                } catch (Exception e) {
////                    Log.d("onResponse", "There is an error");
//                    showWaitIndicator(false);
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GetSellers> call, Throwable t) {
//                showWaitIndicator(false);
//                Log.d("onFailure", t.toString());
//                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//    }*/
//
//   /* private void sendGetVendorRequest() {
//
////        mContext.showWaitIndicator(true);
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(RestInterface.API_BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        RestInterface service = retrofit.create(RestInterface.class);
//
//        Call<GetVendors> call = service.sendGetVendorsRequest();
//
//        call.enqueue(new Callback<GetVendors>() {
//            @Override
//            public void onResponse(Call<GetVendors> call, Response<GetVendors> response) {
//
//
//                try {
//                    if (response.code() == 200) {
//
//                        if(response.body().getStatus() == 1){
//
//                            rv_marketplace.setVisibility(View.VISIBLE);
//                            txt_empty_marketplace.setVisibility(View.GONE);
//
//                            for(int i =0; i < response.body().getData().size(); i++){
//
//                                mGetVendorsDataList.add(response.body().getData().get(i));
//                                Log.e("GET_VENDOR","RESPONSE : "+response.body().getData().get(i).getCategoryName());
//                            }
//
//                            mGetVendorsAdapter = new GetVendorsAdapter(mContext, mGetVendorsDataList);
//                            rv_marketplace.setAdapter(mGetVendorsAdapter);
//
//                        }else {
//
//                            rv_marketplace.setVisibility(View.GONE);
//                            txt_empty_marketplace.setVisibility(View.VISIBLE);
//                            txt_empty_marketplace.setText(response.body().getMessage());
//                        }
//
//
//
//                    }
//
//
//                } catch (Exception e) {
////                    Log.d("onResponse", "There is an error");
////                    mContext.showWaitIndicator(false);
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GetVendors> call, Throwable t) {
////                mContext.showWaitIndicator(false);
//                Log.d("onFailure", t.toString());
//                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//    }*/
//
//}
