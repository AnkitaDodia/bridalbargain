package com.aasthasolutions.bridalbargain.activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.customview.SlidingTabLayout;
import com.aasthasolutions.bridalbargain.fragment.SignInFragment;
import com.aasthasolutions.bridalbargain.fragment.SignUpFragment;

import java.util.List;

public class AuthorizationTabActivity extends BaseActivity {

    Context mContext;
    LinearLayout ll_parent_authentication, ll_back_authentication;
    ViewPager vp_authentication;
    TextView txt_authentication_title;
    SlidingTabLayout stl_authentication;
    SlidingPagerAdapter adapter;

    String[] mStrings = {"Sign In", "Sign Up"};

    int Numboftabs = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_login);

        mContext = this;


        // Assigning ViewPager View and setting the adapter
        vp_authentication = findViewById(R.id.vp_authentication);
        ll_parent_authentication = findViewById(R.id.ll_parent_authentication);
        stl_authentication = findViewById(R.id.stl_authentication);
        ll_back_authentication = findViewById(R.id.ll_back_authentication);
        txt_authentication_title = findViewById(R.id.txt_authentication_title);

//        if(USER_MODE == 1){
//
//            Numboftabs = 1;
//        }


        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter = new SlidingPagerAdapter(getSupportFragmentManager(), mStrings, Numboftabs);

        vp_authentication.setAdapter(adapter);

        stl_authentication.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        stl_authentication.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        stl_authentication.setViewPager(vp_authentication);

        ll_back_authentication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getUserMode() == 0){

                    Intent i = new Intent(mContext, SimpleIntroActivity.class);
                    startActivity(i);
                    finish();

                }else if(getUserMode() == 1){

                    Intent i = new Intent(mContext, SimpleIntroActivity.class);
                    startActivity(i);
                    finish();

                }else if(getUserMode() == 2){

                    finish();

                }
            }
        });


        overrideFonts(ll_parent_authentication, mContext);
        txt_authentication_title.setTypeface(getThemeFonts(mContext));

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        List<Fragment> listOfFragments = getSupportFragmentManager().getFragments();

        if (listOfFragments.size() >= 1) {
            for (Fragment fragment : listOfFragments) {

                if (fragment instanceof SignUpFragment) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    class SlidingPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public SlidingPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);

            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        //This method return the fragment for the every position in the View Pager
        @Override
        public Fragment getItem(int position) {

            if (position == 0) // if the position is 0 we are returning the First tab
            {
                SignInFragment tab1 = new SignInFragment();
                return tab1;
            } else             // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
            {
                SignUpFragment tab2 = new SignUpFragment();
                return tab2;
            }


        }

        // This method return the titles for the Tabs in the Tab Strip

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        // This method return the Number of tabs for the tabs Strip

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }

    @Override
    public void onBackPressed() {


        if(getUserMode() == 0){

            Intent i = new Intent(mContext, SimpleIntroActivity.class);
            startActivity(i);
            finish();

        }else if(getUserMode() == 1){

            Intent i = new Intent(mContext, SimpleIntroActivity.class);
            startActivity(i);
            finish();

        }else if(getUserMode() == 2){

            finish();

        }


    }

    public void ChangePagerFragment() {

        vp_authentication.setCurrentItem(0);
    }
}
