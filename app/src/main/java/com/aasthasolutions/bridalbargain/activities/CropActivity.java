package com.aasthasolutions.bridalbargain.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.customviews.CropImageView;

public class CropActivity extends BaseActivity {

    Context mContext;

    private CropImageView mCropView;
    private TextView txt_crop_title;
    private LinearLayout ll_back_crop;

    private RectF mFrameRect = null;
    private static final String KEY_FRAME_RECT = "FrameRect";
    private LinearLayout ll_save_crop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);

        mContext = this;

        initViews();
        SetListeners();

        if (savedInstanceState != null) {
            // restore data
            mFrameRect = savedInstanceState.getParcelable(KEY_FRAME_RECT);
//            mSourceUri = savedInstanceState.getParcelable(KEY_SOURCE_URI);
        }
    }

    private void initViews() {

        mCropView = findViewById(R.id.cropImageView);
        txt_crop_title = findViewById(R.id.txt_crop_title);
        txt_crop_title.setTypeface(getThemeFonts(mContext));
        ll_back_crop = findViewById(R.id.ll_back_crop);
//        Bitmap croppedImage = ((BitmapDrawable) ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.img_banner, null)).getBitmap();
        mCropView.setImageBitmap(BaseActivity.BmpCropped);

        ll_save_crop = findViewById(R.id.ll_save_crop);
    }


    private void SetListeners() {

        ll_save_crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.BmpCropped = mCropView.getCroppedBitmap();
                finish();
            }
        });

        ll_back_crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
//                returnIntent.putExtra("result",result);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();

            }
        });

    }

}
