package com.aasthasolutions.bridalbargain.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.adapter.GetMessagesAdapter;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.fragment.AddProductFragment;
import com.aasthasolutions.bridalbargain.fragment.AddServiceFragment;
import com.aasthasolutions.bridalbargain.fragment.ChatingFragment;
import com.aasthasolutions.bridalbargain.fragment.ContactFragment;
import com.aasthasolutions.bridalbargain.fragment.HomeMarketFragment;
import com.aasthasolutions.bridalbargain.fragment.HomeSellerFragment;
import com.aasthasolutions.bridalbargain.fragment.HomeVendorFragment;
import com.aasthasolutions.bridalbargain.fragment.MarketProductFragment;
import com.aasthasolutions.bridalbargain.fragment.MessagesFragment;
import com.aasthasolutions.bridalbargain.fragment.ChangePasswordFragment;
import com.aasthasolutions.bridalbargain.fragment.MyPlansFragment;
import com.aasthasolutions.bridalbargain.fragment.ProductDetailsFragment;
import com.aasthasolutions.bridalbargain.fragment.ProfileFragment;
import com.aasthasolutions.bridalbargain.fragment.ServiceDetailsFragment;
import com.aasthasolutions.bridalbargain.fragment.VendorServicesFragment;
import com.aasthasolutions.bridalbargain.model.Logout;
import com.aasthasolutions.bridalbargain.model.ServiceStatus;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.model.UserMessages;
import com.aasthasolutions.bridalbargain.model.VendorServices;
import com.aasthasolutions.bridalbargain.residemenu.MenuAdapter;
import com.aasthasolutions.bridalbargain.residemenu.ResideMenuItem;
import com.aasthasolutions.bridalbargain.residemenu.SlidingPaneLayout;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends BaseActivity{

    Context mContext;
    private LinearLayout ll_parent_cutomer_home, ll_home_nav, ll_add_product;
    private ImageView img_home_nav;

    Fragment currentFragment;
    boolean doubleBackToExitPressedOnce = false;
    TextView txt_home_title;

    public DrawerLayout mDrawerLayout;
    public ListView mLV_Menu;
    public ResideMenuItem item;
    public SlidingPaneLayout mSlidingPaneLayout;
    private String Message,clickAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_residemenu);

        mContext = this;

        isShowBackIcon = false;

        InitViews();
        SetClickListners();

        BaseActivity.USER_MODE = getUserMode();

        PrepareResideMenu(BaseActivity.USER_MODE);

//        Intent intent = getIntent();
//
//        int mode = intent.getIntExtra("mode", 0);

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                if(key.equalsIgnoreCase("click_action")){
                    clickAction = getIntent().getExtras().get(key).toString();
                }
            }
        }

        if (getMode() == 10 || clickAction != null && clickAction.equalsIgnoreCase("BRIDLE_HOME")) {
            clearModePref();
            changeFragment(new MessagesFragment());
        }else{
            if (BaseActivity.USER_MODE == 0) {
                BaseActivity.isFromVendor = false;
                changeFragment(new HomeMarketFragment());
//            changeFragment(new HomeSellerFragment());

            } else if (BaseActivity.USER_MODE == 1) {
                changeFragment(new HomeVendorFragment());
            } else if (BaseActivity.USER_MODE == 2) {
                changeFragment(new HomeMarketFragment());
            }
        }

        showAddProductLayout(false);
    }

    private void InitViews() {

        ll_home_nav = findViewById(R.id.ll_home_nav);
        ll_parent_cutomer_home = findViewById(R.id.ll_parent_cutomer_home);
        txt_home_title = findViewById(R.id.txt_home_title);
        ll_add_product = findViewById(R.id.ll_add_product);

        overrideFonts(ll_parent_cutomer_home, mContext);
        txt_home_title.setTypeface(getThemeFonts(mContext));

        img_home_nav = findViewById(R.id.img_home_nav);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.app_drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        mLV_Menu = (ListView) findViewById(R.id.left_navi_listview);

        mSlidingPaneLayout = (SlidingPaneLayout) findViewById(R.id.slidingpanel);
        mSlidingPaneLayout.setCloseAfterItemSelection(true);
    }

    private void SetClickListners() {

        ll_home_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                BaseActivity.hideKeyboard(HomeActivity.this);
                if (isShowBackIcon) {

                    Log.e("isShowBackIcon", " " + isShowBackIcon);
                    performBackTransaction();

                } else {
                    Log.e("isShowBackIcon", " " + isShowBackIcon);
                    if (!mSlidingPaneLayout.isOpen()) {
                        mSlidingPaneLayout.openPane();
                    }
                }

            }
        });

        ll_add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (BaseActivity.USER_MODE == 0) {
                    BaseActivity.IS_ADD_PRODUCT = true;
                    changeFragment(new AddProductFragment());
                } else if (BaseActivity.USER_MODE == 1) {
                    sendServiceStatusRequest();
                } else if (BaseActivity.USER_MODE == 2) {
                }
            }
        });
    }

    public void showBackButton(boolean isShow) {

        if (isShow) {
            img_home_nav.setImageResource(R.drawable.ic_back_white);
        } else {
            img_home_nav.setImageResource(R.drawable.ic_nav_menu);
        }
    }

    public void showAddProductLayout(boolean isShow) {
        if (isShow) {
            ll_add_product.setVisibility(View.VISIBLE);
        } else {
            ll_add_product.setVisibility(View.INVISIBLE);
        }
    }

    private void PrepareResideMenu(int mode) {

        final ArrayList<ResideMenuItem> list = getSlidingMenuItems(mode);

        MenuAdapter adapter = new MenuAdapter(this, list);
        mLV_Menu.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        mLV_Menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int
                    position, long id) {
                item = (ResideMenuItem) parent.getItemAtPosition(position);

                if (mSlidingPaneLayout.closeAfterItemSelection()) {
                    mSlidingPaneLayout.closePane();
                }

                onSideMenuClick(item);
            }
        });
    }

    public ArrayList<ResideMenuItem> getSlidingMenuItems(int mode) {

        final ArrayList<ResideMenuItem> list = new ArrayList<>();

        //For Seller 0, vendor 1, customer 2
        switch (mode) {
            case 0:
                if (getLogin() == 1) {

                    list.add(new ResideMenuItem("Home", R.drawable.ic_shop_home));
                    list.add(new ResideMenuItem("My Items", R.drawable.ic_shop_home));
                    list.add(new ResideMenuItem("Profile", R.drawable.ic_profile));
                    list.add(new ResideMenuItem("Change Password", R.drawable.ic_password));
                    list.add(new ResideMenuItem("Inbox", R.drawable.ic_inbox));
                    list.add(new ResideMenuItem("Logout", R.drawable.ic_logout));
                } else {
                    list.add(new ResideMenuItem("Home", R.drawable.ic_shop_home));
                    list.add(new ResideMenuItem("Login", R.drawable.ic_profile));
                }
                break;

            case 1:
                list.add(new ResideMenuItem("Home", R.drawable.ic_shop_home));
                list.add(new ResideMenuItem("Profile", R.drawable.ic_profile));
                list.add(new ResideMenuItem("Change Password", R.drawable.ic_password));
                list.add(new ResideMenuItem("Inbox", R.drawable.ic_inbox));
                list.add(new ResideMenuItem("My Plans", R.drawable.ic_payment));
                list.add(new ResideMenuItem("Contact", R.drawable.ic_contact));
                list.add(new ResideMenuItem("Logout", R.drawable.ic_logout));
                break;

            case 2:
                if (getLogin() == 1) {

                    list.add(new ResideMenuItem("Home", R.drawable.ic_shop_home));
                    list.add(new ResideMenuItem("Profile", R.drawable.ic_profile));
                    list.add(new ResideMenuItem("Change Password", R.drawable.ic_password));
                    list.add(new ResideMenuItem("Inbox", R.drawable.ic_inbox));
                    list.add(new ResideMenuItem("Logout", R.drawable.ic_logout));
                } else {
                    list.add(new ResideMenuItem("Home", R.drawable.ic_shop_home));
                    list.add(new ResideMenuItem("Login", R.drawable.ic_profile));
                }
                break;
        }

        return list;
    }

    public void onSideMenuClick(ResideMenuItem item) {

        switch (BaseActivity.USER_MODE) {
            case 0:
                if (item.getLabel().equals("Profile"))
                {
                    changeFragment(new ProfileFragment());
                }
                else if (item.getLabel().equals("My Items"))
                {
                    changeFragment(new HomeSellerFragment());
                }
                else if (item.getLabel().equals("Change Password"))
                {
                    changeFragment(new ChangePasswordFragment());
                }
                else if (item.getLabel().equals("Logout"))
                {
                    sendLogoutRequest();
                }
                else if (item.getLabel().equals("Home"))
                {
                    BaseActivity.isFromVendor = false;
                    changeFragment(new HomeMarketFragment());
                }
                else if (item.getLabel().equals("Inbox"))
                {
                    changeFragment(new MessagesFragment());
                }
                else if (item.getLabel().equals("Login"))
                {
                    Intent i = new Intent(mContext, AuthorizationTabActivity.class);
                    startActivity(i);
                }
                break;

            case 1:
                if (item.getLabel().equals("Profile"))
                {
                    changeFragment(new ProfileFragment());
                }
                else if (item.getLabel().equals("Change Password"))
                {
                    changeFragment(new ChangePasswordFragment());
                }
                else if (item.getLabel().equals("Logout"))
                {
                    sendLogoutRequest();
                }
                else if (item.getLabel().equals("Home"))
                {
                    changeFragment(new HomeVendorFragment());
                }
                else if (item.getLabel().equals("Inbox"))
                {
                    //For Vendor
                    changeFragment(new MessagesFragment());
                }
                else if(item.getLabel().equals("Contact"))
                {
                    changeFragment(new ContactFragment());
                }
                else if(item.getLabel().equals("My Plans"))
                {
                    Intent i = new Intent(mContext, MyPlansActivity.class);
                    i.putExtra("FROM_NOTIFICATION","HomeActivity");
                    startActivity(i);
                }
                break;

            case 2:
                if (item.getLabel().equals("Profile"))
                {
                    changeFragment(new ProfileFragment());
                }
                else if (item.getLabel().equals("Change Password"))
                {
                    changeFragment(new ChangePasswordFragment());
                }
                else if (item.getLabel().equals("Logout"))
                {
                    sendLogoutRequest();
                }
                else if (item.getLabel().equals("Home"))
                {
                    changeFragment(new HomeMarketFragment());
                }
                else if (item.getLabel().equals("Login"))
                {
                    Intent i = new Intent(mContext, AuthorizationTabActivity.class);
                    startActivity(i);
                }
                else if (item.getLabel().equals("Inbox"))
                {
                    changeFragment(new MessagesFragment());
                }
                break;
        }
    }



    private void sendServiceStatusRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = getUserDetails(mContext);

//        Log.e("LOGOUT", "calling sendBuyerMessages");
//        Log.e("LOGOUT", "IdUser  " + mUserDetails.getIdUser());

        Call<ServiceStatus> call = service.sendServiceStatusRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<ServiceStatus>() {
            @Override
            public void onResponse(Call<ServiceStatus> call, Response<ServiceStatus> response) {
                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        if(response.body().getStatus() == 1){

                            BaseActivity.IS_ADD_SERVICE = true;
                            changeFragment(new AddServiceFragment());

                        }else {


                            Message = response.body().getMessage();
                            showServiceAppDialog();
                        }

                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ServiceStatus> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void showServiceAppDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Service");
        builder.setMessage(""+Message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
//                finish();
            }
        });
//        builder.setNegativeButton("", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
        builder.show();

    }

    @Override
    public void onBackPressed() {

        performBackTransaction();

    }

    public void performBackTransaction() {
        FragmentManager fm = getSupportFragmentManager();

        try {
            if (fm.getBackStackEntryCount() > 1) {
//                fm.popBackStackImmediate();

                String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
                Log.e("MainActivity", "Current fragment tag is: " + tag);

                findTheFragmentWhilePoping(tag);
            } else {
                Log.i("MainActivity", "Nothing in back stack call super");
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();

                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(mContext, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findTheFragmentWhilePoping(String tag) {
        FragmentManager fm = getSupportFragmentManager();

        if (tag.equalsIgnoreCase(HomeMarketFragment.class.getName())) {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                finish();
                System.exit(0);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(mContext, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        } else if (tag.equalsIgnoreCase(HomeSellerFragment.class.getName())) {

            currentFragment = new HomeMarketFragment();

            /*if (doubleBackToExitPressedOnce) {
                super.onBackPressed();

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                System.exit(0);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(mContext, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);*/

        } else if (tag.equalsIgnoreCase(HomeVendorFragment.class.getName())) {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                System.exit(0);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(mContext, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        } else if (tag.equalsIgnoreCase(ProfileFragment.class.getName())) {

            switch (BaseActivity.USER_MODE) {

                case 0:
                    currentFragment = new HomeMarketFragment();

                    break;

                case 1:
                    currentFragment = new HomeVendorFragment();

                    break;

                case 2:
                    currentFragment = new HomeMarketFragment();

                    break;
            }

        } else if (tag.equalsIgnoreCase(ChangePasswordFragment.class.getName())) {

            switch (BaseActivity.USER_MODE) {

                case 0:
                    currentFragment = new HomeMarketFragment();
                    break;

                case 1:
                    currentFragment = new HomeVendorFragment();
                    break;

                case 2:
                    currentFragment = new HomeMarketFragment();
                    break;
            }

        } else if (tag.equalsIgnoreCase(MessagesFragment.class.getName())) {

            switch (BaseActivity.USER_MODE) {

                case 0:
                    currentFragment = new HomeMarketFragment();
                    break;

                case 1:
                    currentFragment = new HomeVendorFragment();
                    break;

                case 2:
                    currentFragment = new HomeMarketFragment();
                    break;
            }

        } else if (tag.equalsIgnoreCase(AddProductFragment.class.getName())) {

            showBackButton(false);
            isShowBackIcon = false;

            currentFragment = new HomeSellerFragment();

        } else if (tag.equalsIgnoreCase(AddServiceFragment.class.getName())) {

            showBackButton(false);
            isShowBackIcon = false;

            currentFragment = new HomeVendorFragment();

        } else if (tag.equalsIgnoreCase(VendorServicesFragment.class.getName())) {

            showBackButton(false);
            isShowBackIcon = false;
            BaseActivity.isFromVendor = true;
            currentFragment = new HomeMarketFragment();


        } else if (tag.equalsIgnoreCase(MarketProductFragment.class.getName())) {

            showBackButton(false);
            isShowBackIcon = false;
            BaseActivity.isFromVendor = false;

            currentFragment = new HomeMarketFragment();


        } else if (tag.equalsIgnoreCase(ProductDetailsFragment.class.getName())) {

            showBackButton(false);
            isShowBackIcon = false;

            currentFragment = new MarketProductFragment();

        } else if (tag.equalsIgnoreCase(ServiceDetailsFragment.class.getName())) {

            showBackButton(false);
            isShowBackIcon = false;

            currentFragment = new VendorServicesFragment();

        } else if (tag.equalsIgnoreCase(ChatingFragment.class.getName())) {

            showBackButton(false);
            isShowBackIcon = false;

            currentFragment = new MessagesFragment();

        } else if (tag.equalsIgnoreCase(MessagesFragment.class.getName())) {


            switch (BaseActivity.USER_MODE) {

                case 0:
                    currentFragment = new HomeSellerFragment();
                    break;

                case 1:
                    currentFragment = new HomeVendorFragment();
                    break;

                case 2:
                    currentFragment = new HomeMarketFragment();
                    break;
            }

        }  else if (tag.equalsIgnoreCase(ContactFragment.class.getName())) {

            switch (BaseActivity.USER_MODE) {

                case 0:
                    currentFragment = new HomeSellerFragment();
                    break;

                case 1:
                    currentFragment = new HomeVendorFragment();
                    break;

                case 2:
                    currentFragment = new HomeMarketFragment();
                    break;
            }

        }
//        else if (tag.equalsIgnoreCase(MyPlansFragment.class.getName())) {
//
//            showBackButton(false);
//            isShowBackIcon = false;
//
//            currentFragment = new HomeVendorFragment();
//
//        }



        changeFragment(currentFragment);

    }

    public void changeFragment(Fragment targetFragment) {
//        mResideMenu.clearIgnoredViewList();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, targetFragment, targetFragment.getClass().getName());
        transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(targetFragment.getClass().getName());
//        transaction.detach(targetFragment);
//        transaction.attach(targetFragment);
//        transaction.commit();
        transaction.commitAllowingStateLoss();

        currentFragment = targetFragment;
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void sendLogoutRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = getUserDetails(mContext);

        Log.e("LOGOUT", "calling sendBuyerMessages");
        Log.e("LOGOUT", "IdUser  " + mUserDetails.getIdUser());

        Call<Logout> call = service.sendLogoutRequest(mUserDetails.getIdUser(), String.valueOf(BaseActivity.USER_MODE));

        call.enqueue(new Callback<Logout>() {
            @Override
            public void onResponse(Call<Logout> call, Response<Logout> response) {
                showWaitIndicator(false);

                Log.e("LOGOUT", "MESSAGES_API Respnse : " + new Gson().toJson(response.body()));

                try {
                    if (response.code() == 200) {

                        setLogin(0);
                        Intent i = new Intent(mContext, SimpleIntroActivity.class);
                        startActivity(i);
                        finish();

                    }

                } catch (Exception e) {
                    Log.d("onResponse", "There is an error: " + e);
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Logout> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("LOGOUT", "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
