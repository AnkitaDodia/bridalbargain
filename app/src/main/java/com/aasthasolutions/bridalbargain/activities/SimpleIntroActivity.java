package com.aasthasolutions.bridalbargain.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.adapter.IntroPagerAdapter;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.Banner;
import com.aasthasolutions.bridalbargain.model.BannerTitle;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SimpleIntroActivity extends BaseActivity {

    SimpleIntroActivity mContext;

    private LinearLayout intro_pager_dots, ll_bs_seller, ll_bs_vendor;//ll_bs_customer
    private LinearLayout ll_parent_simple_intro;
    private ViewPager intro_view_pager;
    private IntroPagerAdapter mIntroPagerAdapter;
    private ImageView[] ivArrayDotsPager;
    private TextView txt_skip_sign_in;

    private int dotscount;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 100;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 4000; // time in milliseconds between successive task executions.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_intro);

        mContext = this;

        InitViews();
        SetClickListners();

//        if (BaseActivity.CheckInternet(mContext)) {
//            sendBannerTitlesRequest();
//        } else {
//            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
//        }


        InitializeViewPager();

    }

    private void InitViews() {

        intro_pager_dots = findViewById(R.id.intro_pager_dots);
        intro_view_pager = findViewById(R.id.intro_view_pager);
        ll_parent_simple_intro = findViewById(R.id.ll_parent_simple_intro);

        ll_bs_seller = findViewById(R.id.ll_bs_seller);
        ll_bs_vendor = findViewById(R.id.ll_bs_vendor);
//        ll_bs_customer = findViewById(R.id.ll_bs_customer);

        txt_skip_sign_in = findViewById(R.id.txt_skip_sign_in);

        overrideFonts(ll_parent_simple_intro, mContext);
    }

    private void SetClickListners(){

        ll_bs_seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.USER_MODE = 0;
                setUserMode(0);
                Intent i = new Intent(mContext, AuthorizationTabActivity.class);
                startActivity(i);
                finish();
            }
        });

        ll_bs_vendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.USER_MODE = 1;
                setUserMode(1);
                Intent i = new Intent(mContext, AuthorizationTabActivity.class);
                startActivity(i);
                finish();

            }
        });

        txt_skip_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.USER_MODE = 0;
                setUserMode(0);
                Intent i = new Intent(mContext, HomeActivity.class);
                startActivity(i);
                finish();

            }
        });

//        ll_bs_customer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                BaseActivity.USER_MODE = 2;
//                setUserMode(2);
//                Intent i = new Intent(mContext, HomeActivity.class);
//                startActivity(i);
//                finish();
//            }
//        });

    }


    private void sendBannerTitlesRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);


        Call<Banner> call = service.sendBannerTitlesRequest();

        call.enqueue(new Callback<Banner>() {
            @Override
            public void onResponse(Call<Banner> call, Response<Banner> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        String BannerText = response.body().getData().getBanner();
                        String[] SplitTexts = BannerText.split("\\|");

                        for(int i =0 ; i < SplitTexts.length ; i++) {

                            BannerTitle mBannerTitle = new BannerTitle();
                            mBannerTitle.setImageId(R.drawable.img_intro_bg);
                            mBannerTitle.setTitle(SplitTexts[i]);

                            Log.e("SplitTexts","SplitTexts : "+SplitTexts[i]);
                            mInro_Images_List.add(mBannerTitle);

                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Banner> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void InitializeViewPager() {

        mInro_Images_List.clear();
        String BannerText = getIntroScreenText();
        String[] SplitTexts = BannerText.split("\\|");

        for(int i =0 ; i < SplitTexts.length ; i++) {

            BannerTitle mBannerTitle = new BannerTitle();
            mBannerTitle.setImageId(R.drawable.img_intro_bg);
            mBannerTitle.setTitle(SplitTexts[i]);

            mInro_Images_List.add(mBannerTitle);

        }


//        if(mInro_Images_List.size() > 0){

            mIntroPagerAdapter = new IntroPagerAdapter(mContext, mInro_Images_List);
            intro_view_pager.setAdapter(mIntroPagerAdapter);
//        }


        dotscount = mIntroPagerAdapter.getCount();
        ivArrayDotsPager = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {

            ivArrayDotsPager[i] = new ImageView(mContext);
            ivArrayDotsPager[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.page_indicator_unselected));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            intro_pager_dots.addView(ivArrayDotsPager[i], params);

        }

        ivArrayDotsPager[0].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.page_indicator_selected));

        intro_view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    ivArrayDotsPager[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.page_indicator_unselected));
                }

                ivArrayDotsPager[position].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.page_indicator_selected));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == dotscount) {
                    currentPage = 0;
                }
                intro_view_pager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);
    }



}
