package com.aasthasolutions.bridalbargain.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.Banner;
import com.aasthasolutions.bridalbargain.model.BannerTitle;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.aasthasolutions.bridalbargain.utility.TypeFaces;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashScreenActivity extends BaseActivity {

    Context mContext;

    private static int SPLASH_TIME_OUT = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mContext = this;

        if (BaseActivity.CheckInternet(mContext)) {
            sendBannerTitlesRequest();
        } else {
            showConnectionDialog();
//            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }


    }

    private void sendBannerTitlesRequest() {

//        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);


        Call<Banner> call = service.sendBannerTitlesRequest();

        call.enqueue(new Callback<Banner>() {
            @Override
            public void onResponse(Call<Banner> call, Response<Banner> response) {

//                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {



                        String BannerText = response.body().getData().getBanner();
                        setIntroScreenText(BannerText);


                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {

                                SwitchtoNext();
                            }
                        }, SPLASH_TIME_OUT);

                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
//                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Banner> call, Throwable t) {
//                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void SwitchtoNext(){
        if(getLogin() == 0){

            Intent i = new Intent(mContext, SimpleIntroActivity.class);
            startActivity(i);
            // close this activity
            finish();

        }else {
            BaseActivity.USER_MODE = getUserMode();

            if(getUserMode() == 0){

                Intent i = new Intent(mContext, HomeActivity.class);
                startActivity(i);

            }else if(getUserMode() == 1){

                Intent i = new Intent(mContext, HomeActivity.class);
                startActivity(i);

            }else if(getUserMode() == 2){

                Intent i = new Intent(mContext, HomeActivity.class);
                startActivity(i);

            }else{

            }

        }
    }

    private void showConnectionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("No connection");
        builder.setMessage("No internet connection found. Please try again later.");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
//        builder.setNegativeButton("", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
        builder.show();

    }
}
