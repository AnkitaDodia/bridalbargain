package com.aasthasolutions.bridalbargain.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.MyPlans;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyPlansActivity extends BaseActivity {

    final String TAG = "PAYMENT";
    MyPlansActivity mContext;

    LinearLayout ll_parent_myplan, ll_back_plan;
    TextView txt_payment_start_date, txt_payment_end_date, txt_payment_amount, txt_next_payment_date;
    Button btn_pay_in_advance;

    private String mFrom_Notification = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_myplans);

        mContext = this;

        initViews();
        SetClickListner();

        mFrom_Notification = getIntent().getStringExtra("FROM_NOTIFICATION");
        Log.e("FROM_NOTIFICATION","mFrom_Where : "+mFrom_Notification);

        if(mFrom_Notification == null){

        }else if (mFrom_Notification.equalsIgnoreCase("HomeActivity")){


        }else {

            if(getLogin() == 0){

                BaseActivity.USER_MODE = 1;
                setUserMode(1);
                Intent i = new Intent(mContext, AuthorizationTabActivity.class);
                startActivity(i);
                finish();
            }
        }

        if (BaseActivity.CheckInternet(mContext)) {
            sendMyPlansRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void initViews() {

        ll_parent_myplan = findViewById(R.id.ll_parent_myplan);
        txt_payment_start_date = findViewById(R.id.txt_payment_start_date);
        txt_payment_end_date = findViewById(R.id.txt_payment_end_date);
        txt_payment_amount = findViewById(R.id.txt_payment_amount);

        txt_next_payment_date = findViewById(R.id.txt_next_payment_date);
        btn_pay_in_advance = findViewById(R.id.btn_pay_in_advance);

        ll_back_plan = findViewById(R.id.ll_back_plan);

        mContext.overrideFonts(ll_parent_myplan, mContext);

    }

    private void SetClickListner() {


        btn_pay_in_advance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserDetails mUserDetails = mContext.getUserDetails(mContext);
                String mIdUser = mUserDetails.getIdUser();

                Intent i = new Intent(mContext, PaymentActivity.class);
                i.putExtra("USER_ID", mIdUser);
                i.putExtra("FROM_WHERE", "MyPlans");
                startActivity(i);
            }
        });

        ll_back_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, HomeActivity.class);
                startActivity(i);
            }
        });
    }

    private void sendMyPlansRequest() {

        mContext.showWaitIndicator(true);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);
        String mIdUser = mUserDetails.getIdUser();

        Log.e(TAG, "Mode : " + BaseActivity.USER_MODE);
        Log.e(TAG, "mIdUser : " + mIdUser);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<MyPlans> call = service.sendMyPlansRequest(mIdUser);

        call.enqueue(new Callback<MyPlans>() {
            @Override
            public void onResponse(Call<MyPlans> call, Response<MyPlans> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {
                        Log.e(TAG, "RESPONSE : " + response.body().getMessage());

//                        Log.e(TAG, "RESPONSE : " + response.body().getData().getIdTransaction());

                        txt_payment_start_date.setText("Renews on "+ChangeDateFormate(response.body().getData().getPaymentDate()));
//                            txt_payment_end_date.setText(response.body().getData().getIsPaymentExpired());
                        txt_payment_amount.setText("$"+response.body().getData().getPaymentAmount());
                        txt_next_payment_date.setText("Expires "+ChangeDateFormate(response.body().getData().getNextPaymentDate()));

                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MyPlans> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public static String ChangeDateFormate(String inputDateString) {

        Date date = null;
        String outputDateString = null;

        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("MMM dd, yyyy");

        try {
            date = inputDateFormat.parse(inputDateString);
            outputDateString = outputDateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputDateString;

    }
}
