package com.aasthasolutions.bridalbargain.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.common.BaseActivity;

public class NotificationViewActivity extends BaseActivity {
    TextView notification_text,textview_title,text_title;

    RelativeLayout btnClose;

    LinearLayout mLayoutMain;

    String msg,title;

    NotificationViewActivity mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notification_viwe);

        mContext = this;

        initView();
        initClick();

        msg = getIntent().getStringExtra("body");
        title = getIntent().getExtras().getString("title");

        text_title.setText(title);
        notification_text.setText(msg);
    }

    private void initView(){
        btnClose = findViewById(R.id.btnClose);

        notification_text = findViewById(R.id.notification_text);
        textview_title = findViewById(R.id.textview_title);
        text_title = findViewById(R.id.text_title);

        mLayoutMain = findViewById(R.id.layout_notification_main);

        textview_title.setTypeface(getRegularBoldFonts(mContext));
        text_title.setTypeface(getRegularBoldFonts(mContext));
        notification_text.setTypeface(getRegularFonts(mContext));
    }

    private void initClick(){
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(NotificationViewActivity.this,HomeActivity.class);
                startActivity(it);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it = new Intent(NotificationViewActivity.this,HomeActivity.class);
        startActivity(it);
        finish();
    }
}
