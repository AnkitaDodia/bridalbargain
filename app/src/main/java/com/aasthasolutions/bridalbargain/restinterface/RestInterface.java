package com.aasthasolutions.bridalbargain.restinterface;


import com.aasthasolutions.bridalbargain.model.AddProduct;
import com.aasthasolutions.bridalbargain.model.AddService;
import com.aasthasolutions.bridalbargain.model.Banner;
import com.aasthasolutions.bridalbargain.model.EditProfile;
import com.aasthasolutions.bridalbargain.model.ForgotPassword;
import com.aasthasolutions.bridalbargain.model.GetSellers;
import com.aasthasolutions.bridalbargain.model.GetVendors;
import com.aasthasolutions.bridalbargain.model.Login;
import com.aasthasolutions.bridalbargain.model.Logout;
import com.aasthasolutions.bridalbargain.model.MyPlans;
import com.aasthasolutions.bridalbargain.model.PayMent;
import com.aasthasolutions.bridalbargain.model.SellerProduct;
import com.aasthasolutions.bridalbargain.model.SellerSingleProduct;
import com.aasthasolutions.bridalbargain.model.SendMessages;
import com.aasthasolutions.bridalbargain.model.ServiceStatus;
import com.aasthasolutions.bridalbargain.model.SignUp;
import com.aasthasolutions.bridalbargain.model.SingleChating;
import com.aasthasolutions.bridalbargain.model.StatusMessages;
import com.aasthasolutions.bridalbargain.model.UserMessages;
import com.aasthasolutions.bridalbargain.model.VendorServices;
import com.aasthasolutions.bridalbargain.model.VendorSignUp;
import com.aasthasolutions.bridalbargain.model.VendorSingleService;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;


/**
 * Created by codfidea on 09/22/2016.
 */
public interface RestInterface
{
    public static String API_BASE_URL = "https://admin.bridalbargain.net";

    String LOGIN = "/Api/Login";
    String SELLER_SIGNUP = "/Api/SignUp";
    String VENDOR_SIGNUP = "Api/vendorSignUp";

    String PAYMENT = "/Api/payment_init";

    String FORGOT_PASSWORD = "/Api/ForgotPassword";

    String GET_VENDORS = "/Api/GetVendors";
    String GET_SELLERS = "/Api/GetSellers";
    String VENDOR_SERVICES = "/Api/VendorServices";
    String VENDOR_SINGLE_SERVICES = "/Api/SingleService";
    String SELLER_PRODUCT = "/Api/SellerProducts";
    String SELLER_SINGLE_PRODUCT = "/Api/SingleProduct";
    String BANNER_TITLES = "/Api/GetBannerText";
    String UPDATE_PROFILE = "/Api/editProfile";

    String PRODUCT_BY_SELLER = "/Api/productsBySeller";
    String ADD_PRODUCT = "/Api/addProduct";
    String EDIT_PRODUCT = "/Api/editProduct";
    String DELETE_PRODUCT = "/Api/deleteProduct";

    String SERVICE_BY_VENDOR = "/Api/VendorsServices";
    String ADD_SERVICE = "/Api/addService";
    String EDIT_SERVICE = "/Api/editService";
    String DELETE_SERVICE = "/Api/deleteService";

    String CHANGE_PAASWORD = "/Api/changePassword";

    String GET_BUYER_MESSAGES = "/Api/GetBuyerMessages";
    String GET_SELLER_MESSAGES = "/Api/GetSellerMessages";
    String GET_VENDOR_MESSAGES = "/Api/GetVendorMessages";
    String GET_BUYER_SINGLE_MESSAGES = "/Api/GetBuyerSingleMsg";
    String GET_VENDOR_SELLER_SINGLE_MESSAGES = "/Api/GetVsSingleMsg";
    String BLOCK_USER_MESSAGES = "/api/BlockUser";
    String UNBLOCK_USER_MESSAGES = "/api/UnBlockUser";

    String SEND_BUYER_MESSAGES = "/Api/BuyerSendMessage";
    String SEND_MESSAGES_VS = "/Api/SendMessageVS";
    String LOGOUT = "/Api/logout";
    String SEARCH_API = "/Api/search";
    String SERVICE_STATUS = "/Api/CheckServiceStatus";
    String SEARCH_PRODUCT = "/Api/ProductSeach";


    String MY_PLAN = "/Api/paymentPlan";

    @FormUrlEncoded
    @POST(LOGIN)
    Call<Login> sendLoginRequest(@Field("email") String email, @Field("password") String password, @Field("mode") int mode, @Field("fcm") String fcm);

    @FormUrlEncoded
    @POST(FORGOT_PASSWORD)
    Call<ForgotPassword> sendForgotPasswordRequest(@Field("email") String email, @Field("mode") int mode);


    @Multipart
    @POST(SELLER_SIGNUP)
    Call<SignUp> sendSellerSignUpRequest (
            @Part MultipartBody.Part imageFile,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("address") RequestBody address,
            @Part("city") RequestBody city,
            @Part("phone") RequestBody phone,
            @Part("mode") RequestBody mode);

    @Multipart
    @POST(VENDOR_SIGNUP)
    Call<VendorSignUp> sendVendorSignUpRequest (
            @Part MultipartBody.Part imageFile,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("address") RequestBody address,
            @Part("city") RequestBody city,
            @Part("phone") RequestBody phone);

    @FormUrlEncoded
    @POST(PAYMENT)
    Call<PayMent> sendCardNonceRequest(@Field("id_vendor") String id_vendor, @Field("nonce") String nonce);


    @FormUrlEncoded
    @POST(MY_PLAN)
    Call<MyPlans> sendMyPlansRequest(@Field("id_vendor") String id_vendor);

    @Multipart
    @POST(UPDATE_PROFILE)
    Call<EditProfile> sendUpdateProfileRequest (
            @Part MultipartBody.Part imageFile,
            @PartMap Map<String, RequestBody> options);

    @GET(GET_VENDORS)
    Call<GetVendors> sendGetVendorsRequest();

    @FormUrlEncoded
    @POST(VENDOR_SERVICES)
    Call<VendorServices> sendVendorServicesRequest(@Field("id_category") String id_category, @Field("id_vendor") String id_vendor);


    @FormUrlEncoded
    @POST(VENDOR_SINGLE_SERVICES)
    Call<VendorSingleService> sendVendorSingleServicesRequest(@Field("id_service") String id_service);


    @GET(GET_SELLERS)
    Call<GetSellers> sendGetSellersRequest();

    @FormUrlEncoded
    @POST(SELLER_PRODUCT)
    Call<SellerProduct> sendSellerProductRequest(@Field("id_category") String id_category, @Field("id_seller") String id_seller);


    @FormUrlEncoded
    @POST(SELLER_SINGLE_PRODUCT)
    Call<SellerSingleProduct> sendSellerSingleProductRequest(@Field("id_product") String id_category);

    @GET(BANNER_TITLES)
    Call<Banner> sendBannerTitlesRequest();

    @FormUrlEncoded
    @POST(PRODUCT_BY_SELLER)
    Call<SellerProduct> sendProductBySellerRequest(@Field("id_seller") String id_seller);

    @FormUrlEncoded
    @POST(DELETE_PRODUCT)
    Call<AddProduct> sendDeleteProductRequest(@Field("id_product") String id_product);


    @Multipart
    @POST(ADD_PRODUCT)
    Call<AddProduct> sendAddProductRequest (
            @Part("id_seller") RequestBody id_seller,
            @Part("id_category") RequestBody id_category,
            @Part("product_name") RequestBody product_name,
            @Part("product_price") RequestBody product_price,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part imageFile);

//    @Part("product_img") RequestBody product_img

    @Multipart
    @POST(EDIT_PRODUCT)
    Call<AddProduct> sendEditProductRequest (
            @Part("id_product") RequestBody id_product,
            @Part("id_seller") RequestBody id_seller,
            @Part("id_category") RequestBody id_category,
            @Part("product_name") RequestBody product_name,
            @Part("product_price") RequestBody product_price,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part imageFile);

//    @Part("product_img") RequestBody product_img

    @FormUrlEncoded
    @POST(SERVICE_BY_VENDOR)
    Call<VendorServices> sendServiceByVendorRequest(@Field("id_vendor") String id_seller);

    @FormUrlEncoded
    @POST(CHANGE_PAASWORD)
    Call<Login> sendChangePasswordRequest(@Field("user_id") String user_id, @Field("old_password") String old_password, @Field("new_password") String new_password, @Field("confirm_password") String confirm_password, @Field("mode") int mode);

    @Multipart
    @POST(ADD_SERVICE)
    Call<AddService> sendAddServicesRequest (
            @Part("id_vendor") RequestBody id_seller,
            @Part("id_category") RequestBody id_category,
            @Part("service_name") RequestBody product_name,
            @Part("service_price") RequestBody product_price,
            @Part("description") RequestBody service_description,
            @Part("zip") RequestBody zip,
            @Part MultipartBody.Part imageFile); //service_img



    @Multipart
    @POST(EDIT_SERVICE)
    Call<AddService> sendEditServiceRequest (
            @Part("id_service") RequestBody id_service,
            @Part("id_vendor") RequestBody id_vendor,
            @Part("id_category") RequestBody id_category,
            @Part("service_name") RequestBody service_name,
            @Part("service_price") RequestBody service_price,
            @Part("description") RequestBody service_description,
            @Part("zip") RequestBody zip,
            @Part MultipartBody.Part imageFile);


    @FormUrlEncoded
    @POST(DELETE_SERVICE)
    Call<AddService> sendDeleteServiceRequest(@Field("id_service") String id_service);


    @FormUrlEncoded
    @POST(GET_BUYER_MESSAGES)
    Call<UserMessages> sendBuyerMessagesRequest(@Field("id_seller") String id_buyer);

    @FormUrlEncoded
    @POST(GET_SELLER_MESSAGES)
    Call<UserMessages> sendSellerMessagesRequest(@Field("id_seller") String id_seller);

    @FormUrlEncoded
    @POST(GET_VENDOR_MESSAGES)
    Call<UserMessages> sendVendorMessagesRequest(@Field("id_vendor") String id_vendor);

    @FormUrlEncoded
    @POST(GET_BUYER_SINGLE_MESSAGES)
    Call<SingleChating> sendBuyerSingleMessagesRequest(@Field("id_seller") String id_seller, @Field("id_user") String id_user , @Field("role") String role);

    @FormUrlEncoded
    @POST(GET_VENDOR_SELLER_SINGLE_MESSAGES)
    Call<SingleChating> sendSingleMessagesVendorSellerRequest(@Field("id_seller") String id_buyer, @Field("id_user") String id_user , @Field("role") String role);


    @FormUrlEncoded
    @POST(SEND_BUYER_MESSAGES)
    Call<SendMessages> sendBuyerSendMessagesRequest(@Field("id_seller") String id_seller, @Field("to") String to , @Field("role") String role , @Field("message") String message);


    @FormUrlEncoded
    @POST(SEND_MESSAGES_VS)
    Call<SendMessages> sendMessagesVSRequest(@Field("from") String from, @Field("id_seller") String id_buyer, @Field("role") String role , @Field("message") String message);


    @FormUrlEncoded
    @POST(BLOCK_USER_MESSAGES)
    Call<StatusMessages> sendBlockUserRequest(@Field("id_from") String id_from, @Field("role_from") String role_from, @Field("id_to") String id_to , @Field("role_to") String role_to);


    @FormUrlEncoded
    @POST(UNBLOCK_USER_MESSAGES)
    Call<StatusMessages> sendUnBlockUserRequest(@Field("id_from") String id_from, @Field("role_from") String role_from, @Field("id_to") String id_to , @Field("role_to") String role_to);



    @FormUrlEncoded
    @POST(LOGOUT)
    Call<Logout> sendLogoutRequest(@Field("id_user") String id_user, @Field("role") String role);


    @FormUrlEncoded
    @POST(SEARCH_API)
    Call<VendorServices> sendSearchByZipRequest(@Field("zip") String zip, @Field("id_category") String id_category);

    @FormUrlEncoded
    @POST(SERVICE_STATUS)
    Call<ServiceStatus> sendServiceStatusRequest(@Field("id_vendor") String id_vendor);

    @FormUrlEncoded
    @POST(SEARCH_PRODUCT)
    Call<SellerProduct> sendSearchProductRequest(@Field("search") String search, @Field("id_category") String id_category);

}
