package com.aasthasolutions.bridalbargain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SellerSingleProduct {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private SellerSingleProductData data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public SellerSingleProductData getData() {
        return data;
    }

    public void setData(SellerSingleProductData data) {
        this.data = data;
    }

}
