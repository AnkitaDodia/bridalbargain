package com.aasthasolutions.bridalbargain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SingleChating {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("block")
    @Expose
    private Integer block;
    @SerializedName("data")
    @Expose
    private ArrayList<SingleChatingData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<SingleChatingData> getData() {
        return data;
    }

    public void setData(ArrayList<SingleChatingData> data) {
        this.data = data;
    }

    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }
}
