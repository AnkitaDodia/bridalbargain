package com.aasthasolutions.bridalbargain.model;


public class UserDetails {

    private String idUser;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String address;
    private String city;
    private String profile_img;
    private String phonenumber;


    public void UserDetails(String midUser, String mfirstName, String mlastName, String memail, String maddress, String mcity, String mphonenumber, String mprofile_img){

        idUser = midUser;
        firstName = mfirstName;
        lastName = mlastName;
        email = memail;
        address = maddress;
        city = mcity;
        profile_img = mprofile_img;
        phonenumber = mphonenumber;

    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idVendor) {
        this.idUser = idVendor;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProfileImage() {
        return profile_img;
    }

    public void setProfileImage(String profile_img) {
        this.profile_img = profile_img;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

}
