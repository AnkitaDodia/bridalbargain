package com.aasthasolutions.bridalbargain.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.adapter.ProductBySellerAdapter;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.AddProduct;
import com.aasthasolutions.bridalbargain.model.SellerProduct;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HomeSellerFragment extends Fragment {

    HomeActivity mContext;
    RecyclerView rv_vendors;
    LinearLayout ll_parent_vendors, linear_search_layout;
    ProductBySellerAdapter mProductBySellerAdapter;
    TextView txt_empty_vendors;

    String mProductId;

    public HomeSellerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vendors, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        InitViews(view);

        if (BaseActivity.CheckInternet(mContext)) {
            sendProductBySellerRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }

        mContext.showAddProductLayout(true);
    }

    private void InitViews(View view){

        ll_parent_vendors = view.findViewById(R.id.ll_parent_vendors);
        rv_vendors = view.findViewById(R.id.rv_vendors);
        txt_empty_vendors = view.findViewById(R.id.txt_empty_vendors);
        ll_parent_vendors = view.findViewById(R.id.ll_parent_vendors);

        linear_search_layout = view.findViewById(R.id.linear_search_layout);
        linear_search_layout.setVisibility(View.GONE);
        rv_vendors.setHasFixedSize(true);

//        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
//        rv_vendors.addItemDecoration(new SpacesItemDecoration(2));
        rv_vendors.setLayoutManager(new GridLayoutManager(mContext, 2));

        ll_parent_vendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    private void sendProductBySellerRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);


        Log.e("SELLER_ID", "RESPONSE : " + mUserDetails.getIdUser());

        Call<SellerProduct> call = service.sendProductBySellerRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<SellerProduct>() {
            @Override
            public void onResponse(Call<SellerProduct> call, Response<SellerProduct> response) {
                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        if (response.body().getStatus() == 1) {

                            rv_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setVisibility(View.GONE);

                            BaseActivity.mGetSellerProductDataList.clear();
                            for (int i = 0; i < response.body().getData().size(); i++) {

                                BaseActivity.mGetSellerProductDataList.add(response.body().getData().get(i));
                                Log.e("PRODUCT_BY_SELLER", "RESPONSE : " + response.body().getData().get(i).getProductImg());
                            }

//                            ll_parent_vendors.setBackgroundColor(Color.parseColor("#33406A"));
                            PrepareAdapter();
//                            mSellerProductAdapter = new  SellerProductAdapter(mContext, mGetSellerProductDataList);
//                            rv_vendors.setAdapter(mSellerProductAdapter);

                        } else {

                            rv_vendors.setVisibility(View.GONE);
                            txt_empty_vendors.setVisibility(View.VISIBLE);
//
                            txt_empty_vendors.setText(response.body().getMessage());
                        }

                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SellerProduct> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void PrepareAdapter() {
        mProductBySellerAdapter = new ProductBySellerAdapter(mContext, BaseActivity.mGetSellerProductDataList, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View v, int position) {

                if (v.getId() == R.id.ll_delete_product) {

//                    Log.e("",""+mGetSellerProductDataList.get(position).getProductPrice());

                    mProductId = BaseActivity.mGetSellerProductDataList.get(position).getIdProduct();

                    showDeleteDialog();

//                    Toast.makeText(mContext, "Click Delete Product : "+mProductId, Toast.LENGTH_SHORT).show();
                }  else if (v.getId() == R.id.ll_edit_product) {

//                    Toast.makeText(mContext, "Click Edit Product", Toast.LENGTH_SHORT).show();

                    BaseActivity.SELLER_PRODUCT_POSITION = position;
//                    BaseActivity.SELLER_PRODUCT_ID = BaseActivity.mGetSellerProductDataList.get(position).getIdProduct();
                    BaseActivity.IS_ADD_PRODUCT = false;
                    mContext.changeFragment(new AddProductFragment());

                } else if (v.getId() == R.id.ll_row_product_by_seller) {

////                    Toast.makeText(mContext, "Click Parent", Toast.LENGTH_SHORT).show();
//
//                    BaseActivity.IS_ADD_PRODUCT = true;
//                    mContext.changeFragment(new AddProductFragment());

                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });


        rv_vendors.setAdapter(mProductBySellerAdapter);
    }

    private void sendDeleteProductRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Log.e("DELETE_PRODUCT", "mProductId : " +mProductId);

        Call<AddProduct> call = service.sendDeleteProductRequest(mProductId);

        call.enqueue(new Callback<AddProduct>() {
            @Override
            public void onResponse(Call<AddProduct> call, Response<AddProduct> response) {
                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        if (response.body().getStatus() == 1) {

                            sendProductBySellerRequest();

                            Log.e("DELETE_PRODUCT", "RESPONSE : " + response.body().getMessage());
                            Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        } else {
                            Log.e("DELETE_PRODUCT", "RESPONSE ELSE: " + response.body().getMessage());
                            Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddProduct> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Delete product");
        builder.setMessage("Are you sure you want to delete this product?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                sendDeleteProductRequest();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
}
