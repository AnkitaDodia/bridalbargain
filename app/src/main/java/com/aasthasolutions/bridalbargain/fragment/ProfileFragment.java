package com.aasthasolutions.bridalbargain.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.CropActivity;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.EditProfile;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.aasthasolutions.bridalbargain.utility.CameraUtils;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ProfileFragment extends Fragment {

    HomeActivity mContext;

    LinearLayout ll_parent_profile;
    Button btn_profile_update;
    ImageView img_profile_pic;
    TextInputEditText tedt_signin_firstname, tedt_signin_lastname, tedt_signin_address, tedt_signin_city, tedt_signin_phone, tedt_signin_email;
    String mIdUser;

    //For image selection from gallery
    private static final int PICK_IMAGE = 1;
    private static final int CROP_IMAGE = 2;
    public static final int CAMERA_CAPTURE = 100;
    Bitmap bitmap;
    String mCurrentPhotoPath, filemanagerstring, selectedImagePath = null;
    Uri selectedImageUri;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        initViews(view);
        SetListeners();
        mContext.showAddProductLayout(false);


    }

    private void initViews(View view) {

        ll_parent_profile = view.findViewById(R.id.ll_parent_profile);

        img_profile_pic = view.findViewById(R.id.img_profile_pic);

        tedt_signin_firstname = view.findViewById(R.id.tedt_signin_firstname);
        tedt_signin_lastname = view.findViewById(R.id.tedt_signin_lastname);
        tedt_signin_address = view.findViewById(R.id.tedt_signin_address);
        tedt_signin_city = view.findViewById(R.id.tedt_signin_city);
        tedt_signin_phone = view.findViewById(R.id.tedt_signin_phone);
        tedt_signin_email = view.findViewById(R.id.tedt_signin_email);

        btn_profile_update = view.findViewById(R.id.btn_profile_update);

        mContext.overrideFonts(ll_parent_profile, mContext);

        setLocalData();
    }


    private void SetListeners() {

        tedt_signin_address.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (tedt_signin_address.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });

        img_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                requestStoragePermission();
                ShowPicOption();


            }
        });

        ll_parent_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btn_profile_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(tedt_signin_firstname.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "Please enter first name", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(tedt_signin_lastname.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "Please enter last name", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(tedt_signin_address.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "Please enter address", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(tedt_signin_city.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "Please enter city", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(tedt_signin_phone.getText().toString().trim())) {
//                    edt_login_password.setError("Please enter valid password");
                    Toast.makeText(mContext, "Please enter phone number", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(tedt_signin_email.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "Please enter email address", Toast.LENGTH_LONG).show();
                } else if (!BaseActivity.isValidEmail(tedt_signin_email.getText().toString())) {
//                    edt_login_email.setError("Please enter valid email");
                    Toast.makeText(mContext, "Please enter a valid email address", Toast.LENGTH_LONG).show();
                }

                else {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendUpdateProfileRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }


    private void setLocalData() {

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        if (mUserDetails != null) {

            mIdUser = mUserDetails.getIdUser();

            tedt_signin_firstname.setText(mUserDetails.getFirstName());
            tedt_signin_lastname.setText(mUserDetails.getLastName());
            tedt_signin_address.setText(mUserDetails.getAddress());
            tedt_signin_city.setText(mUserDetails.getCity());
            tedt_signin_email.setText(mUserDetails.getEmail());
            tedt_signin_phone.setText(mUserDetails.getPhonenumber());
//            edt_profile_password.setText(mUserDetails.getPassword());

            tedt_signin_firstname.setSelection(mUserDetails.getFirstName().length());
            tedt_signin_lastname.setSelection(mUserDetails.getLastName().length());
            tedt_signin_address.setSelection(mUserDetails.getAddress().length());
            tedt_signin_city.setSelection(mUserDetails.getCity().length());
            tedt_signin_email.setSelection(mUserDetails.getEmail().length());
            tedt_signin_phone.setSelection(mUserDetails.getPhonenumber().length());

            Log.e("img_profile_pic","img_profile_pic : "+mUserDetails.getProfileImage());
            Log.e("img_profile_pic","mIdUser : "+mUserDetails.getIdUser());

            Glide.with(mContext)
                    .load(mUserDetails.getProfileImage())
                    .into(img_profile_pic);


        }

    }


    private void sendUpdateProfileRequest() {

        final String TAG = "UPDATE_PROFILE";
        mContext.showWaitIndicator(true);

        Log.e(TAG, "FirstName : " + tedt_signin_firstname.getText().toString());
        Log.e(TAG, "LastName : " + tedt_signin_lastname.getText().toString());
        Log.e(TAG, "Email : " + tedt_signin_email.getText().toString());
//        Log.e(TAG, "Password : " + edt_profile_password.getText().toString());
        Log.e(TAG, "Address : " + tedt_signin_address.getText().toString());
        Log.e(TAG, "City : " + tedt_signin_city.getText().toString());
        Log.e(TAG, "Mode : " + BaseActivity.USER_MODE);
        Log.e(TAG, "phone : " + tedt_signin_phone.getText().toString());
        Log.e(TAG, "mIdUser : " + mIdUser);

        MultipartBody.Part mPartPic = null;

        if (selectedImagePath == null) {

        }else {

            File file = BitmaptoFile();
//            File file = new File(selectedImagePath);
            Log.e(TAG, "ImageFile from Bitmap : " + file);
            mPartPic = getbodyPart(file, "profile_img");
        }


        RequestBody firstname = RequestBody.create(MediaType.parse("text/plain"), tedt_signin_firstname.getText().toString().trim());
        RequestBody lastname = RequestBody.create(MediaType.parse("text/plain"), tedt_signin_lastname.getText().toString().trim());
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), tedt_signin_email.getText().toString().trim());
//        RequestBody password = RequestBody.create(MediaType.parse("text/plain"), "123456");
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), tedt_signin_address.getText().toString().trim());
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), tedt_signin_city.getText().toString().trim());
        RequestBody mode = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(BaseActivity.USER_MODE));
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), tedt_signin_phone.getText().toString());

        RequestBody iduser = RequestBody.create(MediaType.parse("text/plain"), mIdUser);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

//    profile_img, id_seller/id_buyer, first_name, last_name, email, password, , address, city, mode

        Map<String, RequestBody> parameters = new HashMap<>();


        parameters.put("user_id",iduser);
        parameters.put("first_name",firstname);
        parameters.put("last_name",lastname);
        parameters.put("email",email);
//        parameters.put("password",password);
        parameters.put("address",address);
        parameters.put("city",city);
        parameters.put("mode",mode);
        parameters.put("phone",phone);


        Call<EditProfile> call = service.sendUpdateProfileRequest(mPartPic, parameters);

        call.enqueue(new Callback<EditProfile>() {
            @Override
            public void onResponse(Call<EditProfile> call, Response<EditProfile> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

//                        mContext.setLogin(1);

                        Log.e(TAG, "RESPONSE : " + response.body().getMessage());
                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();



                        UserDetails mUserDetails = new UserDetails();

                        if(BaseActivity.USER_MODE == 0){

                            Log.e(TAG,"RESPONSE : "+response.body().getData().getIdSeller());

                            mUserDetails.setIdUser(response.body().getData().getIdSeller());

                        }else if(BaseActivity.USER_MODE == 1){

                            Log.e(TAG,"RESPONSE : "+response.body().getData().getIdVendor());

                            mUserDetails.setIdUser(response.body().getData().getIdVendor());

                        }else if(BaseActivity.USER_MODE == 2){

                            Log.e(TAG,"RESPONSE : "+response.body().getData().getIdBuyer());

                            mUserDetails.setIdUser(response.body().getData().getIdBuyer());

                        }

                        Log.e(TAG,"UPDATE RESPONSE : "+response.body().getData().getProfileImg());

                        mUserDetails.setFirstName(response.body().getData().getFirstName());
                        mUserDetails.setLastName(response.body().getData().getLastName());

                        mUserDetails.setAddress(response.body().getData().getAddress());
                        mUserDetails.setCity(response.body().getData().getCity());
                        mUserDetails.setEmail(response.body().getData().getEmail());
//                        mUserDetails.setPassword(response.body().getData().getPassword());
                        mUserDetails.setPhonenumber(response.body().getData().getPhone());
                        mUserDetails.setProfileImage(response.body().getData().getProfileImg());

                        mContext.setUserDetails(mContext, mUserDetails);
                        BaseActivity.hideKeyboard(mContext);

                        if(BaseActivity.USER_MODE == 0){

                            mContext.changeFragment(new HomeSellerFragment());

                        }else if(BaseActivity.USER_MODE == 1){

                            mContext.changeFragment(new HomeVendorFragment());

                        }else if(BaseActivity.USER_MODE == 2){

                            mContext.changeFragment(new HomeMarketFragment());
                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<EditProfile> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("SIGN_UP onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public MultipartBody.Part getbodyPart(@NonNull File file, @NonNull String fieldname) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(fieldname, file.getName(), requestFile);
    }

    private void ShowPicOption(){

        final String[] options = {"Camera", "Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Pick a picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]

                Log.e("OnClick ","Option : "+options[which]);

                String mOptions = options[which];
                if(mOptions.equalsIgnoreCase("Camera")){

                    if (CameraUtils.checkPermissions(mContext)) {
//            layout_include.setVisibility(View.GONE);
                        captureImage();
                    } else {
                        requestCameraPermission(BaseActivity.MEDIA_TYPE_IMAGE);
                    }

                }else if(mOptions.equalsIgnoreCase("Gallery")){

                    requestStoragePermission();
                }

            }
        });
        builder.show();

    }

    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission(final int type) {
        Dexter.withActivity(mContext)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == BaseActivity.MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    //********************* Coding for camera **************************//
    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(BaseActivity.MEDIA_TYPE_IMAGE);
        if (file != null) {
            BaseActivity.imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(mContext, file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE);
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(mContext);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    private void requestStoragePermission() {

        Dexter.withActivity(mContext)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted
                        OpenGallery();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void OpenGallery() {

        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int requestcode, Intent data) {
        super.onActivityResult(requestCode, requestcode, data);

        switch (requestCode) {
            case PICK_IMAGE:
                if (requestcode == Activity.RESULT_OK) {
//            Uri photoURI = FileProvider.getUriForFile(mContext,
//                    BuildConfig.APPLICATION_ID + ".provider",
//                    createImageFile());

                    selectedImageUri = data.getData();
                    String filePath = null;

                    try {
                        // OI FILE Manager
                        filemanagerstring = selectedImageUri.getPath();

                        // MEDIA GALLERY
                        selectedImagePath = mContext.getPath(selectedImageUri);
                        Log.e("selectedImagePath", "path : " + selectedImagePath);

                        if (selectedImagePath != null) {
                            filePath = selectedImagePath;
                        } else if (filemanagerstring != null) {
                            filePath = filemanagerstring;
                        } else {
                            Toast.makeText(mContext, "Unknown path", Toast.LENGTH_LONG).show();
                            Log.e("Bitmap", "Unknown path");
                        }

                        if (filePath != null) {
//                    bitmap = BitmapFactory.decodeFile(filePath);
                            bitmap = lessResolution(filePath, 200, 200);
                            img_profile_pic.setImageBitmap(bitmap);
                            BaseActivity.BmpCropped = bitmap;

                            Intent i = new Intent(mContext, CropActivity.class);
                            startActivityForResult(i, CROP_IMAGE);

                        } else {
                            bitmap = null;
                        }

                    } catch (Exception e) {
                        Toast.makeText(mContext, "Internal Error", Toast.LENGTH_LONG).show();
                        Log.e(e.getClass().getName(), e.getMessage(), e);
                    }
                }
                break;
            case CROP_IMAGE:

                img_profile_pic.setImageBitmap(BaseActivity.BmpCropped);

                break;

            case CAMERA_CAPTURE:

                if (requestcode == Activity.RESULT_OK) {
                    // Refreshing the gallery
                    try {

                        CameraUtils.refreshGallery(mContext.getApplicationContext(), mContext.imageStoragePath);

                        selectedImagePath = mContext.imageStoragePath;

                        Log.e("selectedImagePath", "Camera Path : "+selectedImagePath);

                        if (selectedImagePath != null) {

                            bitmap = lessResolution(selectedImagePath, 200, 200);
                            img_profile_pic.setImageBitmap(bitmap);
                            BaseActivity.BmpCropped = bitmap;

                            Intent i = new Intent(mContext, CropActivity.class);
                            startActivityForResult(i, CROP_IMAGE);

                        } else {
                            bitmap = null;
                        }

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                } else if (requestcode == Activity.RESULT_CANCELED) {
                    // user cancelled Image capture
                    Toast.makeText(mContext.getApplicationContext(),
                            "You cancelled image capture", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    // failed to capture image
                    Toast.makeText(mContext.getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }

                break;
        }

    }

    public static Bitmap lessResolution(String filePath, int width, int height) {
        int reqHeight = height;
        int reqWidth = width;
        BitmapFactory.Options options = new BitmapFactory.Options();

        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private File BitmaptoFile(){
        File imgfile = null;
        Log.e("UPDATE_PROFILE","Inside BitmaptoFile");
        try{
            //create a file to write bitmap data
            imgfile = new File(mContext.getCacheDir(), "profile_pic.jpg");
            imgfile.createNewFile();

//Convert bitmap to byte array
            Bitmap bitmap = BaseActivity.BmpCropped;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(imgfile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

        }catch (Exception e){

            Log.e("UPDATE_PROFILE","Exception :"+e);

        }

        return imgfile ;

    }



}
