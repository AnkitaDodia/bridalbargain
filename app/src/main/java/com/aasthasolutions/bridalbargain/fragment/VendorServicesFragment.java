package com.aasthasolutions.bridalbargain.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.AuthorizationTabActivity;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.model.VendorServices;
import com.aasthasolutions.bridalbargain.model.VendorServicesData;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class VendorServicesFragment extends Fragment {

    HomeActivity mContext;

    RecyclerView rv_vendors;

    VendorServicesAdapter mVendorServicesAdapter;
    TextView txt_empty_vendors;

    EditText search;
    LinearLayout linear_search_layout, ll_search;

    ArrayList<VendorServicesData> mGetVendorServicesDataList = new ArrayList<>();

    public VendorServicesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vendors, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity)getActivity();
        mContext.showBackButton(true);
        mContext.isShowBackIcon = true;

       initView(view);
       setClickListener();

        if (BaseActivity.CheckInternet(mContext)) {
            sendVendorServicesRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void initView(View view)
    {
        txt_empty_vendors = view.findViewById(R.id.txt_empty_vendors);

        rv_vendors = view.findViewById(R.id.rv_vendors);
        rv_vendors.setHasFixedSize(true);
        rv_vendors.setLayoutManager(new GridLayoutManager(mContext, 2));

        search = view.findViewById(R.id.search_vendor);
        search.setHint("Search vendor with zipcode");
        search.setInputType(InputType.TYPE_CLASS_TEXT);
        ll_search = view.findViewById(R.id.ll_search);
        linear_search_layout = view.findViewById(R.id.linear_search_layout);
        linear_search_layout.setVisibility(View.GONE);
    }

    private void setClickListener()
    {

        ll_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(search.getText().toString().trim())){

                    Toast.makeText(mContext, "Please enter zipcode to search", Toast.LENGTH_LONG).show();

                }else {

                    sendSearchByZipRequest();
                }

            }
        });

        ll_search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                View view = mContext.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });

        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

                if(TextUtils.isEmpty(search.getText().toString().trim())){
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendVendorServicesRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub


            }
        });
    }

    private void sendVendorServicesRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);
        String mIdVendor = mUserDetails.getIdUser();

        Log.e("VENDOR_SERVICES","RESPONSE : "+BaseActivity.SERVICE_CATEGORY_ID);

        Call<VendorServices> call = service.sendVendorServicesRequest(BaseActivity.SERVICE_CATEGORY_ID, mIdVendor);

        call.enqueue(new Callback<VendorServices>() {
            @Override
            public void onResponse(Call<VendorServices> call, Response<VendorServices> response) {
                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        if(response.body().getStatus() == 1){

                            linear_search_layout.setVisibility(View.VISIBLE);
                            rv_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setVisibility(View.GONE);

                            mGetVendorServicesDataList.clear();
                            mGetVendorServicesDataList = response.body().getData();
                            for(int i =0; i < response.body().getData().size(); i++){

//                                mGetVendorServicesDataList.add(response.body().getData().get(i));
                                Log.e("VENDOR_SERVICES","RESPONSE : "+response.body().getData().get(i).getZip());
                            }

                            PrepareAdapter(mGetVendorServicesDataList);

                        }else {
                            linear_search_layout.setVisibility(View.GONE);
                            rv_vendors.setVisibility(View.GONE);
                            txt_empty_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setText(response.body().getMessage());
                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<VendorServices> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }


    private void sendSearchByZipRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Log.e("VENDOR_SEARCH","CategoryId : "+BaseActivity.SERVICE_CATEGORY_ID);
        Log.e("VENDOR_SEARCH","ZipCode : "+search.getText().toString().trim());

        Call<VendorServices> call = service.sendSearchByZipRequest(search.getText().toString().trim(), BaseActivity.SERVICE_CATEGORY_ID);

        call.enqueue(new Callback<VendorServices>() {
            @Override
            public void onResponse(Call<VendorServices> call, Response<VendorServices> response) {
                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        if(response.body().getStatus() == 1){

                            rv_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setVisibility(View.GONE);

                            mGetVendorServicesDataList.clear();
                            mGetVendorServicesDataList = response.body().getData();
                            for(int i =0; i < response.body().getData().size(); i++){

//                                mGetVendorServicesDataList.add(response.body().getData().get(i));
//                                Log.e("VENDOR_SERVICES","RESPONSE : "+response.body().getData().get(i).getZip());

                            }

                            PrepareAdapter(mGetVendorServicesDataList);

                        }else {

                            rv_vendors.setVisibility(View.GONE);
                            txt_empty_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setText(response.body().getMessage());
                        }

                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<VendorServices> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void PrepareAdapter(final ArrayList<VendorServicesData> mGetVendorServicesDataList) {


        mVendorServicesAdapter = new VendorServicesAdapter(mContext, mGetVendorServicesDataList, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View v, int position) {

                BaseActivity.CHAT_ROLE = mGetVendorServicesDataList.get(position).getRole();

                BaseActivity.CHAT_USER_ID = mGetVendorServicesDataList.get(position).getIdVendor();
                BaseActivity.CHAT_NAME = mGetVendorServicesDataList.get(position).getFirstName();
                        BaseActivity.CHAT_IMAGE = mGetVendorServicesDataList.get(position).getProfileImg();

                UserDetails mUserDetails = mContext.getUserDetails(mContext);
                BaseActivity.USER_IMAGE = mUserDetails.getProfileImage();

                BaseActivity.hideKeyboard(mContext);

                if (v.getId() == R.id.ll_chat) {
                    if (mContext.getLogin() == 0) {
                        Intent i = new Intent(mContext, AuthorizationTabActivity.class);
                        startActivity(i);
                    } else {
                        mContext.changeFragment(new ChatingFragment());
                    }
                } else if (v.getId() == R.id.ll_row_vendor_services) {

                    BaseActivity.SERVICE_ID = mGetVendorServicesDataList.get(position).getIdService();

                    mContext.changeFragment(new ServiceDetailsFragment());
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });


        rv_vendors.setAdapter(mVendorServicesAdapter);
    }

    /********************************* Adapter ****************************************************/
    public class VendorServicesAdapter extends RecyclerView.Adapter<VendorServicesAdapter.MyViewHolder> {
        ArrayList<VendorServicesData> mVendorServicesDataList = new ArrayList<>();

        HomeActivity mContext;
        private final BaseActivity.ClickListener listener;

        public VendorServicesAdapter(HomeActivity mContext, ArrayList<VendorServicesData> mList, BaseActivity.ClickListener listener) {
            this.mContext = mContext;
            this.mVendorServicesDataList = mList;
            this.listener = listener;
//            arraylist = mList;
        }

        @Override
        public VendorServicesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_vendor_services, null);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final VendorServicesAdapter.MyViewHolder holder, final int position) {
            VendorServicesData mVendorServicesData = mVendorServicesDataList.get(position);

            Log.e("VENDOR_DETAIL_LIST",""+mVendorServicesData.getServiceImg());
            Glide.with(mContext).load(mVendorServicesData.getServiceImg())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.progress_market_product.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            holder.img_vendor_services.setImageDrawable(resource);
                            holder.progress_market_product.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.img_vendor_services);

            holder.txt_service_title.setText(mVendorServicesData.getServiceName());
            holder.txt_service_price.setText("$" + mVendorServicesData.getServicePrice());
//        holder.txt_service_messages.setText("Message");
        }

        @Override
        public int getItemCount() {
            return mVendorServicesDataList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            FrameLayout ll_row_vendor_services;
            LinearLayout ll_chat;
            ImageView img_vendor_services;
            TextView txt_service_title, txt_service_price;

            private WeakReference<BaseActivity.ClickListener> listenerRef;

            ProgressBar progress_market_product;

            public MyViewHolder(View itemView) {
                super(itemView);

                listenerRef = new WeakReference<>(listener);

                ll_row_vendor_services = itemView.findViewById(R.id.ll_row_vendor_services);
                img_vendor_services = itemView.findViewById(R.id.img_vendor_services);
                txt_service_title = itemView.findViewById(R.id.txt_service_title);
                txt_service_price = itemView.findViewById(R.id.txt_service_price);
                ll_chat = itemView.findViewById(R.id.ll_chat);

                progress_market_product = itemView.findViewById(R.id.progress_market_product);

                mContext.overrideFonts(ll_row_vendor_services, mContext);

                ll_row_vendor_services.setOnClickListener(this);
                ll_chat.setOnClickListener(this);
            }

            // onClick Listener for view
            @Override
            public void onClick(View v) {

           /* if (v.getId() == ll_chat.getId()) {
                Toast.makeText(v.getContext(), "ITEM PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(v.getContext(), "ROW PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            }*/

                listenerRef.get().onClick(v, getAdapterPosition());
            }
        }
    }
}
