package com.aasthasolutions.bridalbargain.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.Login;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChangePasswordFragment extends Fragment {

    final String TAG = "CHANGE_PASSWORD";
    HomeActivity mContext;
    TextInputEditText tedt_old_password, tedt_new_password, tedt_confirm_password;
    TextInputLayout txtlay_old_password, txtlay_new_password, txtlay_confirm_password;
    LinearLayout lay_parent_change_password;
    Button btn_change_password;


    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        mContext.showAddProductLayout(false);

        lay_parent_change_password = view.findViewById(R.id.lay_parent_change_password);

        tedt_old_password = view.findViewById(R.id.tedt_old_password);
        tedt_new_password = view.findViewById(R.id.tedt_new_password);
        tedt_confirm_password = view.findViewById(R.id.tedt_confirm_password);

        txtlay_old_password = view.findViewById(R.id.txtlay_old_password);
        txtlay_new_password = view.findViewById(R.id.txtlay_new_password);
        txtlay_confirm_password = view.findViewById(R.id.txtlay_confirm_password);

        txtlay_old_password.setTypeface(mContext.getRegularFonts(mContext));
        txtlay_new_password.setTypeface(mContext.getRegularFonts(mContext));
        txtlay_confirm_password.setTypeface(mContext.getRegularFonts(mContext));

        btn_change_password = view.findViewById(R.id.btn_change_password);

        btn_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(tedt_old_password.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "Please enter current password", Toast.LENGTH_LONG).show();
                }else if (TextUtils.isEmpty(tedt_new_password.getText().toString().trim())) {
//                    edt_login_password.setError("Please enter valid password");
                    Toast.makeText(mContext, "Please enter new password", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(tedt_confirm_password.getText().toString().trim())) {
//                    edt_login_password.setError("Please enter valid password");
                    Toast.makeText(mContext, "Please enter confirm password", Toast.LENGTH_LONG).show();
                } else {

                    if (BaseActivity.CheckInternet(mContext)) {
                        sendChangePasswordRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        lay_parent_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mContext.overrideFonts(lay_parent_change_password, mContext);
    }


    private void sendChangePasswordRequest() {

        mContext.showWaitIndicator(true);

        String old_password = tedt_old_password.getText().toString();
        String new_password = tedt_new_password.getText().toString();
        String confirm_password = tedt_confirm_password.getText().toString();

        UserDetails mUserDetails = mContext.getUserDetails(mContext);
        String mIdUser = mUserDetails.getIdUser();

        Log.e(TAG,"Mode : "+ BaseActivity.USER_MODE);
        Log.e(TAG,"mIdUser : "+ mIdUser);
        Log.e(TAG,"old_password : "+tedt_old_password.getText().toString());
        Log.e(TAG,"new_password : "+tedt_new_password.getText().toString());
        Log.e(TAG,"confirm_password : "+tedt_confirm_password.getText().toString());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<Login> call = service.sendChangePasswordRequest(mIdUser, old_password, new_password, confirm_password, BaseActivity.USER_MODE);

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {
                        Log.e(TAG,"RESPONSE : "+response.body().getMessage());
                        Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if(response.body().getStatus() == 1){

//                            mContext.setLogin(1);
                            Intent i = new Intent(mContext, HomeActivity.class);
                            startActivity(i);


                            Log.e(TAG,"RESPONSE : "+response.body().getData().getPhone());

//                            Log.e(TAG,"RESPONSE : "+response.body().getData().getFirstName());
//                            Log.e(TAG,"RESPONSE : "+response.body().getData().getFirstName());

                            UserDetails mUserDetails = new UserDetails();

                            if(BaseActivity.USER_MODE == 0){

                                Log.e(TAG,"RESPONSE : "+response.body().getData().getIdSeller());

                                mUserDetails.setIdUser(response.body().getData().getIdSeller());

                            }else if(BaseActivity.USER_MODE == 1){

                                Log.e(TAG,"RESPONSE : "+response.body().getData().getIdVendor());

                                mUserDetails.setIdUser(response.body().getData().getIdVendor());

                            }else if(BaseActivity.USER_MODE == 2){

                                Log.e(TAG,"RESPONSE : "+response.body().getData().getIdBuyer());

                                mUserDetails.setIdUser(response.body().getData().getIdBuyer());

                            }

                            mUserDetails.setFirstName(response.body().getData().getFirstName());
                            mUserDetails.setLastName(response.body().getData().getLastName());
                            mUserDetails.setAddress(response.body().getData().getAddress());
                            mUserDetails.setCity(response.body().getData().getCity());
                            mUserDetails.setEmail(response.body().getData().getEmail());
                            mUserDetails.setPassword(response.body().getData().getPassword());
                            mUserDetails.setPhonenumber(response.body().getData().getPhone());
                            mUserDetails.setProfileImage(response.body().getData().getProfileImg());

                            mContext.setUserDetails(mContext, mUserDetails);

                        }else {

                        }


                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG,"onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
