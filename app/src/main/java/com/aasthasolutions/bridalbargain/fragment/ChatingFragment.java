package com.aasthasolutions.bridalbargain.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.adapter.GetSingleMessagesAdapter;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.Logout;
import com.aasthasolutions.bridalbargain.model.SendMessages;
import com.aasthasolutions.bridalbargain.model.SingleChating;
import com.aasthasolutions.bridalbargain.model.SingleChatingData;
import com.aasthasolutions.bridalbargain.model.StatusMessages;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChatingFragment extends Fragment {


    HomeActivity mContext;
    public ArrayList<SingleChatingData> mSingleChatingDataList = new ArrayList<>();

    String TAG = "CHATTING_FRAGMENT";
    RecyclerView rv_messages;
    GetSingleMessagesAdapter mGetSingleMessagesAdapter;
    LinearLayout ll_parent_chating, ll_send_msg, ll_parent_send, ll_parent_block, ll_block_user;
    TextInputEditText tedt_type_chat_messgae;

    public ChatingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_chatting, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        mContext.showAddProductLayout(false);
        mContext.showBackButton(true);
        mContext.isShowBackIcon = true;

//        sendReadAllMessagesRequest();

        ll_parent_chating = view.findViewById(R.id.ll_parent_chating);
        ll_send_msg = view.findViewById(R.id.ll_send_msg);
        ll_parent_send = view.findViewById(R.id.ll_parent_send);
        ll_parent_block = view.findViewById(R.id.ll_parent_block);
        ll_block_user = view.findViewById(R.id.ll_block_user);
        tedt_type_chat_messgae = view.findViewById(R.id.tedt_type_chat_messgae);
        tedt_type_chat_messgae.setTypeface(mContext.getRegularFonts(mContext));

        rv_messages = view.findViewById(R.id.rv_messages);

        rv_messages.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true);
//        mLayoutManager.setStackFromEnd(true);
        rv_messages.setLayoutManager(mLayoutManager);


        mSingleChatingDataList.clear();
        if (BaseActivity.CheckInternet(mContext)) {
            switch (BaseActivity.USER_MODE) {
                case 0: //Seller
//                    sendSingleMessagesVendorSellerRequest();
                    sendBuyerSingleMessagesRequest();
                    break;
                case 1: //vendor
                    sendSingleMessagesVendorSellerRequest();
                    break;
            }

        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }

        ll_send_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(tedt_type_chat_messgae.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "You haven't type anything", Toast.LENGTH_LONG).show();
                } else {
                    if (BaseActivity.CheckInternet(mContext)) {

                        switch (BaseActivity.USER_MODE) {
                            case 0: //Seller
                                sendBuyerSendMessagesRequest();
                                break;
                            case 1: //vendor
                                sendMessagesVSRequest();
                                break;
                        }


                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        tedt_type_chat_messgae.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (tedt_type_chat_messgae.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });

        ll_parent_chating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ll_block_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (BaseActivity.CheckInternet(mContext)) {

                    showBlockUserDialog();

                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }

            }
        });

        ll_parent_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (BaseActivity.CheckInternet(mContext)) {

                    sendUnBlockUserRequest();

                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private void sendBuyerSendMessagesRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        String ID_SELLER = mUserDetails.getIdUser();
        String TO = BaseActivity.CHAT_USER_ID;
        String ROLE = BaseActivity.CHAT_ROLE;
        String MESSGAE = tedt_type_chat_messgae.getText().toString().trim();

//        Log.e(TAG, "Calling BuyerSendMessage API ");
//        Log.e(TAG, "ID_SELLER : " + ID_SELLER);
//        Log.e(TAG, "TO : " +TO);
//        Log.e(TAG, "ROLE : " + ROLE);
//        Log.e(TAG, "MESSAGE : " + MESSGAE);

        Call<SendMessages> call = service.sendBuyerSendMessagesRequest(ID_SELLER, TO, ROLE, MESSGAE);

        call.enqueue(new Callback<SendMessages>() {
            @Override
            public void onResponse(Call<SendMessages> call, Response<SendMessages> response) {
                mContext.showWaitIndicator(false);

//                Log.e(TAG, "RESPONSE : " + response.code());
//                Log.e(TAG, "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                try {
                    if (response.code() == 200) {

//                        Log.e(TAG, "STATUS : " + response.body().getStatus());

                        if (response.body().getStatus() == 1) {

                            sendBuyerSingleMessagesRequest();
                            tedt_type_chat_messgae.setText("");
                            BaseActivity.hideKeyboard(mContext);

                        } else {


                        }

                    }

                } catch (Exception e) {
                    Log.d("onResponse", "There is an error  :" + e);
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SendMessages> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }


    private void sendMessagesVSRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

//        Log.e(TAG, "BUYER_ID : " + mUserDetails.getIdUser());
//        Log.e(TAG, "CHAT_USER_ID : " + BaseActivity.CHAT_USER_ID);
//        Log.e(TAG, "ROLE : " + BaseActivity.CHAT_ROLE);
//        Log.e(TAG, "MESSAGE : " + tedt_type_chat_messgae.getText().toString().trim());

        String BUYER_ID = mUserDetails.getIdUser();
        String CHAT_USER_ID = BaseActivity.CHAT_USER_ID;
        String ROLE = BaseActivity.CHAT_ROLE;
        String MESSGAE = tedt_type_chat_messgae.getText().toString();

        Call<SendMessages> call = service.sendMessagesVSRequest(BUYER_ID, CHAT_USER_ID, ROLE, MESSGAE);

        call.enqueue(new Callback<SendMessages>() {
            @Override
            public void onResponse(Call<SendMessages> call, Response<SendMessages> response) {
                mContext.showWaitIndicator(false);

//                Log.e(TAG, "RESPONSE : " + response.code());

                try {
                    if (response.code() == 200) {

//                        Log.e(TAG, "STATUS : " + response.body().getStatus());

                        if (response.body().getStatus() == 1) {

                            sendSingleMessagesVendorSellerRequest();
                            tedt_type_chat_messgae.setText("");
                            BaseActivity.hideKeyboard(mContext);


                        } else {


                        }

                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SendMessages> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void sendBuyerSingleMessagesRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);
        Log.e(TAG, "calling sendBuyerSingleMessages");
        Log.e(TAG, "SELLER_ID : " + mUserDetails.getIdUser());
        Log.e(TAG, "CHAT_USER_ID : " + BaseActivity.CHAT_USER_ID);
        Log.e(TAG, "ROLE : " + BaseActivity.CHAT_ROLE);

        String SELLER_ID = mUserDetails.getIdUser();
        String CHAT_USER_ID = BaseActivity.CHAT_USER_ID;
        String ROLE = BaseActivity.CHAT_ROLE;

        Call<SingleChating> call = service.sendBuyerSingleMessagesRequest(SELLER_ID, CHAT_USER_ID, ROLE);

        call.enqueue(new Callback<SingleChating>() {
            @Override
            public void onResponse(Call<SingleChating> call, Response<SingleChating> response) {
                mContext.showWaitIndicator(false);

                Log.e(TAG, "RESPONSE : " + response.code());

                try {
                    if (response.code() == 200) {

                        Log.e(TAG, "STATUS : " + response.body().getStatus());
                        Log.e(TAG, "BLOCK : " + response.body().getBlock());
                        mSingleChatingDataList.clear();
                        if (response.body().getStatus() == 1) {

                            for (int i = 0; i < response.body().getData().size(); i++) {

                                mSingleChatingDataList.add(response.body().getData().get(i));
//                                Log.e(TAG, "RESPONSE : " + response.body().getData().get(i).getMessage());
                            }

                            mGetSingleMessagesAdapter = new GetSingleMessagesAdapter(mContext, mSingleChatingDataList);
                            rv_messages.setAdapter(mGetSingleMessagesAdapter);
                            rv_messages.scrollToPosition(mSingleChatingDataList.size() - 1);

                            if (response.body().getBlock() == 0) {
                                ll_parent_send.setVisibility(View.VISIBLE);
                                ll_parent_block.setVisibility(View.GONE);
                            } else {
                                ll_parent_send.setVisibility(View.GONE);
                                ll_parent_block.setVisibility(View.VISIBLE);
                            }

                        } else {


                        }


                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SingleChating> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void sendSingleMessagesVendorSellerRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);
        Log.e(TAG, "Calling  sendSingleMessagesVendorSellerRequest");
        Log.e(TAG, "BUYER_ID : " + mUserDetails.getIdUser());
        Log.e(TAG, "CHAT_USER_ID : " + BaseActivity.CHAT_USER_ID);
        Log.e(TAG, "ROLE : " + BaseActivity.CHAT_ROLE);

        String BUYER_ID = mUserDetails.getIdUser();
        String CHAT_USER_ID = BaseActivity.CHAT_USER_ID;
        String ROLE = BaseActivity.CHAT_ROLE;

        Call<SingleChating> call = service.sendSingleMessagesVendorSellerRequest(CHAT_USER_ID, BUYER_ID, ROLE);

        call.enqueue(new Callback<SingleChating>() {
            @Override
            public void onResponse(Call<SingleChating> call, Response<SingleChating> response) {
                mContext.showWaitIndicator(false);

                Log.e(TAG, "RESPONSE : " + response.code());

                try {
                    if (response.code() == 200) {

                        Log.e(TAG, "FULL RESPONSE : " + new Gson().toJson(response.body().getData()));
                        Log.e(TAG, "BLOCK : " + response.body().getBlock());

//                        Log.e(TAG, "STATUS : " + response.body().getStatus());
                        mSingleChatingDataList.clear();
                        if (response.body().getStatus() == 1) {

                            for (int i = 0; i < response.body().getData().size(); i++) {

                                mSingleChatingDataList.add(response.body().getData().get(i));
//                                Log.e(TAG, "RESPONSE : " + response.body().getData().get(i).getMessage());
                            }

                            mGetSingleMessagesAdapter = new GetSingleMessagesAdapter(mContext, mSingleChatingDataList);
                            rv_messages.setAdapter(mGetSingleMessagesAdapter);
                            rv_messages.scrollToPosition(mSingleChatingDataList.size() - 1);

                            if (response.body().getBlock() == 0) {
                                ll_parent_send.setVisibility(View.VISIBLE);
                                ll_parent_block.setVisibility(View.GONE);
                            } else {
                                ll_parent_send.setVisibility(View.GONE);
                                ll_parent_block.setVisibility(View.VISIBLE);
                            }

                        } else {


                        }

                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SingleChating> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void showBlockUserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Block Messages");
        builder.setMessage("Are you sure you want to block messages from this user ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                sendBlockUserRequest();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }


    private void sendBlockUserRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);


        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        String FROM_ID = mUserDetails.getIdUser();
        String TO_ID = BaseActivity.CHAT_USER_ID;
        String ROLE_FROM = BaseActivity.FROM_ROLE;
        String ROLE_TO = BaseActivity.TO_ROLE;

        Log.e(TAG, "Calling  sendBlockUserRequest");
        Log.e(TAG, "FROM_ID : " + mUserDetails.getIdUser());
        Log.e(TAG, "ROLE_FROM : " + ROLE_FROM);
        Log.e(TAG, "TO_ID : " + BaseActivity.CHAT_ROLE);
        Log.e(TAG, "ROLE_TO : " + ROLE_TO);

        Call<StatusMessages> call = service.sendBlockUserRequest(FROM_ID, ROLE_FROM, TO_ID, ROLE_TO);

        call.enqueue(new Callback<StatusMessages>() {
            @Override
            public void onResponse(Call<StatusMessages> call, Response<StatusMessages> response) {
                mContext.showWaitIndicator(false);

                Log.e(TAG, "RESPONSE : " + response.code());

                try {
                    if (response.code() == 200) {

                        if (response.body().getStatus() == 1) {

                            mSingleChatingDataList.clear();
                            if (BaseActivity.CheckInternet(mContext)) {
                                switch (BaseActivity.USER_MODE) {
                                    case 0: //Seller
                                        sendBuyerSingleMessagesRequest();
                                        break;
                                    case 1: //vendor
                                        sendSingleMessagesVendorSellerRequest();
                                        break;
                                }

                            } else {
                                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                            }

                        } else {


                        }

                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<StatusMessages> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void sendUnBlockUserRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);


        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        String FROM_ID = mUserDetails.getIdUser();
        String TO_ID = BaseActivity.CHAT_USER_ID;
        String ROLE_FROM = BaseActivity.FROM_ROLE;
        String ROLE_TO = BaseActivity.TO_ROLE;

        Log.e(TAG, "Calling  sendBlockUserRequest");
        Log.e(TAG, "FROM_ID : " + mUserDetails.getIdUser());
        Log.e(TAG, "ROLE_FROM : " + ROLE_FROM);
        Log.e(TAG, "TO_ID : " + BaseActivity.CHAT_ROLE);
        Log.e(TAG, "ROLE_TO : " + ROLE_TO);

        Call<StatusMessages> call = service.sendUnBlockUserRequest(FROM_ID, ROLE_FROM, TO_ID, ROLE_TO);

        call.enqueue(new Callback<StatusMessages>() {
            @Override
            public void onResponse(Call<StatusMessages> call, Response<StatusMessages> response) {
                mContext.showWaitIndicator(false);

                Log.e(TAG, "RESPONSE : " + response.code());

                try {
                    if (response.code() == 200) {

                        if (response.body().getStatus() == 1) {

                            mSingleChatingDataList.clear();
                            if (BaseActivity.CheckInternet(mContext)) {
                                switch (BaseActivity.USER_MODE) {
                                    case 0: //Seller
                                        sendBuyerSingleMessagesRequest();
                                        break;
                                    case 1: //vendor
                                        sendSingleMessagesVendorSellerRequest();
                                        break;
                                }

                            } else {
                                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                            }

                        } else {


                        }

                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<StatusMessages> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }




}
