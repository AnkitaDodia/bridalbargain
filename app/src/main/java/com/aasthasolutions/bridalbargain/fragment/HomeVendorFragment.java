package com.aasthasolutions.bridalbargain.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.adapter.ServicesByVendorAdapter;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.AddService;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.model.VendorServices;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HomeVendorFragment extends Fragment {

    HomeActivity mContext;

    RecyclerView rv_vendors;

    LinearLayout ll_parent_vendors,linear_search_layout;

    ServicesByVendorAdapter mServicesByVendorAdapter;

    TextView txt_empty_vendors;

    String mServiceId;


    public HomeVendorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vendors, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        initView(view);

        mContext.showAddProductLayout(true);

        if (BaseActivity.CheckInternet(mContext)) {
            sendServiceByVendorRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void initView(View view)
    {
        linear_search_layout = view.findViewById(R.id.linear_search_layout);
        linear_search_layout.setVisibility(View.GONE);

        rv_vendors = view.findViewById(R.id.rv_vendors);
        rv_vendors.setHasFixedSize(true);
        rv_vendors.setLayoutManager(new GridLayoutManager(mContext, 2));

        txt_empty_vendors = view.findViewById(R.id.txt_empty_vendors);
        ll_parent_vendors = view.findViewById(R.id.ll_parent_vendors);

        ll_parent_vendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void sendServiceByVendorRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);


        Log.e("VENDOR_ID", "RESPONSE : " + mUserDetails.getIdUser());

        Call<VendorServices> call = service.sendServiceByVendorRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<VendorServices>() {
            @Override
            public void onResponse(Call<VendorServices> call, Response<VendorServices> response) {
                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        if (response.body().getStatus() == 1) {

                            Log.e("VENDOR_PRODUCT", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                            rv_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setVisibility(View.GONE);

                            BaseActivity.mVendorServiceDataList.clear();
                            BaseActivity.mVendorServiceDataList = response.body().getData();

//                            for (int i = 0; i < response.body().getData().size(); i++) {
//
//                                BaseActivity.mVendorServiceDataList.add(response.body().getData().get(i));
//                                Log.e("SERVICES_BY_VENDOR", "RESPONSE : " + response.body().getData().get(i).getServiceName());
//                            }
                            PrepareAdapter();

                        } else {
                            rv_vendors.setVisibility(View.GONE);
                            txt_empty_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setText(""+response.body().getMessage());
                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<VendorServices> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void PrepareAdapter() {
        mServicesByVendorAdapter = new ServicesByVendorAdapter(mContext, BaseActivity.mVendorServiceDataList, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View v, int position) {

                if (v.getId() == R.id.ll_delete_product) {

                    mServiceId = BaseActivity.mVendorServiceDataList.get(position).getIdService();

                    showDeleteDialog();

                } else if (v.getId() == R.id.ll_edit_product) {


                    BaseActivity.VENDOR_SERVICE_POSITION = position;
//                    BaseActivity.SELLER_PRODUCT_ID = BaseActivity.mGetSellerProductDataList.get(position).getIdProduct();
                    BaseActivity.IS_ADD_SERVICE = false;
                    mContext.changeFragment(new AddServiceFragment());

                } else if (v.getId() == R.id.ll_row_product_by_seller) {


                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rv_vendors.setAdapter(mServicesByVendorAdapter);
    }

    private void sendDeleteServiceRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Log.e("DELETE_SERVICE", "mServiceId : " + mServiceId);

        Call<AddService> call = service.sendDeleteServiceRequest(mServiceId);

        call.enqueue(new Callback<AddService>() {
            @Override
            public void onResponse(Call<AddService> call, Response<AddService> response) {
                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        if (response.body().getStatus() == 1) {

                            sendServiceByVendorRequest();

                            Log.e("DELETE_SERVICE", "RESPONSE : " + response.body().getMessage());
                            Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        } else {
                            Log.e("DELETE_SERVICE", "RESPONSE ELSE: " + response.body().getMessage());
                            Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddService> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Delete service");
        builder.setMessage("Are you sure you want to delete this service?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if (BaseActivity.CheckInternet(mContext)) {
                    sendDeleteServiceRequest();
                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }
}
