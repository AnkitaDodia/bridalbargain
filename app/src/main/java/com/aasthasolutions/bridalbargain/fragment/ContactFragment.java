package com.aasthasolutions.bridalbargain.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.CropActivity;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.EditProfile;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.aasthasolutions.bridalbargain.utility.CameraUtils;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ContactFragment extends Fragment {

    HomeActivity mContext;
    LinearLayout ll_parent_contact, ll_parent_profile, ll_facebook, ll_instagram;

    public ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        initViews(view);
        SetClickListner();
        mContext.showAddProductLayout(false);


    }

    private void initViews(View view) {

        ll_parent_profile = view.findViewById(R.id.ll_parent_profile);
        ll_facebook = view.findViewById(R.id.ll_facebook);
        ll_instagram = view.findViewById(R.id.ll_instagram);

        ll_parent_contact = view.findViewById(R.id.ll_parent_contact);


        mContext.overrideFonts(ll_parent_profile, mContext);

    }
    private void SetClickListner(){

        ll_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/bridalbargainapp")));

            }
        });

        ll_instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/bridalbargain/")));

            }
        });

        ll_parent_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

}
