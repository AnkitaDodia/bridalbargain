package com.aasthasolutions.bridalbargain.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.AuthorizationTabActivity;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.activities.PaymentActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.Login;
import com.aasthasolutions.bridalbargain.model.MyPlans;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyPlansFragment extends Fragment {

    final String TAG = "MyPlans";
    HomeActivity mContext;
    LinearLayout ll_parent_myplan;
    TextView txt_payment_start_date, txt_payment_end_date, txt_payment_amount, txt_next_payment_date;
    Button btn_pay_in_advance;

    public MyPlansFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_myplans, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        initViews(view);
        SetClickListner();
        mContext.showAddProductLayout(false);

        if (BaseActivity.CheckInternet(mContext)) {
            sendMyPlansRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }

    }

    private void initViews(View view) {

        ll_parent_myplan = view.findViewById(R.id.ll_parent_myplan);
        txt_payment_start_date = view.findViewById(R.id.txt_payment_start_date);
        txt_payment_end_date = view.findViewById(R.id.txt_payment_end_date);
        txt_payment_amount = view.findViewById(R.id.txt_payment_amount);

        txt_next_payment_date = view.findViewById(R.id.txt_next_payment_date);
        btn_pay_in_advance = view.findViewById(R.id.btn_pay_in_advance);

        mContext.overrideFonts(ll_parent_myplan, mContext);

    }

    private void SetClickListner() {


        btn_pay_in_advance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, PaymentActivity.class);
                i.putExtra("FROM_WHERE", "MyPlans");
                startActivity(i);
            }
        });

        ll_parent_myplan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void sendMyPlansRequest() {

        mContext.showWaitIndicator(true);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);
        String mIdUser = mUserDetails.getIdUser();

        Log.e(TAG, "Mode : " + BaseActivity.USER_MODE);
        Log.e(TAG, "mIdUser : " + mIdUser);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<MyPlans> call = service.sendMyPlansRequest(mIdUser);

        call.enqueue(new Callback<MyPlans>() {
            @Override
            public void onResponse(Call<MyPlans> call, Response<MyPlans> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {
                        Log.e(TAG, "RESPONSE : " + response.body().getMessage());
//                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

//                        if (response.body().getStatus() == 1) {

                            Log.e(TAG, "RESPONSE : " + response.body().getData().getIdTransaction());

                            txt_payment_start_date.setText("Renews on "+ChangeDateFormate(response.body().getData().getPaymentDate()));
//                            txt_payment_end_date.setText(response.body().getData().getIsPaymentExpired());
                            txt_payment_amount.setText("$"+response.body().getData().getPaymentAmount());
                            txt_next_payment_date.setText("Expires "+ChangeDateFormate(response.body().getData().getNextPaymentDate()));

//                        } else {
//
//                        }


                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MyPlans> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure" + t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public static String ChangeDateFormate(String inputDateString) {

        Date date = null;
        String outputDateString = null;

        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("MMM dd, yyyy");

        try {
            date = inputDateFormat.parse(inputDateString);
            outputDateString = outputDateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputDateString;

    }

}
