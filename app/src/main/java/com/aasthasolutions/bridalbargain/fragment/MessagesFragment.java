package com.aasthasolutions.bridalbargain.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.adapter.GetMessagesAdapter;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.model.UserMessages;
import com.aasthasolutions.bridalbargain.model.UserMessagesData;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MessagesFragment extends Fragment {


    HomeActivity mContext;
    public ArrayList<UserMessagesData> mUserMessagesDataList = new ArrayList<>();

    String TAG = "MESSAGES_FRAGMENT";
    LinearLayout ll_parent_messages;
    RecyclerView rv_messages;
    GetMessagesAdapter mGetMessagesAdapter;
    TextView txt_empty_messages;

    public MessagesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_messages, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();
        mContext.showAddProductLayout(false);

        rv_messages = view.findViewById(R.id.rv_messages);
        txt_empty_messages = view.findViewById(R.id.txt_empty_messages);
        ll_parent_messages = view.findViewById(R.id.ll_parent_messages);

        rv_messages.setHasFixedSize(true);
        rv_messages.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rv_messages.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));

        ll_parent_messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mUserMessagesDataList.clear();

        if (BaseActivity.CheckInternet(mContext)) {
            switch (BaseActivity.USER_MODE)
            {
                case 0: //Seller
//                    sendSellerMessagesRequest();
                    sendBuyerMessagesRequest();
                    break;
                case 1: //vendor
                    sendVendorMessagesRequest();
                    break;
                case 2: //customer
//                    sendBuyerMessagesRequest();
                    break;
            }
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }

        rv_messages.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_messages, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                BaseActivity.CHAT_USER_ID = mUserMessagesDataList.get(position).getId();
                BaseActivity.CHAT_NAME = mUserMessagesDataList.get(position).getName();
                BaseActivity.CHAT_IMAGE = mUserMessagesDataList.get(position).getImage();

                BaseActivity.FROM_ROLE = String.valueOf(BaseActivity.USER_MODE);
                BaseActivity.TO_ROLE = mUserMessagesDataList.get(position).getRole();

                switch (BaseActivity.USER_MODE)
                {
                    case 0: //Seller

//                        BaseActivity.CHAT_ROLE = String.valueOf(BaseActivity.USER_MODE);
                        BaseActivity.CHAT_ROLE = String.valueOf(mUserMessagesDataList.get(position).getRole());
                        break;
                    case 1: //vendor
                        BaseActivity.CHAT_ROLE = String.valueOf(BaseActivity.USER_MODE);
//                        BaseActivity.CHAT_ROLE = mUserMessagesDataList.get(position).getRole();
                        break;
                }

                UserDetails mUserDetails = mContext.getUserDetails(mContext);
                BaseActivity.USER_IMAGE = mUserDetails.getProfileImage();

                Log.e(TAG, "CHAT_USER_ID : "+BaseActivity.CHAT_USER_ID);

                mContext.changeFragment(new ChatingFragment());

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

//        Log.e(TAG, "YES I'm Here !");

    }

    private void sendBuyerMessagesRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        Log.e("MESSAGES_API", "calling sendBuyerMessages");
        Log.e("MESSAGES_API", "IdUser  " + mUserDetails.getIdUser());

        Call<UserMessages> call = service.sendBuyerMessagesRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<UserMessages>() {
            @Override
            public void onResponse(Call<UserMessages> call, Response<UserMessages> response) {
                mContext.showWaitIndicator(false);

//                Log.e(TAG,"RESPONSE : "+response.code());
                Log.e(TAG, "MESSAGES_API Respnse : "+ new Gson().toJson(response.body().getData()));

                try {
                    if (response.code() == 200) {

//                        Log.e(TAG,"RESPONSE : "+response.body().getData());
                        rv_messages.setVisibility(View.VISIBLE);
                        txt_empty_messages.setVisibility(View.GONE);

                        if(response.body().getStatus() == 1){

                            mUserMessagesDataList = response.body().getData();

                            mGetMessagesAdapter = new GetMessagesAdapter(mContext, mUserMessagesDataList);
                            rv_messages.setAdapter(mGetMessagesAdapter);

                        }else {

                            rv_messages.setVisibility(View.GONE);
                            txt_empty_messages.setVisibility(View.VISIBLE);
                            txt_empty_messages.setText(response.body().getMessage());
                        }

                    }

                } catch (Exception e) {
                    Log.d("onResponse", "There is an error: "+e);
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<UserMessages> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void sendSellerMessagesRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        Log.e("BUYER_ID", "RESPONSE : " + mUserDetails.getIdUser());

        Call<UserMessages> call = service.sendSellerMessagesRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<UserMessages>() {
            @Override
            public void onResponse(Call<UserMessages> call, Response<UserMessages> response) {
                mContext.showWaitIndicator(false);

                Log.e(TAG,"RESPONSE : "+response.code());

                try {
                    if (response.code() == 200) {

                        Log.e(TAG,"RESPONSE : "+response.body().getStatus());

                        if(response.body().getStatus() == 1){

                            for (int i = 0; i < response.body().getData().size(); i++) {

                                mUserMessagesDataList.add(response.body().getData().get(i));
                                Log.e(TAG, "RESPONSE : " + response.body().getData().get(i).getName());
                            }

                            mGetMessagesAdapter = new GetMessagesAdapter(mContext, mUserMessagesDataList);
                            rv_messages.setAdapter(mGetMessagesAdapter);

                        }else {


                        }

                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<UserMessages> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void sendVendorMessagesRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        Log.e("BUYER_ID", "RESPONSE : " + mUserDetails.getIdUser());

        Call<UserMessages> call = service.sendVendorMessagesRequest(mUserDetails.getIdUser());

        call.enqueue(new Callback<UserMessages>() {
            @Override
            public void onResponse(Call<UserMessages> call, Response<UserMessages> response) {
                mContext.showWaitIndicator(false);

                Log.e(TAG,"RESPONSE : "+response.code());

                try {
                    if (response.code() == 200) {

                        Log.e(TAG,"RESPONSE : "+response.body().getStatus());

                        if(response.body().getStatus() == 1){

                            for (int i = 0; i < response.body().getData().size(); i++) {

                                mUserMessagesDataList.add(response.body().getData().get(i));
                                Log.e(TAG, "RESPONSE : " + response.body().getData().get(i).getName());
                            }

                            mGetMessagesAdapter = new GetMessagesAdapter(mContext, mUserMessagesDataList);
                            rv_messages.setAdapter(mGetMessagesAdapter);

                        }else {
                            rv_messages.setVisibility(View.GONE);
                            txt_empty_messages.setVisibility(View.VISIBLE);
                            txt_empty_messages.setText(response.body().getMessage());
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<UserMessages> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
