package com.aasthasolutions.bridalbargain.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.adapter.SlidingPagerAdapter;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.customview.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;


public class HomeMarketFragment extends Fragment {

    HomeActivity mContext;

//    TabLayout tab_market_vendor;
    ViewPager vp_market_vendor;
    SlidingTabLayout svp_market_vendor;
    SlidingPagerAdapter adapter;
    CharSequence Titles[]={"MarketPlace","Vendors"};
    int Numboftabs =2;

//    http://www.android4devs.com/2015/01/how-to-make-material-design-sliding-tabs.html

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_market, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();
        mContext.showAddProductLayout(false);

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new SlidingPagerAdapter(mContext.getSupportFragmentManager(), Titles, Numboftabs);

        // Assigning ViewPager View and setting the adapter
        vp_market_vendor = view.findViewById(R.id.vp_market_vendor);
        vp_market_vendor.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        svp_market_vendor = view.findViewById(R.id.svp_market_vendor);
        svp_market_vendor.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        svp_market_vendor.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        svp_market_vendor.setViewPager(vp_market_vendor);

        if(BaseActivity.isFromVendor){

            vp_market_vendor.setCurrentItem(1);

        }else{

            vp_market_vendor.setCurrentItem(0);
        }
    }


    @Override
    public void onResume() {
        super.onResume();

//        Toast.makeText(mContext, "onResume call", Toast.LENGTH_SHORT).show();
    }
}
