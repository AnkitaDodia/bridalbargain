package com.aasthasolutions.bridalbargain.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.adapter.GetSellersAdapter;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.GetSellers;
import com.aasthasolutions.bridalbargain.model.GetSellersData;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.google.gson.Gson;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MarketPlaceFragment extends Fragment {

    HomeActivity mContext;

    RecyclerView rv_marketplace;
    TextView txt_empty_marketplace;
    LinearLayout ll_parent_marketplace;

    GetSellersAdapter mGetSellersAdapter;
    ArrayList<GetSellersData> mGetSellersDataList = new ArrayList<>();

    public MarketPlaceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_marketplace, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity)getActivity();

        initView(view);
        setListener();

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            rv_marketplace.setNestedScrollingEnabled(true);
//        }

//        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
//        rv_marketplace.addItemDecoration(new SpacesItemDecoration(spacingInPixels));



        if (BaseActivity.CheckInternet(mContext)) {
            sendGetSellersRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void initView(View view)
    {
        txt_empty_marketplace = view.findViewById(R.id.txt_empty_marketplace);

        rv_marketplace = view.findViewById(R.id.rv_marketplace);
        rv_marketplace.setHasFixedSize(true);
        rv_marketplace.setLayoutManager(new GridLayoutManager(mContext, 2));

        ll_parent_marketplace = view.findViewById(R.id.ll_parent_marketplace);
        ll_parent_marketplace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void setListener()
    {
        rv_marketplace.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_marketplace, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                BaseActivity.SELLER_CATEGORY_ID = mGetSellersDataList.get(position).getIdCategory();

                mContext.changeFragment(new MarketProductFragment());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void sendGetSellersRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<GetSellers> call = service.sendGetSellersRequest();

        call.enqueue(new Callback<GetSellers>() {
            @Override
            public void onResponse(Call<GetSellers> call, Response<GetSellers> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("MARKET_PLACE", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                        if(response.body().getStatus() == 1){

                            rv_marketplace.setVisibility(View.VISIBLE);
                            txt_empty_marketplace.setVisibility(View.GONE);

                            mGetSellersDataList = response.body().getData();

                            setAdapter(mGetSellersDataList);

                        }else {
                            rv_marketplace.setVisibility(View.GONE);
                            txt_empty_marketplace.setVisibility(View.VISIBLE);
                            txt_empty_marketplace.setText(response.body().getMessage());
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetSellers> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setAdapter(ArrayList<GetSellersData> mGetSellersDataList)
    {
        Log.e("mGetSellersDataList",""+mGetSellersDataList.size());
        mGetSellersAdapter = new GetSellersAdapter(mContext, mGetSellersDataList);
        rv_marketplace.setAdapter(mGetSellersAdapter);
    }
}
