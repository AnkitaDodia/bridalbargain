package com.aasthasolutions.bridalbargain.fragment;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.autofill.AutofillManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.AuthorizationTabActivity;
import com.aasthasolutions.bridalbargain.activities.ForgotPasswordActivity;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.activities.PaymentActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.Login;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SignInFragment extends Fragment {


    final String TAG = "SIGN_IN";
    AuthorizationTabActivity mContext;
    private Button btn_login;
    private LinearLayout ll_parent_vendor_signin;
    private TextView txt_forgotten_password;
    private TextInputEditText tedt_login_email , tedt_login_password;
    private TextInputLayout txtlay_login_email, txtlay_login_password;



    public SignInFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_signin, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (AuthorizationTabActivity) getActivity();

        initviews(view);
        setListners();
        Log.e(TAG,"Mode : "+ BaseActivity.USER_MODE);
    }



    private void initviews(@NonNull View view) {


        tedt_login_email = view.findViewById(R.id.tedt_login_email);
        tedt_login_password = view.findViewById(R.id.tedt_login_password);

        txtlay_login_email = view.findViewById(R.id.txtlay_login_email);
        txtlay_login_password = view.findViewById(R.id.txtlay_login_password);

        btn_login = view.findViewById(R.id.btn_login);

        txt_forgotten_password = view.findViewById(R.id.txt_forgotten_password);

        ll_parent_vendor_signin = view.findViewById(R.id.ll_parent_vendor_signin);

        txtlay_login_email.setTypeface(mContext.getRegularFonts(mContext));
        txtlay_login_password.setTypeface(mContext.getRegularFonts(mContext));

        mContext.overrideFonts(ll_parent_vendor_signin, mContext);

    }

    private void setListners(){

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(tedt_login_email.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext,"Please enter email address",Toast.LENGTH_LONG).show();
                } else if (!BaseActivity.isValidEmail(tedt_login_email.getText().toString())) {
//                    edt_login_email.setError("Please enter valid email");
                    Toast.makeText(mContext,"Please enter a valid email address",Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(tedt_login_password.getText().toString().trim())) {
//                    edt_login_password.setError("Please enter valid password");
                    Toast.makeText(mContext,"Please enter password",Toast.LENGTH_LONG).show();
                } else {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendLoginRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        txt_forgotten_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, ForgotPasswordActivity.class);
                startActivity(i);
            }
        });


    }

    private void sendLoginRequest() {

        mContext.showWaitIndicator(true);

        String email = tedt_login_email.getText().toString();
        String password = tedt_login_password.getText().toString();

        BaseActivity.FCMTOKEN = mContext.getFCMTOKEN();

        Log.e(TAG,"Mode : "+ BaseActivity.USER_MODE);
        Log.e(TAG,"Email : "+tedt_login_email.getText().toString());
        Log.e(TAG,"Password : "+tedt_login_password.getText().toString());
        Log.e(TAG,"FCM Token : "+BaseActivity.FCMTOKEN);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<Login> call = service.sendLoginRequest(email, password, BaseActivity.USER_MODE, BaseActivity.FCMTOKEN);

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e(TAG,"RESPONSE : "+response.body().getMessage());
                        Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        if(response.body().getStatus() == 1){


                            Log.e(TAG,"RESPONSE : "+response.body().getData().getPhone());

//                            Log.e(TAG,"RESPONSE : "+response.body().getData().getFirstName());
//                            Log.e(TAG,"RESPONSE : "+response.body().getData().getFirstName());

                            UserDetails mUserDetails = new UserDetails();

                            if(BaseActivity.USER_MODE == 0){

                                Log.e(TAG,"RESPONSE : "+response.body().getData().getIdSeller());

                                mContext.setUserMode(0);
                                mUserDetails.setIdUser(response.body().getData().getIdSeller());
                                mUserDetails.setFirstName(response.body().getData().getFirstName());
                                mUserDetails.setLastName(response.body().getData().getLastName());
                                mUserDetails.setAddress(response.body().getData().getAddress());
                                mUserDetails.setCity(response.body().getData().getCity());
                                mUserDetails.setEmail(response.body().getData().getEmail());
                                mUserDetails.setPassword(response.body().getData().getPassword());
                                mUserDetails.setPhonenumber(response.body().getData().getPhone());
                                mUserDetails.setProfileImage(response.body().getData().getProfileImg());

                                mContext.setUserDetails(mContext, mUserDetails);

                                mContext.setLogin(1);
                                Intent i = new Intent(mContext, HomeActivity.class);
                                startActivity(i);
                                mContext.finish();

                            }else if(BaseActivity.USER_MODE == 1){

                                Log.e(TAG,"RESPONSE : "+response.body().getData().getIdVendor());
                                Log.e(TAG,"RESPONSE Due Payment: "+response.body().getData().getDuePayment());

                                if(response.body().getData().getDuePayment().equalsIgnoreCase("0")){
                                    mContext.setUserMode(1);
                                    mUserDetails.setIdUser(response.body().getData().getIdVendor());
                                    mUserDetails.setFirstName(response.body().getData().getFirstName());
                                    mUserDetails.setLastName(response.body().getData().getLastName());
                                    mUserDetails.setAddress(response.body().getData().getAddress());
                                    mUserDetails.setCity(response.body().getData().getCity());
                                    mUserDetails.setEmail(response.body().getData().getEmail());
                                    mUserDetails.setPassword(response.body().getData().getPassword());
                                    mUserDetails.setPhonenumber(response.body().getData().getPhone());
                                    mUserDetails.setProfileImage(response.body().getData().getProfileImg());

                                    mContext.setUserDetails(mContext, mUserDetails);

                                    mContext.setLogin(1);
                                    Intent i = new Intent(mContext, HomeActivity.class);
                                    startActivity(i);
                                    mContext.finish();
                                }else {
                                    Intent i = new Intent(mContext, PaymentActivity.class);
                                    i.putExtra("FROM_WHERE", "SIGNIN");
                                    i.putExtra("USER_ID", response.body().getData().getIdVendor());
                                    startActivity(i);
                                }

                            }else if(BaseActivity.USER_MODE == 2){

                                Log.e(TAG,"RESPONSE : "+response.body().getData().getIdBuyer());

                                mContext.setUserMode(2);
                                mUserDetails.setIdUser(response.body().getData().getIdBuyer());
                                mUserDetails.setFirstName(response.body().getData().getFirstName());
                                mUserDetails.setLastName(response.body().getData().getLastName());
                                mUserDetails.setAddress(response.body().getData().getAddress());
                                mUserDetails.setCity(response.body().getData().getCity());
                                mUserDetails.setEmail(response.body().getData().getEmail());
                                mUserDetails.setPassword(response.body().getData().getPassword());
                                mUserDetails.setPhonenumber(response.body().getData().getPhone());
                                mUserDetails.setProfileImage(response.body().getData().getProfileImg());

                                mContext.setUserDetails(mContext, mUserDetails);

                                mContext.setLogin(1);
                                Intent i = new Intent(mContext, HomeActivity.class);
                                startActivity(i);
                                mContext.finish();

                            }



                        }else {

                        }


                    }


                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG,"onFailure"+t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
