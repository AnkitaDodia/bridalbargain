package com.aasthasolutions.bridalbargain.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.AuthorizationTabActivity;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.adapter.SellerProductAdapter;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.customview.SquareImageView;
import com.aasthasolutions.bridalbargain.model.GetSellersData;
import com.aasthasolutions.bridalbargain.model.SellerProduct;
import com.aasthasolutions.bridalbargain.model.SellerProductData;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MarketProductFragment extends Fragment {

    HomeActivity mContext;

    RecyclerView rv_vendors;

    LinearLayout ll_parent_vendors, linear_search_layout, ll_search;

    SellerProductAdapter mSellerProductAdapter;

    TextView txt_empty_vendors;

    EditText search;

    ArrayList<SellerProductData> mGetSellerProductDataList = new ArrayList<>();
    private static ArrayList<SellerProductData> tempGetSellerProductDataList = new ArrayList<>();

    public MarketProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vendors, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        mContext.showBackButton(true);
        mContext.isShowBackIcon = true;

        initView(view);
        setListener();

        if (BaseActivity.CheckInternet(mContext)) {
            sendSellerProductRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void initView(View view)
    {
        ll_parent_vendors = view.findViewById(R.id.ll_parent_vendors);
        rv_vendors = view.findViewById(R.id.rv_vendors);
//        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(mContext, R.dimen.recycler_spacing);
//        rv_vendors.addItemDecoration(itemDecoration);
        rv_vendors.setHasFixedSize(true);
        rv_vendors.setLayoutManager(new GridLayoutManager(mContext, 2));

        txt_empty_vendors = view.findViewById(R.id.txt_empty_vendors);

        search = view.findViewById(R.id.search_vendor);
        ll_search = view.findViewById(R.id.ll_search);
        linear_search_layout = view.findViewById(R.id.linear_search_layout);
        linear_search_layout.setVisibility(View.GONE);

    }

    private void setListener()
    {

        ll_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(search.getText().toString().trim())){

                    Toast.makeText(mContext, "Please enter some text to search", Toast.LENGTH_LONG).show();

                }else {
                    sendSearchProductRequest();
                }

            }
        });

        ll_search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                View view = mContext.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });


        ll_parent_vendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                if(TextUtils.isEmpty(search.getText().toString().trim())){
                    sendSellerProductRequest();
                }
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub

            }
        });
    }



    private void sendSellerProductRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Log.e("SELLER_PRODUCT", "RESPONSE : " + BaseActivity.SELLER_CATEGORY_ID);
        String mIdSeller = "";

        if (mContext.getLogin() == 1) {

            UserDetails mUserDetails = mContext.getUserDetails(mContext);
            mIdSeller = mUserDetails.getIdUser();

        }else {

        }

        Log.e("SELLER_PRODUCT", "mIdSeller  : " + mIdSeller);

        Call<SellerProduct> call = service.sendSellerProductRequest(BaseActivity.SELLER_CATEGORY_ID, mIdSeller);

        call.enqueue(new Callback<SellerProduct>() {
            @Override
            public void onResponse(Call<SellerProduct> call, Response<SellerProduct> response) {
                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("SELLER_PRODUCT", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));
                        if (response.body().getStatus() == 1) {


                            linear_search_layout.setVisibility(View.VISIBLE);
                            rv_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setVisibility(View.GONE);

                            mGetSellerProductDataList.clear();
                            mGetSellerProductDataList = response.body().getData();
                            tempGetSellerProductDataList = mGetSellerProductDataList;
//                            for (int i = 0; i < response.body().getData().size(); i++) {
//
//                                mGetSellerProductDataList.add(response.body().getData().get(i));
//                                Log.e("SELLER_PRODUCT", "RESPONSE : " + response.body().getData().get(i).getProductName());
//                            }

//                            ll_parent_vendors.setBackgroundColor(Color.parseColor("#33406A"));
                            PrepareAdapter(mGetSellerProductDataList);
//                            mSellerProductAdapter = new  SellerProductAdapter(mContext, mGetSellerProductDataList);
//                            rv_vendors.setAdapter(mSellerProductAdapter);

                        } else {

                            linear_search_layout.setVisibility(View.GONE);
                            rv_vendors.setVisibility(View.GONE);
                            txt_empty_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setText(response.body().getMessage());
                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SellerProduct> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }



    private void sendSearchProductRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Log.e("SEARCH_PRODUCT", "SEARCH STRING : " +search.getText().toString().trim());
        Log.e("SEARCH_PRODUCT", "CATEGORY_ID : " +BaseActivity.SELLER_CATEGORY_ID);

        Call<SellerProduct> call = service.sendSearchProductRequest(search.getText().toString().trim(), BaseActivity.SELLER_CATEGORY_ID);

        call.enqueue(new Callback<SellerProduct>() {
            @Override
            public void onResponse(Call<SellerProduct> call, Response<SellerProduct> response) {
                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("SEARCH_PRODUCT", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));
                        if (response.body().getStatus() == 1) {

                            rv_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setVisibility(View.GONE);

                            mGetSellerProductDataList.clear();
                            mGetSellerProductDataList = response.body().getData();
                            tempGetSellerProductDataList = mGetSellerProductDataList;
//                            for (int i = 0; i < response.body().getData().size(); i++) {
//
//                                mGetSellerProductDataList.add(response.body().getData().get(i));
//                                Log.e("SELLER_PRODUCT", "RESPONSE : " + response.body().getData().get(i).getProductName());
//                            }

//                            ll_parent_vendors.setBackgroundColor(Color.parseColor("#33406A"));
                            PrepareAdapter(mGetSellerProductDataList);
//                            mSellerProductAdapter = new  SellerProductAdapter(mContext, mGetSellerProductDataList);
//                            rv_vendors.setAdapter(mSellerProductAdapter);

                            BaseActivity.hideKeyboard(mContext);


                        } else {

                            rv_vendors.setVisibility(View.GONE);
                            txt_empty_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setText(response.body().getMessage());
                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SellerProduct> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void PrepareAdapter(ArrayList<SellerProductData> mGetSellerProductDataList) {
        mSellerProductAdapter = new SellerProductAdapter(mContext, mGetSellerProductDataList, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View v, int position) {

                BaseActivity.CHAT_ROLE = MarketProductFragment.this.mGetSellerProductDataList.get(position).getRole();

                Log.e("TO", "TO : " + MarketProductFragment.this.mGetSellerProductDataList.get(position).getIdSeller());

                BaseActivity.CHAT_USER_ID = MarketProductFragment.this.mGetSellerProductDataList.get(position).getIdSeller();
                BaseActivity.CHAT_NAME = MarketProductFragment.this.mGetSellerProductDataList.get(position).getFirstName();
                BaseActivity.CHAT_IMAGE = MarketProductFragment.this.mGetSellerProductDataList.get(position).getProfileImg();

                UserDetails mUserDetails = mContext.getUserDetails(mContext);
                BaseActivity.USER_IMAGE = mUserDetails.getProfileImage();

                BaseActivity.hideKeyboard(mContext);

                if (v.getId() == R.id.ll_chat) {
                    if (mContext.getLogin() == 0) {

                        Intent i = new Intent(mContext, AuthorizationTabActivity.class);
                        startActivity(i);

                    } else {


                        mContext.changeFragment(new ChatingFragment());
//                        Toast.makeText(mContext, "role : "+mGetSellerProductDataList.get(position).getRole(), Toast.LENGTH_SHORT).show();
                    }

                    Log.e("", "" + MarketProductFragment.this.mGetSellerProductDataList.get(position).getRole());


                } else if (v.getId() == R.id.ll_row_vendor_services) {

                    BaseActivity.SELLER_PRODUCT_ID = MarketProductFragment.this.mGetSellerProductDataList.get(position).getIdProduct();

//                    Intent i = new Intent(mContext, MarketProductDetailsActivity.class);
//                    startActivity(i);

                    mContext.changeFragment(new ProductDetailsFragment());

                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });
        rv_vendors.setAdapter(mSellerProductAdapter);
    }

    /****************************************** Adapter ******************************************/

    /*public class SellerProductAdapter extends RecyclerView.Adapter<SellerProductAdapter.MyViewHolder>
    {
        ArrayList<SellerProductData> mSellerProductDataList = new ArrayList<>();
        ArrayList<SellerProductData> arraylist = new ArrayList<>();

        HomeActivity mContext;

        private final BaseActivity.ClickListener listener;

        public SellerProductAdapter(HomeActivity mContext, ArrayList<SellerProductData> mList,
                                    BaseActivity.ClickListener listener)
        {
            this.mContext = mContext;
            this.mSellerProductDataList = mList;
            this.listener = listener;
            this.arraylist = mList;
        }

        @Override
        public SellerProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_vendor_services, null);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final SellerProductAdapter.MyViewHolder holder, final int position) {
            SellerProductData mSellerProductData = mSellerProductDataList.get(position);

            Glide.with(mContext).load(mSellerProductData.getProductImg())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.progress_market_product.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            holder.img_vendor_services.setImageDrawable(resource);
                            holder.progress_market_product.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.img_vendor_services);

            Log.e("getProductImg",""+mSellerProductData.getProductImg());

            holder.txt_service_title.setText(mSellerProductData.getProductName());
            holder.txt_service_price.setText("$"+mSellerProductData.getProductPrice());
//        holder.txt_service_messages.setText("Message");
        }

        @Override
        public int getItemCount() {
            return mSellerProductDataList.size();
        }

        *//*@Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        mSellerProductDataList = tempGetSellerProductDataList;
                    } else {
                        ArrayList<SellerProductData> filteredList = new ArrayList<>();
                        for (SellerProductData row : arraylist) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getProductName().toLowerCase().contains(charString.toLowerCase()) || row.getDescription().toLowerCase().contains(charString.toLowerCase())){
                                filteredList.add(row);
                            }
//                            if(row.getDescription().toLowerCase().contains(charString.toLowerCase())){
//
//                            }
                        }

                        mSellerProductDataList = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mSellerProductDataList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mSellerProductDataList = (ArrayList<SellerProductData>) filterResults.values;
                    mGetSellerProductDataList = (ArrayList<SellerProductData>) filterResults.values;
                    Log.e("Size",""+mSellerProductDataList.size());
                    if(mSellerProductDataList.size() > 0)
                    {
                        notifyDataSetChanged();
                    }
                    else
                    {
                        PrepareAdapter(mSellerProductDataList);
                    }
                }
            };
        }*//*

        public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {
            FrameLayout ll_row_vendor_services;

            LinearLayout ll_chat;
            SquareImageView img_vendor_services;

            TextView txt_service_title, txt_service_price;

            private WeakReference<BaseActivity.ClickListener> listenerRef;

            ProgressBar progress_market_product;

            public MyViewHolder(View itemView)
            {
                super(itemView);

                listenerRef = new WeakReference<>(listener);

                ll_row_vendor_services = itemView.findViewById(R.id.ll_row_vendor_services);
                img_vendor_services = itemView.findViewById(R.id.img_vendor_services);
                txt_service_title = itemView.findViewById(R.id.txt_service_title);
                txt_service_price = itemView.findViewById(R.id.txt_service_price);
                ll_chat = itemView.findViewById(R.id.ll_chat);
//            txt_service_messages = itemView.findViewById(R.id.txt_service_messages);

                mContext.overrideFonts(ll_row_vendor_services, mContext);
                ll_chat.setOnClickListener(this);
                ll_row_vendor_services.setOnClickListener(this);

                progress_market_product = itemView.findViewById(R.id.progress_market_product);
            }

            // onClick Listener for view
            @Override
            public void onClick(View v) {

           *//* if (v.getId() == ll_chat.getId()) {
                Toast.makeText(v.getContext(), "ITEM PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(v.getContext(), "ROW PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            }*//*

                listenerRef.get().onClick(v, getAdapterPosition());
            }
        }
    }*/
}
