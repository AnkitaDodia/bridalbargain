package com.aasthasolutions.bridalbargain.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.AuthorizationTabActivity;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.customview.SquareImageView;
import com.aasthasolutions.bridalbargain.model.SellerSingleProduct;
import com.aasthasolutions.bridalbargain.model.VendorSingleService;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ServiceDetailsFragment extends Fragment {

    HomeActivity mContext;

    LinearLayout layout_detail_main, ll_market_details_message, ll_discription;
    SquareImageView img_detail;
    TextView txt_product_name_detail, txt_product_price_detail, txt_lable_product_discription, txt_product_discription, txt_soldby_msg, txt_product_provider, txt_lable_manufacture_details,
            txt_lable_product_email_detail, txt_product_email_detail, txt_lable_product_address_detail, txt_product_address_detail, txt_lable_product_contact, txt_product_contact, txt_product_message;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_market_product_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();
        mContext.showBackButton(true);
        mContext.isShowBackIcon = true;
        InitViews(view);

        if (BaseActivity.CheckInternet(mContext)) {
            sendVendorSingleServicesRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews(View view)
    {
        layout_detail_main = view.findViewById(R.id.layout_detail_main);
        ll_market_details_message = view.findViewById(R.id.ll_market_details_message);
        ll_discription = view.findViewById(R.id.ll_discription);

       /* image_product_detail = view.findViewById(R.id.image_product_detail);

        text_product_name_detail = view.findViewById(R.id.text_product_name_detail);
        text_product_price_detail = view.findViewById(R.id.text_product_price_detail);
        text_product_owner_name_detail = view.findViewById(R.id.text_product_owner_name_detail);
        text_product_email_detail = view.findViewById(R.id.text_product_email_detail);
        text_product_address_detail = view.findViewById(R.id.text_product_address_detail);
        text_product_discription = view.findViewById(R.id.text_product_discription);
        text_product_contact = view.findViewById(R.id.text_product_contact);

        text_product_message = view.findViewById(R.id.text_product_message);
        text_product_message.setText("CONTACT TO VENDOR");

        text_product_provider = view.findViewById(R.id.text_product_provider);
        img_profile_pic = view.findViewById(R.id.img_profile_pic);
        txt_soldby_msg = view.findViewById(R.id.txt_soldby_msg);
        txt_soldby_msg.setText("SERVICE PROVIDER");*/

        img_detail = view.findViewById(R.id.img_detail);

        txt_product_name_detail = view.findViewById(R.id.txt_product_name_detail);
        txt_product_price_detail = view.findViewById(R.id.txt_product_price_detail);
        txt_lable_product_discription = view.findViewById(R.id.txt_lable_product_discription);
        txt_product_discription = view.findViewById(R.id.txt_product_discription);

        txt_soldby_msg = view.findViewById(R.id.txt_soldby_msg);
        txt_product_provider = view.findViewById(R.id.txt_product_provider);
        txt_lable_manufacture_details = view.findViewById(R.id.txt_lable_manufacture_details);
        txt_lable_manufacture_details.setVisibility(View.GONE);

        txt_lable_product_email_detail  = view.findViewById(R.id.txt_lable_product_email_detail);
        txt_product_email_detail = view.findViewById(R.id.txt_product_email_detail);

        txt_lable_product_address_detail = view.findViewById(R.id.txt_lable_product_address_detail);
        txt_product_address_detail = view.findViewById(R.id.txt_product_address_detail);

        txt_lable_product_contact = view.findViewById(R.id.txt_lable_product_contact);
        txt_product_contact = view.findViewById(R.id.txt_product_contact);


        txt_product_message = view.findViewById(R.id.txt_product_message);
        txt_product_message.setText("CONTACT VENDOR");

        txt_soldby_msg.setText("SERVICE PROVIDER");

        mContext.overrideFonts(layout_detail_main, mContext);

        layout_detail_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ll_market_details_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mContext.getLogin() == 0) {

                    Intent i = new Intent(mContext, AuthorizationTabActivity.class);
                    startActivity(i);

                } else {

                    mContext.changeFragment(new ChatingFragment());
//                    Toast.makeText(mContext, "Working on it", Toast.LENGTH_SHORT).show();

                }
            }
        });

        txt_soldby_msg.setTypeface(mContext.getRegularBoldFonts(mContext));
        txt_product_provider.setTypeface(mContext.getRegularBoldFonts(mContext));
//        txt_lable_manufacture_details.setTypeface(mContext.getRegularBoldFonts(mContext));

//        ll_discription.setVisibility(View.GONE);
    }


    private void sendVendorSingleServicesRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Log.e("SELLER_PRODUCT_DETAILS","RESPONSE : "+BaseActivity.SERVICE_ID);

        Call<VendorSingleService> call = service.sendVendorSingleServicesRequest(BaseActivity.SERVICE_ID);

        call.enqueue(new Callback<VendorSingleService>() {
            @Override
            public void onResponse(Call<VendorSingleService> call, Response<VendorSingleService> response) {
                mContext.showWaitIndicator(false);

                Log.e("SELLER_PRODUCT_DETAILS","RESPONSE : "+response.code());

                try {
                    if (response.code() == 200) {

                        Log.e("SELLER_PRODUCT_DETAILS","RESPONSE : "+response.body().getStatus());

                        if(response.body().getStatus() == 1){

                            Log.e("SELLER_PRODUCT_DETAILS","RESPONSE : "+response.body().getData().getServiceImg());




                           /* Glide.with(mContext)
                                    .load(response.body().getData().getServiceImg())
                                    .into(image_product_detail);

                            text_product_name_detail.setText(response.body().getData().getServiceName());
                            text_product_price_detail.setText("$"+response.body().getData().getServicePrice());
                            text_product_owner_name_detail.setText(response.body().getData().getFirstName());
                            text_product_email_detail.setText(response.body().getData().getEmail());
                            text_product_address_detail.setText(response.body().getData().getAddress());

                            Glide.with(mContext)
                                    .load(response.body().getData().getProfileImg())
                                    .into(img_profile_pic);

//                            text_product_provider.setText(response.body().getData().getFirstName()+" "+response.body().getData().getLastName());

                            text_product_discription.setText(response.body().getData().getDescription());
                            text_product_contact.setText(response.body().getData().getPhone());

                            String upperString = response.body().getData().getFirstName().substring(0,1).toUpperCase() + response.body().getData().getFirstName().substring(1);
                            text_product_provider.setText(upperString+" "+response.body().getData().getLastName());

                            Glide.with(mContext)
                                    .load(response.body().getData().getServiceImg())
                                    .into(img_detail);*/

                            Glide.with(mContext)
                                    .load(response.body().getData().getServiceImg())
                                    .into(img_detail);

                            txt_product_name_detail.setText(response.body().getData().getServiceName());
                            txt_product_price_detail.setText("$"+response.body().getData().getServicePrice());
                            txt_product_discription.setText(response.body().getData().getDescription());

                            String upperString = response.body().getData().getFirstName().substring(0,1).toUpperCase() + response.body().getData().getFirstName().substring(1);
                            txt_product_provider.setText(upperString+" "+response.body().getData().getLastName());


                            txt_product_email_detail.setText(": "+response.body().getData().getEmail());
                            txt_product_address_detail.setText(response.body().getData().getAddress());
                            txt_product_contact.setText(": "+response.body().getData().getPhone());


                        }else {


                        }

                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<VendorSingleService> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
