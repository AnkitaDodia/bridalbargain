package com.aasthasolutions.bridalbargain.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.adapter.GetVendorsAdapter;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.GetVendors;
import com.aasthasolutions.bridalbargain.model.GetVendorsData;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class VendorsFragment extends Fragment {

    HomeActivity mContext;

    RecyclerView rv_vendors;

    GetVendorsAdapter mGetVendorsAdapter;

    TextView txt_empty_vendors;

    LinearLayout linear_search_layout;

    ArrayList<GetVendorsData> mGetVendorsDataList = new ArrayList<>();

    public VendorsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vendors, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity)getActivity();

        initView(view);
        setClickListener();

        if (BaseActivity.CheckInternet(mContext)) {
            sendGetVendorRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void initView(View view)
    {
        linear_search_layout = view.findViewById(R.id.linear_search_layout);
        linear_search_layout.setVisibility(View.GONE);

        txt_empty_vendors = view.findViewById(R.id.txt_empty_vendors);

        rv_vendors = view.findViewById(R.id.rv_vendors);
        rv_vendors.setHasFixedSize(true);
        rv_vendors.setLayoutManager(new GridLayoutManager(mContext, 2));

    }

    private void setClickListener()
    {
        rv_vendors.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_vendors, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BaseActivity.SERVICE_CATEGORY_ID = mGetVendorsDataList.get(position).getIdCategory();

                mContext.changeFragment(new VendorServicesFragment());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void sendGetVendorRequest() {

//        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<GetVendors> call = service.sendGetVendorsRequest();

        call.enqueue(new Callback<GetVendors>() {
            @Override
            public void onResponse(Call<GetVendors> call, Response<GetVendors> response) {


                try {
                    if (response.code() == 200) {

                        if(response.body().getStatus() == 1){
                            Log.e("VENDOR", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                            rv_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setVisibility(View.GONE);

                            mGetVendorsDataList = response.body().getData();

                            setAdapter(mGetVendorsDataList);
                        }else {

                            rv_vendors.setVisibility(View.GONE);
                            txt_empty_vendors.setVisibility(View.VISIBLE);
                            txt_empty_vendors.setText(response.body().getMessage());
                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
//                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetVendors> call, Throwable t) {
//                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void setAdapter(ArrayList<GetVendorsData> mGetVendorsDataList)
    {
        Log.e("mGetVendorsDataList",""+mGetVendorsDataList.size());
        mGetVendorsAdapter = new  GetVendorsAdapter(mContext, mGetVendorsDataList);
        rv_vendors.setAdapter(mGetVendorsAdapter);
    }
}
