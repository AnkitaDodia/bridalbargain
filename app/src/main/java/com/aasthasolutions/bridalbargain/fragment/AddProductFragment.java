package com.aasthasolutions.bridalbargain.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.CropActivity;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.AddProduct;
import com.aasthasolutions.bridalbargain.model.GetSellers;
import com.aasthasolutions.bridalbargain.model.SellerSingleProduct;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.restinterface.RestInterface;
import com.aasthasolutions.bridalbargain.utility.CameraUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class AddProductFragment extends Fragment {


    HomeActivity mContext;

    ImageView img_product;
//    EditText edt_product_name, edt_product_price, edt_product_discription;

    TextInputEditText tedt_product_name, tedt_product_price, tedt_product_discription;

    private TextInputLayout txtlay_product_name, txtlay_product_price, txtlay_product_discription;

    LinearLayout lay_parent_addproduct;

    ProgressBar progress_add_product;

    Button btn_label_add_product;
    Spinner spnr_categories_name;
    TextView txt_lable_add_image;
    List<String> categories = new ArrayList<String>();
    List<String> categoriesId = new ArrayList<String>();

    private String mIdUser, mCategoryId, mIdProduct;

    //For image selection from gallery
    private static final int PICK_IMAGE = 1;
    private static final int CROP_IMAGE = 2;
    public static final int CAMERA_CAPTURE = 100;
    Bitmap bitmap;
    String mCurrentPhotoPath, filemanagerstring, selectedImagePath = null;
    Uri selectedImageUri;

    public AddProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_product, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomeActivity) getActivity();

        mContext.showAddProductLayout(false);
        mContext.showBackButton(true);
        mContext.isShowBackIcon = true;

        InitViews(view);
        SetListners();

        if (BaseActivity.CheckInternet(mContext)) {
            sendGetSellersRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }

        UserDetails mUserDetails = mContext.getUserDetails(mContext);
        mIdUser = mUserDetails.getIdUser();

        if (BaseActivity.IS_ADD_PRODUCT) {

            txt_lable_add_image.setText("Tap on above image to change it");
            btn_label_add_product.setText("ADD PRODUCT");

        } else {

            txt_lable_add_image.setText("Tap on above image to change it");
            btn_label_add_product.setText("UPDATE PRODUCT");
//            sendSellerProductRequest();
        }

        tedt_product_price.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("$ ")){
                    tedt_product_price.setText("$ ");
                    Selection.setSelection(tedt_product_price.getText(), tedt_product_price.getText().length());

                }

            }
        });

        tedt_product_discription.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (tedt_product_discription.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });
    }

    private void SetListners() {

        img_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                requestStoragePermission();
                ShowPicOption();
            }
        });

        btn_label_add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(tedt_product_name.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "Please enter product name", Toast.LENGTH_LONG).show();
                } else if (tedt_product_price.getText().toString().trim().equalsIgnoreCase("$")) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "Please enter product price", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(tedt_product_discription.getText().toString().trim())) {
//                    edt_login_email.setError("Please enter email");
                    Toast.makeText(mContext, "Please enter product description", Toast.LENGTH_LONG).show();
                } else {

                    if (BaseActivity.IS_ADD_PRODUCT) {

                        if (BaseActivity.CheckInternet(mContext)) {
                            sendAddProductRequest();
                        } else {
                            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        if (BaseActivity.CheckInternet(mContext)) {
                            sendEditProductRequest();
                        } else {
                            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        lay_parent_addproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void InitViews(View view) {

        img_product = view.findViewById(R.id.img_product);

        tedt_product_name = view.findViewById(R.id.tedt_product_name);
        tedt_product_price = view.findViewById(R.id.tedt_product_price);
        tedt_product_discription = view.findViewById(R.id.tedt_product_discription);

        txtlay_product_name = view.findViewById(R.id.txtlay_product_name);
        txtlay_product_price = view.findViewById(R.id.txtlay_product_price);
        txtlay_product_discription = view.findViewById(R.id.txtlay_product_discription);

        txtlay_product_name.setTypeface(mContext.getRegularFonts(mContext));
        txtlay_product_price.setTypeface(mContext.getRegularFonts(mContext));
        txtlay_product_discription.setTypeface(mContext.getRegularFonts(mContext));

        spnr_categories_name = view.findViewById(R.id.spnr_categories_name);

        btn_label_add_product = view.findViewById(R.id.btn_label_add_product);

        txt_lable_add_image = view.findViewById(R.id.txt_lable_add_image);

        tedt_product_price.setSelection(tedt_product_price.getText().toString().length());

        lay_parent_addproduct = view.findViewById(R.id.lay_parent_addproduct);

        progress_add_product = view.findViewById(R.id.progress_add_product);

        mContext.overrideFonts(lay_parent_addproduct, mContext);
    }

    private void sendAddProductRequest() {

        final String TAG = "ADD_PRODUCT";
        mContext.showWaitIndicator(true);

        String price = tedt_product_price.getText().toString();

        String temp[] = price.split(" ");
        price = temp[1];

        Log.e(TAG, "Product Name : " + tedt_product_name.getText().toString());
        Log.e(TAG, "Product Price : " + tedt_product_price.getText().toString());
        Log.e(TAG, "Product Description : " + price);


        MultipartBody.Part ProductPicPart = null;

        if (selectedImagePath == null) {


        } else {
//            File file = new File(selectedImagePath);
            File file = BitmaptoFile();
            ProductPicPart = getbodyPart(file, "product_img");
        }

        RequestBody productname = RequestBody.create(MediaType.parse("text/plain"), tedt_product_name.getText().toString());
        RequestBody product_price = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody product_description = RequestBody.create(MediaType.parse("text/plain"), tedt_product_discription.getText().toString());
        RequestBody id_seller = RequestBody.create(MediaType.parse("text/plain"), mIdUser);
        RequestBody id_category = RequestBody.create(MediaType.parse("text/plain"), mCategoryId);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<AddProduct> call = service.sendAddProductRequest(id_seller, id_category, productname, product_price, product_description, ProductPicPart);

        call.enqueue(new Callback<AddProduct>() {
            @Override
            public void onResponse(Call<AddProduct> call, Response<AddProduct> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e(TAG, "ADD RESPONSE : " + response.body().getMessage());
                        Log.e(TAG, "ADD RESPONSE Multipart : " + response.body().getData().get(0).getProductImg());

                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        mContext.showBackButton(false);
                        mContext.isShowBackIcon = false;
                        BaseActivity.hideKeyboard(mContext);
                        mContext.changeFragment(new HomeSellerFragment());
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddProduct> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendEditProductRequest() {
        final String TAG = "EDIT_PRODUCT";
        mContext.showWaitIndicator(true);

        String price = tedt_product_price.getText().toString();

        String temp[] = price.split(" ");
        price = temp[1];

        Log.e(TAG, "Product Name : " + tedt_product_name.getText().toString());
        Log.e(TAG, "Product Price : " + price);
        Log.e(TAG, "Product Description : " + tedt_product_discription.getText().toString());
        Log.e(TAG, "mIdUser : " + mIdUser);
        Log.e(TAG, "mCategoryId : " + mCategoryId);
        Log.e(TAG, "mIdProduct : " + mIdProduct);

        MultipartBody.Part productpicbody = null;

        if (selectedImagePath == null) {

        } else {
//            File file = new File(selectedImagePath);
            File file = BitmaptoFile();
            productpicbody = getbodyPart(file, "product_img");
        }

        RequestBody productname = RequestBody.create(MediaType.parse("text/plain"), tedt_product_name.getText().toString());
        RequestBody product_price = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody product_description = RequestBody.create(MediaType.parse("text/plain"), tedt_product_discription.getText().toString());
        RequestBody id_seller = RequestBody.create(MediaType.parse("text/plain"), mIdUser);
        RequestBody id_category = RequestBody.create(MediaType.parse("text/plain"), mCategoryId);
        RequestBody id_product = RequestBody.create(MediaType.parse("text/plain"), mIdProduct);


        Log.e(TAG, "productpicbody : " + productpicbody);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<AddProduct> call = service.sendEditProductRequest(id_product, id_seller, id_category, productname, product_price, product_description, productpicbody);

        call.enqueue(new Callback<AddProduct>() {
            @Override
            public void onResponse(Call<AddProduct> call, Response<AddProduct> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e(TAG, "RESPONSE : " + response.body().getMessage());
                        Log.e(TAG, "RESPONSE Multipart: " + response.body().getData().get(0).getProductImg());
                        Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        mContext.showBackButton(false);
                        mContext.isShowBackIcon = false;
                        BaseActivity.hideKeyboard(mContext);
                        mContext.changeFragment(new HomeSellerFragment());
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddProduct> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d(TAG, "onFailure : " + t.toString());Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public MultipartBody.Part getbodyPart(@NonNull File file, @NonNull String fieldname) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(fieldname, file.getName(), requestFile);
    }

    private void showProgressOnImage()
    {
        if (BaseActivity.IS_ADD_PRODUCT)
        {
            progress_add_product.setVisibility(View.GONE);
            img_product.setImageResource(R.drawable.product_placeholder);
        }
        else
        {
            progress_add_product.setVisibility(View.VISIBLE);
        }
    }

    private void sendSellerProductRequest() {

//        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        BaseActivity.SELLER_PRODUCT_ID = BaseActivity.mGetSellerProductDataList.get(BaseActivity.SELLER_PRODUCT_POSITION).getIdProduct();
        Log.e("PRODUCT_BY_SELLER", "PRODUCT ID : " + BaseActivity.SELLER_PRODUCT_ID);

        Call<SellerSingleProduct> call = service.sendSellerSingleProductRequest(BaseActivity.SELLER_PRODUCT_ID);

        call.enqueue(new Callback<SellerSingleProduct>() {
            @Override
            public void onResponse(Call<SellerSingleProduct> call, Response<SellerSingleProduct> response) {
                mContext.showWaitIndicator(false);

                Log.e("PRODUCT_BY_SELLER", "RESPONSE : " + response.code());

                try {
                    if (response.code() == 200) {

                        Log.e("PRODUCT_BY_SELLER", "RESPONSE : " + response.body().getStatus());

                        if (response.body().getStatus() == 1) {

                            Log.e("PRODUCT_BY_SELLER", "RESPONSE IdProduct: " + response.body().getData().getIdProduct());
                            mIdProduct = response.body().getData().getIdProduct();

                            Log.e("IMAGE",""+response.body().getData().getProductImg());
                            Glide.with(mContext).load(response.body().getData().getProductImg())
                                    .listener(new RequestListener<String, GlideDrawable>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                            progress_add_product.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            progress_add_product.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(img_product);

                            String price = "$ "+response.body().getData().getProductPrice();
                            tedt_product_name.setText(response.body().getData().getProductName());
                            tedt_product_price.setText(price);

                            tedt_product_name.setSelection(response.body().getData().getProductName().length());
                            tedt_product_price.setSelection(price.length());


//                            text_product_provider.setText(response.body().getData().getFirstName()+" "+response.body().getData().getLastName());

                            tedt_product_discription.setVisibility(View.VISIBLE);
                            tedt_product_discription.setText(response.body().getData().getDescription());
                            tedt_product_discription.setSelection(response.body().getData().getDescription().length());


                            Log.e("PRODUCT_BY_SELLER", "R IdProduct : " + response.body().getData().getIdProduct());

                            Log.e("PRODUCT_BY_SELLER", "R IdCategory from api : " + response.body().getData().getIdCategory());
                            Log.e("PRODUCT_BY_SELLER", "R IdCategory size : " + categoriesId.size());

                            for (int i = 0; i < categoriesId.size(); i++) {

                                if (response.body().getData().getIdCategory().equalsIgnoreCase(categoriesId.get(i))) {

                                    Log.e("PRODUCT_BY_SELLER", "R IdCategory: " + response.body().getData().getIdCategory());
                                    spnr_categories_name.setSelection(i);
                                    break;
                                }

                            }

                        } else {


                        }

                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SellerSingleProduct> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }


    private void sendGetSellersRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<GetSellers> call = service.sendGetSellersRequest();

        call.enqueue(new Callback<GetSellers>() {
            @Override
            public void onResponse(Call<GetSellers> call, Response<GetSellers> response) {

//                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {


                        categories.clear();
                        categoriesId.clear();
                        for (int i = 0; i < response.body().getData().size(); i++) {

                            categories.add(response.body().getData().get(i).getCategoryName());
                            categoriesId.add(response.body().getData().get(i).getIdCategory());
                        }

                        CreateCategoriesSppiner();

                        if (BaseActivity.IS_ADD_PRODUCT) {

                            mContext.showWaitIndicator(false);
                            showProgressOnImage();
                        } else {
                            sendSellerProductRequest();
                        }
                    }
                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetSellers> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void CreateCategoriesSppiner() {

        final int listsize = categories.size();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, categories) {
            @Override
            public int getCount() {
                return (listsize); // Truncate the list
            }
        };
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnr_categories_name.setAdapter(dataAdapter);



        spnr_categories_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mCategoryId = categoriesId.get(position);
                Log.e("PRODUCT_BY_SELLER", "mCategoryId : " + mCategoryId);
                Log.e("PRODUCT_BY_SELLER", "mIdUser : " + mIdUser);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void ShowPicOption(){

        final String[] options = {"Camera", "Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Pick a picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]

                Log.e("OnClick ","Option : "+options[which]);

                String mOptions = options[which];
                if(mOptions.equalsIgnoreCase("Camera")){

                    if (CameraUtils.checkPermissions(mContext)) {
//            layout_include.setVisibility(View.GONE);
                        captureImage();
                    } else {
                        requestCameraPermission(BaseActivity.MEDIA_TYPE_IMAGE);
                    }

                }else if(mOptions.equalsIgnoreCase("Gallery")){

                    requestStoragePermission();
                }

            }
        });
        builder.show();

    }

    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission(final int type) {
        Dexter.withActivity(mContext)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == BaseActivity.MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    //********************* Coding for camera **************************//
    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(BaseActivity.MEDIA_TYPE_IMAGE);
        if (file != null) {
            BaseActivity.imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(mContext, file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE);
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(mContext);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    private void requestStoragePermission() {

        Dexter.withActivity(mContext)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted
                        OpenGallery();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void OpenGallery() {

        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int requestcode, Intent data) {
        super.onActivityResult(requestCode, requestcode, data);

        switch (requestCode) {
            case PICK_IMAGE:
                if (requestcode == Activity.RESULT_OK) {
//            Uri photoURI = FileProvider.getUriForFile(mContext,
//                    BuildConfig.APPLICATION_ID + ".provider",
//                    createImageFile());

                    selectedImageUri = data.getData();
                    String filePath = null;

                    try {
                        // OI FILE Manager
                        filemanagerstring = selectedImageUri.getPath();

                        // MEDIA GALLERY
                        selectedImagePath = mContext.getPath(selectedImageUri);
                        Log.e("selectedImagePath", "path : " + selectedImagePath);

                        if (selectedImagePath != null) {
                            filePath = selectedImagePath;
                        } else if (filemanagerstring != null) {
                            filePath = filemanagerstring;
                        } else {
                            Toast.makeText(mContext, "Unknown path", Toast.LENGTH_LONG).show();
                            Log.e("Bitmap", "Unknown path");
                        }

                        if (filePath != null) {
//                    bitmap = BitmapFactory.decodeFile(filePath);
                            bitmap = lessResolution(filePath, 200, 200);
                            img_product.setImageBitmap(bitmap);
                            BaseActivity.BmpCropped = bitmap;

                            Intent i = new Intent(mContext, CropActivity.class);
                            startActivityForResult(i, CROP_IMAGE);

                        } else {
                            bitmap = null;
                        }

                    } catch (Exception e) {
                        Toast.makeText(mContext, "Internal Error", Toast.LENGTH_LONG).show();
                        Log.e(e.getClass().getName(), e.getMessage(), e);
                    }
                }
                break;
            case CROP_IMAGE:

                img_product.setImageBitmap(BaseActivity.BmpCropped);

                break;

            case CAMERA_CAPTURE:

                if (requestcode == Activity.RESULT_OK) {
                    // Refreshing the gallery
                    try {

                        CameraUtils.refreshGallery(mContext.getApplicationContext(), mContext.imageStoragePath);

                        selectedImagePath = mContext.imageStoragePath;

                        Log.e("selectedImagePath", "Camera Path : "+selectedImagePath);

                        if (selectedImagePath != null) {

                            bitmap = lessResolution(selectedImagePath, 200, 200);
                            img_product.setImageBitmap(bitmap);
                            BaseActivity.BmpCropped = bitmap;

                            Intent i = new Intent(mContext, CropActivity.class);
                            startActivityForResult(i, CROP_IMAGE);

                        } else {
                            bitmap = null;
                        }

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                } else if (requestcode == Activity.RESULT_CANCELED) {
                    // user cancelled Image capture
                    Toast.makeText(mContext.getApplicationContext(),
                            "You cancelled image capture", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    // failed to capture image
                    Toast.makeText(mContext.getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }

                break;
        }
    }

    public static Bitmap lessResolution(String filePath, int width, int height) {
        int reqHeight = height;
        int reqWidth = width;
        BitmapFactory.Options options = new BitmapFactory.Options();

        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private File BitmaptoFile(){
        File imgfile = null;
        Log.e("UPDATE_PROFILE","Inside BitmaptoFile");
        try{
            //create a file to write bitmap data
            imgfile = new File(mContext.getCacheDir(), "profile_pic.jpg");
            imgfile.createNewFile();

//Convert bitmap to byte array
            Bitmap bitmap = BaseActivity.BmpCropped;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(imgfile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

        }catch (Exception e){

            Log.e("UPDATE_PROFILE","Exception :"+e);

        }

        return imgfile ;

    }

}