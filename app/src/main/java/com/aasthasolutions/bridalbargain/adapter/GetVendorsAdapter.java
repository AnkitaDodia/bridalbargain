package com.aasthasolutions.bridalbargain.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.model.GetVendorsData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class GetVendorsAdapter extends RecyclerView.Adapter<GetVendorsAdapter.MyViewHolder>
{
    ArrayList<GetVendorsData> mGetVendorsDataList = new ArrayList<>();

    HomeActivity mContext;

    public GetVendorsAdapter(HomeActivity mContext, ArrayList<GetVendorsData> mList)
    {
        this.mContext = mContext;
        this.mGetVendorsDataList = mList;
    }

    @Override
    public GetVendorsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_get_vendors, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final GetVendorsAdapter.MyViewHolder holder, final int position) {
        GetVendorsData mGetVendorsData = mGetVendorsDataList.get(position);

        Glide.with(mContext).load(mGetVendorsData.getImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.progress_market_place.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        holder.img_vendors.setImageDrawable(resource);
                        holder.progress_market_place.setVisibility(View.GONE);
                        return false;
                    }
                }).into(holder.img_vendors);

        holder.txt_vendor_title.setText(mGetVendorsData.getCategoryName());
    }

    @Override
    public int getItemCount() {
        return mGetVendorsDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout layout_get_vendor_main;

        ImageView img_vendors;

        TextView txt_vendor_title;

        ProgressBar progress_market_place;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            layout_get_vendor_main = itemView.findViewById(R.id.layout_get_vendor_main);

            img_vendors = itemView.findViewById(R.id.img_vendors);
            txt_vendor_title = itemView.findViewById(R.id.txt_vendor_title);

            progress_market_place = itemView.findViewById(R.id.progress_market_place);

            mContext.overrideFonts(layout_get_vendor_main,mContext);
        }
    }
}
