package com.aasthasolutions.bridalbargain.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.SingleChatingData;
import com.aasthasolutions.bridalbargain.utility.TimeAgo;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class GetSingleMessagesAdapter extends RecyclerView.Adapter<GetSingleMessagesAdapter.MyViewHolder>
{
    ArrayList<SingleChatingData> mSingleChatingDataList = new ArrayList<>();

    HomeActivity mContext;

    public GetSingleMessagesAdapter(HomeActivity mContext, ArrayList<SingleChatingData> mList)
    {
        this.mContext = mContext;
        this.mSingleChatingDataList = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_chatmessage_received, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SingleChatingData mSingleChatingData = mSingleChatingDataList.get(position);

//        LinearLayout.LayoutParams params = new
//                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT);
//        holder.lay_parent_chat_item.setLayoutParams(params);

        holder.txt_msg_body.setText(mSingleChatingData.getMessage());
        holder.txt_msg_time.setText(mSingleChatingData.getLastseen());
//        holder.txt_msg_time.setText(mSingleChatingData.getTime());

        holder.txt_msg_body.setTypeface(mContext.getRegularFonts(mContext));
        holder.txt_user_name.setTypeface(mContext.getRegularBoldFonts(mContext));

        holder.txt_user_name.setTypeface(mContext.getRegularBoldFonts(mContext));
        holder.txt_msg_body.setTypeface(mContext.getRegularFonts(mContext));
        holder.txt_msg_time.setTypeface(mContext.getRegularFonts(mContext));

        if (mSingleChatingData.getAlign().equalsIgnoreCase("right")) {

            holder.txt_user_name.setText("Me");

            Glide.with(mContext)
                    .load(BaseActivity.USER_IMAGE)
                    .into(holder.img_profile);

        } else {

            holder.txt_user_name.setText(BaseActivity.CHAT_NAME);

            Glide.with(mContext)
                    .load(BaseActivity.CHAT_IMAGE)
                    .into(holder.img_profile);

        }
    }

    @Override
    public int getItemCount() {
        return mSingleChatingDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
//        LinearLayout lay_parent_chat_item;

        TextView txt_msg_body, txt_msg_time, txt_user_name;
        ImageView img_profile;

        public MyViewHolder(View itemView)
        {
            super(itemView);

//            lay_parent_chat_item = itemView.findViewById(R.id.lay_parent_chat_item);

            txt_msg_body = (TextView) itemView.findViewById(R.id.text_message_body);
            txt_msg_time = (TextView) itemView.findViewById(R.id.text_message_time);
            txt_user_name = (TextView) itemView.findViewById(R.id.text_message_name);
            img_profile = (ImageView) itemView.findViewById(R.id.image_message_profile);

            mContext.overrideFonts(txt_msg_body,mContext);
            mContext.overrideFonts(txt_msg_time,mContext);
            mContext.overrideFonts(txt_user_name,mContext);
        }
    }
}
