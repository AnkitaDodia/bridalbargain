package com.aasthasolutions.bridalbargain.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.model.GetSellersData;
import com.aasthasolutions.bridalbargain.model.GetVendorsData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class GetSellersAdapter extends RecyclerView.Adapter<GetSellersAdapter.MyViewHolder>
{
    ArrayList<GetSellersData> mGetSellersDataList = new ArrayList<>();

    HomeActivity mContext;

    public GetSellersAdapter(HomeActivity mContext, ArrayList<GetSellersData> mList)
    {
        this.mContext = mContext;
        this.mGetSellersDataList = mList;
    }

    @Override
    public GetSellersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_get_vendors, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final GetSellersAdapter.MyViewHolder holder, final int position) {
        GetSellersData mGetSellersData = mGetSellersDataList.get(position);


        Glide.with(mContext).load(mGetSellersData.getImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.progress_market_place.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        holder.img_vendors.setImageDrawable(resource);
                        holder.progress_market_place.setVisibility(View.GONE);
                        return false;
                    }
                }).into(holder.img_vendors);

        holder.txt_vendor_title.setText(mGetSellersData.getCategoryName());
    }

    @Override
    public int getItemCount() {
        return mGetSellersDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout layout_get_vendor_main;

        ImageView img_vendors;

        TextView txt_vendor_title;

        ProgressBar progress_market_place;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            img_vendors = itemView.findViewById(R.id.img_vendors);
            txt_vendor_title = itemView.findViewById(R.id.txt_vendor_title);

            layout_get_vendor_main = itemView.findViewById(R.id.layout_get_vendor_main);

            progress_market_place = itemView.findViewById(R.id.progress_market_place);

            mContext.overrideFonts(layout_get_vendor_main,mContext);
        }
    }
}
