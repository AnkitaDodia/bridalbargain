//package com.aasthasolutions.bridalbargain.adapter;
//
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.aasthasolutions.bridalbargain.R;
//import com.aasthasolutions.bridalbargain.activities.HomeActivity;
//import com.aasthasolutions.bridalbargain.common.BaseActivity;
//import com.aasthasolutions.bridalbargain.model.GetVendorsData;
//import com.aasthasolutions.bridalbargain.model.VendorServicesData;
//import com.bumptech.glide.Glide;
//
//import java.lang.ref.WeakReference;
//import java.util.ArrayList;
//
///**
// * Created by My 7 on 14-Aug-18.
// */
//
//public class VendorServicesAdapter extends RecyclerView.Adapter<VendorServicesAdapter.MyViewHolder> {
//    ArrayList<VendorServicesData> mVendorServicesDataList = new ArrayList<>();
//
//    HomeActivity mContext;
//    private final BaseActivity.ClickListener listener;
//
//    public VendorServicesAdapter(HomeActivity mContext, ArrayList<VendorServicesData> mList, BaseActivity.ClickListener listener) {
//        this.mContext = mContext;
//        this.mVendorServicesDataList = mList;
//        this.listener = listener;
//    }
//
//    @Override
//    public VendorServicesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.row_vendor_services, null);
//
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(VendorServicesAdapter.MyViewHolder holder, final int position) {
//        VendorServicesData mVendorServicesData = mVendorServicesDataList.get(position);
//
//        Glide.with(mContext)
//                .load(mVendorServicesData.getServiceImg())
//                .into(holder.img_vendor_services);
//
//        holder.txt_service_title.setText(mVendorServicesData.getServiceName());
//        holder.txt_service_price.setText("$" + mVendorServicesData.getServicePrice());
////        holder.txt_service_messages.setText("Message");
//    }
//
//    @Override
//    public int getItemCount() {
//        return mVendorServicesDataList.size();
//    }
//
//    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//
//        FrameLayout ll_row_vendor_services;
//        LinearLayout ll_chat;
//        ImageView img_vendor_services;
//        TextView txt_service_title, txt_service_price;
//
//        private WeakReference<BaseActivity.ClickListener> listenerRef;
//
//        public MyViewHolder(View itemView) {
//            super(itemView);
//
//            listenerRef = new WeakReference<>(listener);
//
//            ll_row_vendor_services = itemView.findViewById(R.id.ll_row_vendor_services);
//            img_vendor_services = itemView.findViewById(R.id.img_vendor_services);
//            txt_service_title = itemView.findViewById(R.id.txt_service_title);
//            txt_service_price = itemView.findViewById(R.id.txt_service_price);
//            ll_chat = itemView.findViewById(R.id.ll_chat);
//
//            mContext.overrideFonts(ll_row_vendor_services, mContext);
//
//            ll_row_vendor_services.setOnClickListener(this);
//            ll_chat.setOnClickListener(this);
//
//        }
//
//        // onClick Listener for view
//        @Override
//        public void onClick(View v) {
//
//           /* if (v.getId() == ll_chat.getId()) {
//                Toast.makeText(v.getContext(), "ITEM PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(v.getContext(), "ROW PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
//            }*/
//
//            listenerRef.get().onClick(v, getAdapterPosition());
//        }
//    }
//}
