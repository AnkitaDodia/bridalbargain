package com.aasthasolutions.bridalbargain.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.customview.SquareImageView;
import com.aasthasolutions.bridalbargain.model.SellerProductData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class ProductBySellerAdapter extends RecyclerView.Adapter<ProductBySellerAdapter.MyViewHolder>
{
    ArrayList<SellerProductData> mSellerProductDataList = new ArrayList<>();

    HomeActivity mContext;

    private final BaseActivity.ClickListener listener;

    public ProductBySellerAdapter(HomeActivity mContext, ArrayList<SellerProductData> mList, BaseActivity.ClickListener listener)
    {
        this.mContext = mContext;
        this.mSellerProductDataList = mList;
        this.listener = listener;

    }

    @Override
    public ProductBySellerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_product_by_seller, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProductBySellerAdapter.MyViewHolder holder, final int position) {
        SellerProductData mSellerProductData = mSellerProductDataList.get(position);

        Glide.with(mContext).load(mSellerProductData.getProductImg())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.progress_my_items.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progress_my_items.setVisibility(View.GONE);
                        return false;
                    }
                }).into(holder.img_vendor_services);

        holder.txt_service_title.setText(mSellerProductData.getProductName());
        holder.txt_service_price.setText("$"+mSellerProductData.getProductPrice());
//        holder.txt_service_messages.setText("Message");
    }

    @Override
    public int getItemCount() {
        return mSellerProductDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        FrameLayout ll_row_product_by_seller;

        LinearLayout ll_delete_product, ll_edit_product;
        SquareImageView img_vendor_services;

        TextView txt_service_title, txt_service_price;

        ProgressBar progress_my_items;

        private WeakReference<BaseActivity.ClickListener> listenerRef;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            listenerRef = new WeakReference<>(listener);

            ll_row_product_by_seller = itemView.findViewById(R.id.ll_row_product_by_seller);
            img_vendor_services = itemView.findViewById(R.id.img_vendor_services);
            txt_service_title = itemView.findViewById(R.id.txt_service_title);
            txt_service_price = itemView.findViewById(R.id.txt_service_price);

            ll_delete_product = itemView.findViewById(R.id.ll_delete_product);
            ll_edit_product = itemView.findViewById(R.id.ll_edit_product);

            progress_my_items = itemView.findViewById(R.id.progress_my_items);

            mContext.overrideFonts(ll_row_product_by_seller, mContext);
            ll_delete_product.setOnClickListener(this);
            ll_edit_product.setOnClickListener(this);
            ll_row_product_by_seller.setOnClickListener(this);
        }

        // onClick Listener for view
        @Override
        public void onClick(View v) {

           /* if (v.getId() == ll_chat.getId()) {
                Toast.makeText(v.getContext(), "ITEM PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(v.getContext(), "ROW PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            }*/

            listenerRef.get().onClick(v, getAdapterPosition());
        }
    }
}
