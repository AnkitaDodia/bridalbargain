package com.aasthasolutions.bridalbargain.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;
import com.aasthasolutions.bridalbargain.common.BaseActivity;
import com.aasthasolutions.bridalbargain.model.UserDetails;
import com.aasthasolutions.bridalbargain.model.UserMessagesData;
import com.aasthasolutions.bridalbargain.utility.TimeAgo;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class GetMessagesAdapter extends RecyclerView.Adapter<GetMessagesAdapter.MyViewHolder>
{
    ArrayList<UserMessagesData> mUserMessagesDataList = new ArrayList<>();

    HomeActivity mContext;

    public GetMessagesAdapter(HomeActivity mContext, ArrayList<UserMessagesData> mList)
    {
        this.mContext = mContext;
        this.mUserMessagesDataList = mList;
    }

    @Override
    public GetMessagesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_get_messages, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GetMessagesAdapter.MyViewHolder holder, final int position) {
        UserMessagesData mUserMessagesData = mUserMessagesDataList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.lay_parent_chat_item.setLayoutParams(params);

        Glide.with(mContext)
                .load(mUserMessagesData.getImage())
                .into(holder.img_user_pic);

        holder.txt_user_name.setText(mUserMessagesData.getName());
        holder.txt_last_messages.setText(mUserMessagesData.getMessage());
        holder.txt_lastchattime.setText(mUserMessagesData.getLastseen());

        holder.txt_user_name.setTypeface(mContext.getRegularBoldFonts(mContext));
        holder.txt_last_messages.setTypeface(mContext.getRegularFonts(mContext));
        holder.txt_lastchattime.setTypeface(mContext.getRegularFonts(mContext));



        UserDetails mUserDetails = mContext.getUserDetails(mContext);

        Log.e("CheckReadState", "Sender : "+mUserMessagesData.getSender());
        Log.e("CheckReadState", "Id : "+mUserDetails.getIdUser());
        Log.e("CheckReadState", "Read : "+mUserMessagesData.getRead());

        if(mUserMessagesData.getSender().equalsIgnoreCase(mUserDetails.getIdUser())){

            if(mUserMessagesData.getRead().equalsIgnoreCase("0")){


            }else{
//                holder.lay_parent_chat_item.setBackgroundColor(mContext.getResources().getColor(R.color.off_grey));
//                holder.txt_last_messages.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
//                holder.txt_last_messages.setTypeface(mContext.getRegularBoldFonts(mContext));
            }
        } else{

            if(mUserMessagesData.getRead().equalsIgnoreCase("0")){

                holder.lay_parent_chat_item.setBackgroundColor(mContext.getResources().getColor(R.color.off_grey));
                holder.txt_last_messages.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                holder.txt_last_messages.setTypeface(mContext.getRegularBoldFonts(mContext));

            }else{
            }
        }
    }

    @Override
    public int getItemCount() {
        return mUserMessagesDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout lay_parent_chat_item;

        ImageView img_user_pic;

        TextView txt_user_name, txt_last_messages,txt_lastchattime;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            lay_parent_chat_item = itemView.findViewById(R.id.lay_parent_chat_item);

            img_user_pic = itemView.findViewById(R.id.img_user_pic);
            txt_user_name = itemView.findViewById(R.id.txt_user_name);
            txt_last_messages = itemView.findViewById(R.id.txt_last_messages);
            txt_lastchattime = itemView.findViewById(R.id.txt_lastchattime);

            mContext.overrideFonts(lay_parent_chat_item,mContext);
        }
    }
}