package com.aasthasolutions.bridalbargain.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.SimpleIntroActivity;
import com.aasthasolutions.bridalbargain.model.BannerTitle;

import java.util.ArrayList;


public class IntroPagerAdapter extends PagerAdapter {

    private SimpleIntroActivity mContext;
    private LayoutInflater layoutInflater;
    public ArrayList<BannerTitle> mInro_Images_List = new ArrayList<>();

    public IntroPagerAdapter(SimpleIntroActivity context, ArrayList<BannerTitle> mList) {
        this.mContext = context;
        this.mInro_Images_List = mList;
    }

    @Override
    public int getCount() {
        return mInro_Images_List.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = layoutInflater.inflate(R.layout.item_simple_intro, null);

//        LinearLayout img_item_intro = itemView.findViewById(R.id.img_item_intro);

        ImageView img_item_intro = itemView.findViewById(R.id.img_item_intro);
        TextView txt_banner_title = itemView.findViewById(R.id.txt_banner_title);


        txt_banner_title.setTypeface(mContext.getRegularFonts(mContext));

        txt_banner_title.setText(mInro_Images_List.get(position).getTitle());
        img_item_intro.setImageResource(mInro_Images_List.get(position).getImageId());



        ViewPager vp = (ViewPager) container;
        vp.addView(itemView, 0);
        return itemView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}