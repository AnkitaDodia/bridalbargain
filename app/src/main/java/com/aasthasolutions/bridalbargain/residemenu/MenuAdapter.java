package com.aasthasolutions.bridalbargain.residemenu;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aasthasolutions.bridalbargain.R;
import com.aasthasolutions.bridalbargain.activities.HomeActivity;

import java.util.List;


public class MenuAdapter extends ArrayAdapter<ResideMenuItem> {
    protected HomeActivity mContext;
    protected int resourceId;
    protected List<ResideMenuItem> list;
//    private Typeface font;

    public MenuAdapter(HomeActivity context, List<ResideMenuItem> list){
        this(context, R.layout.item_reside_menu, list);
//        font = Typeface.createFromAsset(context.getAssets(),"Ubuntu-R.ttf");
    }
    public MenuAdapter(HomeActivity context, int resourceId, List<ResideMenuItem> list) {
        super(context, resourceId, list);

        this.mContext = context;
        this.resourceId = resourceId;
        this.list = list;
//        font = Typeface.createFromAsset(context.getAssets(),"Ubuntu-R.ttf");
    }

    public int getCount() {
        return list.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder;

        if (rowView == null) {
            LayoutInflater inflater = mContext.getLayoutInflater();
            rowView = inflater.inflate(resourceId, null);

            viewHolder = new ViewHolder();

            viewHolder.tv_title = (TextView) rowView.findViewById(R.id.tv_title);
            viewHolder.iv_icon = (ImageView) rowView.findViewById(R.id.iv_icon);

            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        ResideMenuItem item = list.get(position);

        /*int iconResId = item.getIconResId();
        if(iconResId != 0){
            viewHolder.tv_title.setCompoundDrawablesWithIntrinsicBounds
                    (context.getResources().getDrawable
                            (iconResId),
                    null,
                    null,
                    null);
        }*/
//        viewHolder.tv_title.setTypeface(font);
        viewHolder.tv_title.setText(item.getLabel());
        viewHolder.tv_title.setTypeface(mContext.getRegularFonts(mContext));
        viewHolder.iv_icon.setImageResource(item.getIconResId());

        return rowView;
    }

    static class ViewHolder {
        TextView tv_title;
        ImageView iv_icon;
    }
}

